<?php 
	session_start();

	if (isset($_SESSION['user_details'])) {
		session_unset();
	}

	header("Location: login.php");
?>