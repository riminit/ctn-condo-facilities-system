<h5 class="center grey-text">Sign Up</h5>

<form action="signup.php" method="POST" enctype="multipart/form-data">
	<div class="row">
		<!-- START OF PERSONAL DETAILS -->
		<h6 class="center grey-text">Personal Details</h6>

		<br>

		<!-- IC Number input -->
		<div class="input-field col s6">
			<input id="ic_num" type="tel" name="ic_num" min="0" value="<?php echo isset($ic_num)? htmlspecialchars($ic_num) : '' ?>" class="validate" maxlength="12" required pattern="^[0-9]{12}$" onchange="check_ic_num()">
			<label for="ic_num">MyKad Number</label>
			<span id="ic_num_helper" class="helper-text" data-error="Required field."/>
		</div>

		<!-- IC Name input -->
		<div class="input-field col s6">
			<input id="ic_name" type="text" name="ic_name" value="<?php echo isset($ic_name)? htmlspecialchars($ic_name) : '' ?>" class="validate" required>
			<label for="ic_name">Full Name (According to MyKad)</label>
			<span id="ic_name_helper" class="helper-text" data-error="Required field."/>
		</div>
		
		<?php 
			// if alternative address is set
			if (isset($_SESSION['alt_address'])) {
				include('alt_address_form.php') ;
			}
			include('address_form.php') ;
		?>

		<?php if ($errors) : ?>
			<ul class="browser-default error-msg">
				<?php foreach ($errors as $error) : ?>
					<li><?php echo $error; ?></li>
				<?php endforeach ?>
			</ul>
		<?php endif ?>

		<div class="col s4 center">
			<a href="../login.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
		</div>

		<div class="col s4 center">
			<a href="signup.php" class="btn orange z-depth-0"><i class="material-icons left">chevron_left</i>Back</a>
		</div>

		<div class="col s4 center">
			<button type="submit" name="submit_personal" value="Next" class="btn green z-depth-0"><i class="material-icons right">chevron_right</i>Next</button>
		</div>
		
	</div>
	
</form>