<?php

	// get the states from config
	$config = include('../config/config.php');
	$states = $config['states'];

	$errors = array();
	$su_username = $contact = $ic_name = $address = $city = '';

	// connect to database
	include('../config/db_connect.php');

	//include database functions
	include('../includes/db_functions.php');

	session_start();

	// if signup details has been set before
	// (needed to persist form data when user goes from the form review page back to form page)
	if (isset($_SESSION['signup_details']['su_username'])) {
		$su_username = $_SESSION['signup_details']['su_username'];
		$su_pwd = $_SESSION['signup_details']['su_pwd'];
		$contact = $_SESSION['signup_details']['contact'];
	}
	if (isset($_SESSION['signup_details']['ic_num'])) {
		$ic_num = $_SESSION['signup_details']['ic_num'];
		$ic_name = $_SESSION['signup_details']['ic_name'];
		$level = $_SESSION['signup_details']['level'];
		$unit = $_SESSION['signup_details']['unit'];
		$address = $_SESSION['signup_details']['address'];
		$postcode = $_SESSION['signup_details']['postcode'];
		$city = $_SESSION['signup_details']['city'];
		$state = $_SESSION['signup_details']['state'];
	}

	if (isset($_SESSION['alt_address'])) {
		$alt_level = $_SESSION['alt_address']['level'];
		$alt_unit = $_SESSION['alt_address']['unit'];
		$alt_address = $_SESSION['alt_address']['address'];
		$alt_postcode = $_SESSION['alt_address']['postcode'];
		$alt_city = $_SESSION['alt_address']['city'];
		$alt_state = $_SESSION['alt_address']['state'];
	}

	// if user clicked next (To submit account details) -------------------------------------------------
	if (isset($_POST['submit_account']) && !isset($_POST['submit_personal'])) {
		// to prevent XSS injection attacks
		$su_username = htmlspecialchars($_POST['su_username']);
		$su_pwd = htmlspecialchars($_POST['su_pwd']);
		$contact = htmlspecialchars($_POST['contact']);

		$su_username = mysqli_real_escape_string($conn, $su_username);

		// query for getting data of the user with the same name
		$sql = "SELECT * 
				FROM `USER` 
				WHERE `username`='$su_username'";

		// get result in array
		$same_username = select_single($conn, $sql);

		// if username has been taken
		if (sizeof($same_username)) {
			$errors[] = "Username has been taken.";
		}
		
		// if no errors
		if (!$errors) {
			// set account details to SESSION signup_details
			$_SESSION['signup_details'] = array('su_username' => $su_username,
												'su_pwd' => $su_pwd,
												'contact' => $contact);
		}
		
	}

	// if user clicked next (To submit personal details) -------------------------------------------------
	if (isset($_POST['submit_personal'])) {
		// to prevent XSS injection attacks
		$ic_num = htmlspecialchars($_POST['ic_num']);
		$ic_name = htmlspecialchars($_POST['ic_name']);
		$level = htmlspecialchars($_POST['level']);
		$unit = htmlspecialchars($_POST['unit']);
		$address = htmlspecialchars($_POST['address']);
		$postcode = htmlspecialchars($_POST['postcode']);
		$city = htmlspecialchars($_POST['city']);

		$ic_num = mysqli_real_escape_string($conn, $ic_num);
		// query for getting data of any user with the same ic number
		$sql = "SELECT * 
				FROM `USER` 
				WHERE `ic_num`='$ic_num'";

		// get result in array
		$same_ic_num = select_single($conn, $sql);

		// if there is an existing user with the same ic number
		if (sizeof($same_ic_num)) {
			$errors[] = "There is already an existing user with the same MyKad number.";
		}

		// if a state has already been selected
		if (isset($_POST['state'])) {
			$state = $_POST['state'];

		} else {
			$errors[] = "Please select a state.";
		}

		// get the default address
		$config = include('../config/config.php');
		$default_address = $config['default_address'];

		// compare to check current address (return 0 if match)
		$invalid_address = strcasecmp($default_address['address'], $address);

		foreach ($default_address['city'] as $default_city) {
			// compare to check current city
			if (strcasecmp($default_city, $city)) {
				$invalid_city = 1;
			} else {
				// if there is a match
				$invalid_city = 0;
				break;
			}
		}

		// if address that was keyed in wasnt CTN Condominium
		if ($invalid_address || $postcode!=$default_address['postcode'] || $invalid_city || $state!=$default_address['state']) {
			// set the address details as alternative address
			$_SESSION['alt_address'] = array('level' => $level,
											'unit' => $unit,
											'address' => $address,
											'postcode' => $postcode,
											'city' => $city,
											'state' => $state);

			$alt_level = $level;
			$alt_unit = $unit;
			$alt_address = $address;
			$alt_postcode = $postcode;
			$alt_city = $city;
			$alt_state = $state;

			$level = $unit = '';
			$address = $default_address['address'];
			$postcode = $default_address['postcode'];
			$city = $default_address['city'][0];
			$state = $default_address['state'];

			$errors[] = "Please fill in your current(CTN Condominium) address.";
		}

		// if the form is submitted with the alt address form
		if (isset($_POST['alt_level'])) {
			// update the SESSION alt_address with the latest values
			$_SESSION['alt_address'] = array('level' => htmlspecialchars($_POST['alt_level']),
											'unit' => htmlspecialchars($_POST['alt_unit']),
											'address' => htmlspecialchars($_POST['alt_address']),
											'postcode' => htmlspecialchars($_POST['alt_postcode']),
											'city' => htmlspecialchars($_POST['alt_city']),
											'state' => htmlspecialchars($_POST['alt_state']));
		}

		// if no errors
		if (!$errors) {
			// add personal details to SESSION signup_details
			$_SESSION['signup_details'] += array('ic_num' => $ic_num,
												'ic_name' => $ic_name,
												'level' => $level,
												'unit' => $unit,
												'address' => $address,
												'postcode' => $postcode,
												'city' => $city,
												'state' => $state);
		}
	}

	// if details were confirmed (When user is in form review page) -------------------------------------------------
	if(isset($_POST['signup'])) {
		// escape any kind of malicious SQL characters & protect from SQL injection
		$su_username = mysqli_real_escape_string($conn, $_SESSION['signup_details']['su_username']);
		$su_pwd = mysqli_real_escape_string($conn, $_SESSION['signup_details']['su_pwd']);
		$contact = mysqli_real_escape_string($conn, $_SESSION['signup_details']['contact']);

		$ic_num = mysqli_real_escape_string($conn, $_SESSION['signup_details']['ic_num']);
		$ic_name = mysqli_real_escape_string($conn, $_SESSION['signup_details']['ic_name']);
		$level = mysqli_real_escape_string($conn, $_SESSION['signup_details']['level']);
		$unit = mysqli_real_escape_string($conn, $_SESSION['signup_details']['unit']);
		$address = mysqli_real_escape_string($conn, $_SESSION['signup_details']['address']);
		$postcode = mysqli_real_escape_string($conn, $_SESSION['signup_details']['postcode']);
		$city = mysqli_real_escape_string($conn, $_SESSION['signup_details']['city']);
		$state = mysqli_real_escape_string($conn, $_SESSION['signup_details']['state']);

		// password hashing
		$hash = password_hash($su_pwd, PASSWORD_DEFAULT);

		// query to insert in USER table (status = 0 for unverified users)
		$sql = "INSERT INTO `USER` (`user_type`, `username`, `password`, `ic_num`, `ic_name`, `contact_num`, `status`)
				VALUES ('resident', '$su_username', '$hash', '$ic_num', '$ic_name', '$contact', '0')";

		// if user successfully added
		if (execute_query($conn, $sql)) {
			// get the user_id of the new user
			$sql = "SELECT `user_id`
					FROM `USER`
					WHERE `username`='$su_username'";

			$user_id = select_single($conn, $sql)['user_id'];

			// add the current address in the ADDRESS table
			$sql = "INSERT INTO `ADDRESS` (`user_id`, `level`, `unit`, `address`, `postcode`, `city`, `state`, `type`)
					VALUES ('$user_id', '$level', '$unit', '$address', '$postcode', '$city', '$state', 'current')";

			execute_query($conn, $sql);

			// if there is an alternative address set
			if (isset($_SESSION['alt_address'])) {
				$alt_level = mysqli_real_escape_string($conn, $_SESSION['alt_address']['level']);
				$alt_unit = mysqli_real_escape_string($conn, $_SESSION['alt_address']['unit']);
				$alt_address = mysqli_real_escape_string($conn, $_SESSION['alt_address']['address']);
				$alt_postcode = mysqli_real_escape_string($conn, $_SESSION['alt_address']['postcode']);
				$alt_city = mysqli_real_escape_string($conn, $_SESSION['alt_address']['city']);
				$alt_state = mysqli_real_escape_string($conn, $_SESSION['alt_address']['state']);

				// add the alternative address in the ADDRESS table
				$sql = "INSERT INTO `ADDRESS` (`user_id`, `level`, `unit`, `address`, `postcode`, `city`, `state`, `type`)
						VALUES ('$user_id', '$alt_level', '$alt_unit', '$alt_address', '$alt_postcode', '$alt_city', '$alt_state', 'alternative')";

				execute_query($conn, $sql);

				// unset the alt_address session variables
				unset($_SESSION['alt_address']);
			}

			// notify and redirect user to login page after they click signup
			echo "<script type='text/javascript'>alert('Sign-up successful. Please contact the condominium manager to verify your account.'); window.location.href = '../login.php'</script>";

		} else {
			echo "<script type='text/javascript'>alert('Error adding new user! Please try again later.'); window.location.href = '../login.php'</script>";
		}
	}

	// if user clicked the back button at the review page
	if (isset($_POST['review_back'])) {
		$_POST['submit_account'] = 'Next';
	}

	// close connection
	mysqli_close($conn);

?>

<!DOCTYPE html>
<html>

	<?php include('../templates/header.php') ?>

	<section class="container content-wrap">
		<div class="row">
			<div class="col s2 offset-s5" style="padding-top: 1rem; margin-bottom: -2rem">
				<img src="../img/icons/ctncondo_brandlogo2.ico" class="responsive-img">
			</div>
		</div>
		<h3 id="title" class="center brand-text" style="margin-bottom: -10px">CTN Condominium</h3>
		<h5 class="center brand-text">Facilities System</h5>

		<div class="row">
      <div class="col s12 card white">
        <div class="card-content container">
        	<?php
        		// check if user has submitted account details and there are no errors
	        	if (isset($_POST['submit_account']) && !$errors) {
	        		include('personal_details_form.php');
	        	} else {
	        		if (isset($_POST['submit_personal'])) {
	        			if (!$errors) {
	        				include('signup_form_review.php');
	        			} else {
	        				include('personal_details_form.php');
	        			}
		        	} else {
		        		include('account_details_form.php');
		        	}
	        	} 
        	?>
        </div>
        <hr>
        <div class="center">
        	<p class="grey-text">Already have an account?
        		<a href="../login.php">Log In</a>
        	</p>
        </div>
    	</div>
	  </div>
	</section>

	<?php include('../templates/footer.php') ?>

</html>