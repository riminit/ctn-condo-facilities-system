<ul class="collection with-header">
	<a onclick="<?php unset($_SESSION['alt_address']) ?>" class="secondary-content delete"><i class="material-icons red-text">close</i></a>
	<li class="collection-header grey-text"><h6>Alternative Address</h6></li>
	<li class="collection-item">
		<!-- Level input -->
		<div class="input-field col s3">
			<input id="alt_level" type="number" name="alt_level" min="0" value="<?php echo $alt_level ?>" class="validate" required>
			<label for="alt_level">Level</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- Unit input -->
		<div class="input-field col s3">
			<input id="alt_unit" type="number" name="alt_unit" min="0" value="<?php echo $alt_unit ?>" class="validate" required>
			<label for="alt_unit">Unit Number</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- Address input -->
		<div class="input-field col s6">
			<input id="alt_address" type="text" name="alt_address" value="<?php echo $alt_address ?>" class="validate" required>
			<label for="alt_address">Address</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- Postcode input -->
		<div class="input-field col s3">
			<input id="alt_postcode" type="number" name="alt_postcode" min="0" max="99999" value="<?php echo $alt_postcode ?>" class="validate" required>
			<label for="alt_postcode">Postcode</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- City input -->
		<div class="input-field col s3">
			<input id="alt_city" type="text" name="alt_city" value="<?php echo $alt_city ?>">
			<label for="alt_city">City</label>
		</div>

		<!-- State input -->
		<div class="input-field col s6">
			<select id="alt_state" name="alt_state">
				<?php 
				// set selected option if state has been previously selected
				foreach ($states as $option => $value) {
					$attribute = '';

					// if the state matches the states option value
					if ($alt_state == $value) {
						$attribute = ' selected';
					}

					// disable the 'Choose your option' option
					if ($value == '') {
						$attribute .= ' disabled';
					}

					echo '<option value="'.$value.'"'.$attribute.'>'.$option.'</option>';
				} ?>
			</select>
			<label>State</label>
		</div>
	</li>
</ul>