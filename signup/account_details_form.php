<h5 class="center grey-text">Sign Up</h5>

<form action="signup.php" method="POST" enctype="multipart/form-data">
	<div class="row">
		<!-- START OF ACCOUNT DETAILS -->
		<h6 class="center grey-text">Account Details</h6>

		<!-- Username input -->
		<div class="input-field tooltipped col s6" data-position="bottom" 
		data-tooltip="
		<ul class='left-align browser-default'>
			<li>Max. Characters is 10.</li>
			<li>Only alphanumeric characters, '_' and '-'.</li>
			<li>Cannot have two or more underscores or hyphens in a row.</li>
			<li>Cannot have an underscore or hypen at the start or end.</li>
		</ul>
		">
			<i class="material-icons prefix">account_circle</i>
			<input id="su_username" type="text" name="su_username" 
			value="<?php echo htmlspecialchars($su_username) ?>" class="validate" 
			required pattern="^[a-zA-Z]+((_|-)?[a-zA-Z0-9])*[a-zA-Z0-9]*$" 
			maxlength="10"
			onchange="check_username()">
			<label for="su_username">Username</label>
			<span id="usr_helper" class="helper-text" data-error="Required field."/>
		</div>

		<!-- Contacts input -->
		<div class="input-field col s6">
			<i class="material-icons prefix">phone</i>
			<input id="contact" type="tel" name="contact" value="<?php echo htmlspecialchars($contact) ?>" class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
			<label for="contact">Contact Number</label>
			<span id="contact_helper" class="helper-text" data-error="Required field."/>
		</div>

		<!-- Password input -->
		<div class="input-field col s6">
			<i class="material-icons prefix">lock</i>
			<input id="su_pwd" type="password" name="su_pwd" class="validate" required onchange="check_pwd_con()">
			<label for="su_pwd">Password</label>
			<span id="pwd_helper" class="helper-text" data-error="Required field."/>
		</div>

		<!-- Confirm Password input -->
		<div class="input-field col s6">
			<input id="su_pwd_con" type="password" name="su_pwd_con" class="validate" required onchange="check_pwd_con()" onblur="check_pwd_con()">
			<label for="su_pwd_con">Confirm Password</label>
			<span id="pwd_con_helper" class="helper-text" data-error="Required field."/>
		</div>

	    <?php if ($errors) : ?>
	    	<div class="col s12">
	    		<ul class="browser-default error-msg">
					<?php foreach ($errors as $error) : ?>
						<li><?php echo $error; ?></li>
					<?php endforeach ?>
				</ul>
	    	</div>
		<?php endif ?>

		<div class="col s4 center">
			<a href="../login.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
		</div>

		<div class="col s4 center offset-s4">
			<button type="submit" name="submit_account" value="Next" class="btn green z-depth-0"><i class="material-icons right">chevron_right</i>Next</button>
		</div>
		
	</div>
	
</form>