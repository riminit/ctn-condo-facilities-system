<ul class="collection with-header">
	<li class="collection-header grey-text"><h6>Current Address</h6></li>
	<li class="collection-item">
		<!-- Level input -->
		<div class="input-field col s3">
			<input id="level" type="number" name="level" min="0" value="<?php echo $level ?>" class="validate" required>
			<label for="level">Level</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- Unit input -->
		<div class="input-field col s3">
			<input id="unit" type="number" name="unit" min="0" value="<?php echo $unit ?>" class="validate" required>
			<label for="unit">Unit Number</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- Address input -->
		<div class="input-field col s6">
			<input id="address" type="text" name="address" value="<?php echo $address ?>" class="validate" required>
			<label for="address">Address</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- Postcode input -->
		<div class="input-field col s3">
			<input id="postcode" type="number" name="postcode" min="0" max="99999" value="<?php echo $postcode ?>" class="validate" required>
			<label for="postcode">Postcode</label>
			<span class="helper-text" data-error="Required field."/>
		</div>

		<!-- City input -->
		<div class="input-field col s3">
			<input id="city" type="text" name="city" value="<?php echo $city ?>">
			<label for="city">City</label>
		</div>

		<!-- State input -->
		<div class="input-field col s6">
			<select id="state" name="state">
				<?php 
				// set selected option if state has been previously selected
				foreach ($states as $option => $value) {
					$attribute = '';

					// if the state matches the states option value
					if ($state == $value) {
						$attribute = ' selected';
					}

					// disable the 'Choose your option' option
					if ($value == '') {
						$attribute .= ' disabled';
					}

					echo '<option value="'.$value.'"'.$attribute.'>'.$option.'</option>';
				} ?>
			</select>
			<label>State</label>
		</div>
	</li>
</ul>