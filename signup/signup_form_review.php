<h5 class="center grey-text">Sign Up Review</h5>
<h6 class="center grey-text">Please Review the Details</h6>

<form action="signup.php" method="POST">
	<div class="row">
		<!-- START OF ACCOUNT DETAILS -->
		<h6 class="center grey-text">Account Details</h6>

		<!-- Username input -->
		<div class="input-field col s6">
			<i class="material-icons prefix">account_circle</i>
			<input disabled id="su_username" type="text" name="su_username" 
			value="<?php echo $su_username ?>">
			<label for="su_username">Username</label>
		</div>

		<!-- Contacts input -->
		<div class="input-field col s6">
			<i class="material-icons prefix">phone</i>
			<input disabled id="contact" type="tel" name="contact" value="<?php echo $contact ?>">
			<label for="contact">Contact Number</label>
		</div>

		<div class="col s12">
			<div class="divider"></div>
			<br>
		</div>

		<!-- START OF PERSONAL DETAILS -->
		<h6 class="center grey-text">Personal Details</h6>

		<br>

		<!-- IC Number input -->
		<div class="input-field col s6">
			<input disabled id="ic_num" type="number" name="ic_num" value="<?php echo $ic_num ?>">
			<label for="ic_num">MyKad Number</label>
		</div>

		<!-- IC Name input -->
		<div class="input-field col s6">
			<input disabled id="ic_name" type="text" name="ic_name" value="<?php echo $ic_name ?>">
			<label for="ic_name">Full Name (According to MyKad)</label>
		</div>

		<?php if (isset($_SESSION['alt_address'])) : ?>
			<!-- ALTERNATIVE ADDRESS -->
			<ul class="collection with-header">
				<li class="collection-header grey-text"><h6>Alternative Address</h6></li>
				<li class="collection-item">
					<!-- Level input -->
					<div class="input-field col s3">
						<input disabled id="alt_level" type="number" name="alt_level" min="0" value="<?php echo $_SESSION['alt_address']['level'] ?>" class="validate" required>
						<label for="alt_level">Level</label>
						<span class="helper-text" data-error="Required field."/>
					</div>

					<!-- Unit input -->
					<div class="input-field col s3">
						<input disabled id="alt_unit" type="number" name="alt_unit" min="0" value="<?php echo $_SESSION['alt_address']['unit'] ?>" class="validate" required>
						<label for="alt_unit">Unit Number</label>
						<span class="helper-text" data-error="Required field."/>
					</div>

					<!-- Address input -->
					<div class="input-field col s6">
						<input disabled id="alt_address" type="text" name="alt_address" value="<?php echo $_SESSION['alt_address']['address'] ?>" class="validate" required>
						<label for="alt_address">Address</label>
						<span class="helper-text" data-error="Required field."/>
					</div>

					<!-- Postcode input -->
					<div class="input-field col s3">
						<input disabled id="alt_postcode" type="number" name="alt_postcode" min="0" max="99999" value="<?php echo $_SESSION['alt_address']['postcode'] ?>" class="validate" required>
						<label for="alt_postcode">Postcode</label>
						<span class="helper-text" data-error="Required field."/>
					</div>

					<!-- City input -->
					<div class="input-field col s3">
						<input disabled id="alt_city" type="text" name="alt_city" value="<?php echo $_SESSION['alt_address']['city'] ?>">
						<label for="alt_city">City</label>
					</div>

					<!-- State input -->
					<div class="input-field col s6">
						<input disabled id="alt_state" type="text" name="alt_state" value="<?php echo $_SESSION['alt_address']['state'] ?>">
						<label for="alt_state">State</label>
					</div>
				</li>
			</ul>
		<?php endif ?>

		<!-- CURRENT ADDRESS -->
		<ul class="collection with-header">
			<li class="collection-header grey-text"><h6>Current Address</h6></li>
			<li class="collection-item">
				<!-- Level input -->
				<div class="input-field col s3">
					<input disabled id="level" type="number" name="level" value="<?php echo $level ?>">
					<label for="level">Level</label>
				</div>

				<!-- Unit input -->
				<div class="input-field col s3">
					<input disabled id="unit" type="number" name="unit" value="<?php echo $unit ?>">
					<label for="unit">Unit Number</label>
				</div>

				<!-- Address input -->
				<div class="input-field col s6">
					<input disabled id="address" type="text" name="address" value="<?php echo $address ?>">
					<label for="address">Address</label>
				</div>

				<!-- Postcode input -->
				<div class="input-field col s3">
					<input disabled id="postcode" type="number" name="postcode" value="<?php echo $postcode ?>">
					<label for="postcode">Postcode</label>
				</div>

				<!-- City input -->
				<div class="input-field col s3">
					<input disabled id="city" type="text" name="city" value="<?php echo $city ?>">
					<label for="city">City</label>
				</div>
				
				<!-- State input -->
				<div class="input-field col s6">
					<input disabled id="state" type="text" name="state" value="<?php echo $state ?>">
					<label for="state">State</label>
				</div>
			</li>
		</ul>
		
	</div>

	<div class="center">
		<div class="col s4 center">
			<a href="../login.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
		</div>

		<div class="col s4 center">
			<button type="submit" name="review_back" value="review_back" class="btn orange z-depth-0"><i class="material-icons right">chevron_left</i>Back</button>
		</div>

		<div class="col s4 center">
			<button type="submit" name="signup" value="Confirm" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
		</div>
	</div>
	
</form>