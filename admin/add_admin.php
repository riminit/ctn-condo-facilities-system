<?php 
	// connect to database
	include('../config/db_connect.php');

	//include database functions
	include('../includes/db_functions.php');

	$errors = array();

	if (isset($_POST['submit'])) {
		$username = htmlspecialchars($_POST['username']);
		$username = mysqli_real_escape_string($conn, $username);

		$ic_num = htmlspecialchars($_POST['ic_num']);
		$ic_num = mysqli_real_escape_string($conn, $ic_num);

		// query for getting data of any user with the same name
		$sql = "SELECT * 
				FROM `USER` 
				WHERE `username`='$username'";

		// get result in array
		$same_username = select_single($conn, $sql);

		// if username has been taken
		if (sizeof($same_username)) {
			$errors[] = "Username has been taken.";

		}

		// query for getting data of any user with the same ic number
		$sql = "SELECT * 
				FROM `USER` 
				WHERE `ic_num`='$ic_num'";

		// get result in array
		$same_ic_num = select_single($conn, $sql);

		// if there is an existing user with the same ic number
		if (sizeof($same_ic_num)) {
			$errors[] = "There is already an existing user with the same MyKad number.";
		}

		if (!$errors) {
			$contact = htmlspecialchars($_POST['contact']);
			$contact = mysqli_real_escape_string($conn, $contact);
			
			$ic_name = htmlspecialchars($_POST['ic_name']);
			$ic_name = mysqli_real_escape_string($conn, $ic_name);

			// default pass (format: [last 5 letters of name] + @ + [last 4 digits of MyKad number])
			$default_pass = substr($ic_name,-5) . '@' . substr($ic_num,-4);

			// password hashing
			$hash = password_hash($default_pass, PASSWORD_DEFAULT);

			// query to insert in USER table (status = 1 for verified users)
			$sql = "INSERT INTO `USER` (`user_type`, `username`, `password`, `ic_num`, `ic_name`, `contact_num`, `status`)
					VALUES ('admin', '$username', '$hash', '$ic_num', '$ic_name', '$contact', '1')";

			if (execute_query($conn, $sql)) {
				// notify and redirect user to admin_accounts page if succesfully added
				echo "<script type='text/javascript'>alert('Admin account succesfully added.'); window.location.href = 'admin_accounts.php'</script>";
			} else {
				$errors[] = 'Error adding admin account.';
			}
		}
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<section class="container content-wrap">
		<h3 id="title" class="center brand-text">New Admin Account</h3>
		<div class="col s12 card white">
			<div class="card-content container">
				<form action="add_admin.php" method="POST" enctype="multipart/form-data">
					<div class="row">
						<!-- START OF ACCOUNT DETAILS -->
						<h6 class="center grey-text">Account Details</h6>

						<!-- Username input -->
						<div class="input-field tooltipped col s6" data-position="bottom" 
						data-tooltip="
						<ul class='left-align browser-default'>
							<li>Max. Characters is 10.</li>
							<li>Only alphanumeric characters, '_' and '-'.</li>
							<li>Cannot have two or more underscores or hyphens in a row.</li>
							<li>Cannot have an underscore or hypen at the start or end.</li>
						</ul>
						">
							<i class="material-icons prefix">account_circle</i>
							<input id="su_username" type="text" name="username" 
							value="<?php echo isset($_POST['username']) ? $_POST['username'] : '' ?>" 
							class="validate" 
							required pattern="^[a-zA-Z]+((_|-)?[a-zA-Z0-9])*[a-zA-Z0-9]*$" 
							maxlength="10"
							onchange="check_username()">
							<label for="su_username">Username</label>
							<span id="usr_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- Contacts input -->
						<div class="input-field col s6">
							<i class="material-icons prefix">phone</i>
							<input id="contact" type="tel" name="contact" value="<?php echo isset($_POST['contact']) ? $_POST['contact'] : '' ?>"   class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
							<label for="contact">Contact Number</label>
							<span id="contact_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- START OF PERSONAL DETAILS -->
						<h6 class="center grey-text">Personal Details</h6>

						<!-- IC Number input -->
						<div class="input-field col s6">
							<input id="ic_num" type="tel" name="ic_num" value="<?php echo isset($_POST['ic_num']) ? $_POST['ic_num'] : '' ?>"  min="0" class="validate" maxlength="12" required pattern="^[0-9]{12}$" onchange="check_ic_num()">
							<label for="ic_num">MyKad Number</label>
							<span id="ic_num_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- IC Name input -->
						<div class="input-field col s6">
							<input id="ic_name" type="text" name="ic_name" value="<?php echo isset($_POST['ic_name']) ? $_POST['ic_name'] : '' ?>"  class="validate" required>
							<label for="ic_name">Full Name (According to MyKad)</label>
							<span id="ic_name_helper" class="helper-text" data-error="Required field."/>
						</div>
					</div>

					<div class="row">
						<?php if ($errors) : ?>
							<ul class="browser-default error-msg">
								<?php foreach ($errors as $error) : ?>
									<li><?php echo $error; ?></li>
								<?php endforeach ?>
							</ul>
						<?php endif ?>

						<div class="col s4 center">
							<a href="admin_accounts.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
						</div>

						<div class="col s4 center offset-s4">
							<button type="submit" name="submit" value="submit" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

	<?php include('../templates/footer.php') ?>
</html>