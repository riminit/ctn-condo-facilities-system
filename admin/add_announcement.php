<?php 
	// start session if session hasnt been started / $_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	$errors = array();

	$current_date = date("Y-m-d");

	// if user submitted the announcement form
	if (isset($_POST['submit'])) {
		$title = mysqli_real_escape_string($conn, htmlspecialchars($_POST['title']));
		$description = mysqli_real_escape_string($conn, htmlspecialchars($_POST['description']));
		$date = mysqli_real_escape_string($conn, htmlspecialchars($_POST['date']));

		$date = strtotime("23:59:59 ".$date);
		$date = date("Y-m-d H:i:s", $date);

		if (isset($_POST['pin'])) {
			$is_pinned = 1;
		} else {
			$is_pinned = 0;
		}

		$user_id = $_SESSION['user_details']['user_id'];

		// query to insert in ANNOUNCEMENT table
		$sql = "INSERT INTO `ANNOUNCEMENT` (`user_id`, `announcement_title`, `announcement_desc`, `due_date`, `is_pinned`)
				VALUES ('$user_id', '$title', '$description', '$date', '$is_pinned')";

		// if report successfully submitted
		if (execute_query($conn, $sql)) {
			// notify and redirect user to the announcement page
			echo "<script type='text/javascript'>alert('Announcement made successfully.'); window.location.href = 'announcements.php'</script>";
		} else {
			$errors[] = "Error making announcement! Please try again later.";
		}
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<div class="content-wrap">
		<section class="container">
			<h3 class="center brand-text">New Announcement</h3>
			<div class="card white">
		        <div class="card-content">
		        	<h5 class="center grey-text">Fill in the details</h5>
		        	<form action="add_announcement.php" method="POST">
		        		<div class="row">
							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<input id="title" type="text" name="title" class="validate" required>
								<label for="title">Title</label>
								<span id="title_helper" class="helper-text" data-error="Required field."/>
							</div>

							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<textarea id="description" name="description" class="materialize-textarea validate" required></textarea>
								<label for="description">Description</label>
								<span id="description" class="helper-text" data-error="Required field."/>
							</div>

							<div class="input-field col s4 offset-s4">
								<input id="date" type="date" name="date" class="validate" required min="<?php echo $current_date ?>" onchange="check_date()">
								<label for="date">Expiry Date</label>
								<span id="date_helper" class="helper-text" data-error="Required field."/>
							</div>

							<div class="col s12 center">
								<label>
									<input type="checkbox" name="pin" />
									<span>Pin announcement?</span>
								</label>
							</div>
						</div> <!-- end of div.row -->

						<div class="row">
							<?php if ($errors) : ?>
								<ul class="browser-default error-msg">
									<?php foreach ($errors as $error) : ?>
										<li><?php echo $error; ?></li>
									<?php endforeach ?>
								</ul>
							<?php endif ?>

							<div class="col s4 center">
								<a href="announcements.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
							</div>

							<div class="col s4 center offset-s4">
								<button type="submit" name="submit" value="submit" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
							</div>
						</div>
		        	</form>
		        </div>	<!-- end of div.card-content -->
		    </div>
		</section>
	</div>

	<?php include('../templates/footer.php') ?>

</html>
