<?php 
  // start session if session hasnt been started / $_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  // connect to database
  include('../config/db_connect.php');

  // include database functions
  include('../includes/db_functions.php');

  $errors = array();

  if (isset($_POST['submit'])) {
    $subject = mysqli_real_escape_string($conn, htmlspecialchars($_POST['subject']));
    $description = mysqli_real_escape_string($conn, htmlspecialchars($_POST['description']));

    $user_id = $_SESSION['user_details']['user_id'];
    $recipient_id = $_GET['userid'];

    // query to insert in NOTIFICATION table
    $sql = "INSERT INTO `NOTIFICATION` (`user_id`, `subject`, `description`)
            VALUES ('$user_id', '$subject', '$description')";

    // if notification successfully added
    if (execute_query($conn, $sql)) {
      // get the last id
      $notification_id = mysqli_insert_id($conn);

      // query to insert in RECIPIENT table
      $sql = "INSERT INTO `RECIPIENT` (`notification_id`, `user_id`, `status`)
              VALUES ('$notification_id', '$recipient_id', '0')";

      if (execute_query($conn, $sql)) {
        // notify and redirect user to the report page
        echo "<script type='text/javascript'>alert('Reply made successfully.'); window.location.href = 'report.php'</script>";
      }
    } else {
      $errors[] = "Error making reply! Please try again later.";
    }
  }

  // close connection
  mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <section class="container">
      <h3 class="center brand-text">Reply to Report</h3>
      <div class="card white">
        <div class="card-content">
          <h5 class="center grey-text">Fill in the details</h5>
          <form action="reply_report.php?userid=<?php echo $_GET['userid'] ?>" method="POST">
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">mode_edit</i>
                <input id="subject" type="text" name="subject" class="validate" value="Reply to Report: " required>
                <label for="subject">Title</label>
                <span id="subject_helper" class="helper-text" data-error="Required field."/>
              </div>

              <div class="input-field col s12">
                <i class="material-icons prefix">mode_edit</i>
                <textarea id="description" name="description" class="materialize-textarea validate" required></textarea>
                <label for="description">Description</label>
                <span id="description" class="helper-text" data-error="Required field."/>
              </div>
            </div> <!-- end of div.row -->

            <div class="row">
              <?php if ($errors) : ?>
                <ul class="browser-default error-msg">
                  <?php foreach ($errors as $error) : ?>
                    <li><?php echo $error; ?></li>
                  <?php endforeach ?>
                </ul>
              <?php endif ?>

              <div class="col s4 center">
                <a href="report.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
              </div>

              <div class="col s4 center offset-s4">
                <button type="submit" name="submit" value="submit" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
              </div>
            </div>
          </form>
        </div>  <!-- end of div.card-content -->
      </div>
    </section>
  </div>

  <?php include('../templates/footer.php') ?>

</html>
