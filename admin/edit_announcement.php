<?php 
	// start session if session hasnt been started / $_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	// if user is still editing
	if (isset($_SESSION['edit_announcement'])) {
		$announcement = $_SESSION['edit_announcement'];
		$announcement_id = $announcement['announcement_id'];
		$title = $announcement['announcement_title'];
		$description = $announcement['announcement_desc'];
		$user_id = $announcement['user_id'];
		$announcement_date = $announcement['announcement_date'];
		$date = $announcement['due_date'];
		$date = date("Y-m-d", strtotime($date));
	}

	// if user clicked edit button
	if (isset($_POST['edit_announcement'])) {
		$announcement_id = $_POST['edit_announcement'];

		// query for getting the announcement details
		$sql = "SELECT  `announcement_title`, `announcement_desc`, `due_date`, `is_pinned`, `user_id`, `announcement_date` 
		        FROM `ANNOUNCEMENT` 
		        WHERE `announcement_id`='$announcement_id'";

		$announcement = select_single($conn, $sql);
		$title = $announcement['announcement_title'];
		$description = $announcement['announcement_desc'];
		$date = $announcement['due_date'];
		$date = date("Y-m-d", strtotime($date));
		$is_pinned = $announcement['is_pinned'];
		$user_id = $announcement['user_id'];
		$announcement_date = $announcement['announcement_date'];

		$_SESSION['edit_announcement'] = $announcement;
		$_SESSION['edit_announcement']['announcement_id'] = $announcement_id;

	} elseif (isset($_POST['cancel'])) {
		header("Location: announcements.php");

	} elseif (isset($_POST['save_changes'])) {
		$title = htmlspecialchars($_POST['title']);
		$description = htmlspecialchars($_POST['description']);
		$date = htmlspecialchars($_POST['date']);

		$date = strtotime("23:59:59 ".$date);
		$date = date("Y-m-d H:i:s", $date);

		if (isset($_POST['pin'])) {
			$is_pinned = 1;
		} else {
			$is_pinned = 0;
		}

		// if title was changed
		if ($title != $_SESSION['edit_announcement']['announcement_title'] ) {
			$title = mysqli_real_escape_string($conn, $title);
			// query to update row in ANNOUNCEMENT table
			$sql = "UPDATE `ANNOUNCEMENT` 
					SET `announcement_title`='$title' 
					WHERE `announcement_id`='$announcement_id'";

			// if title successfully updated
			if (execute_query($conn, $sql)) {
				// update the session title
				$_SESSION['edit_announcement']['announcement_title'] = $title;

				// notify user
				$update_msg[] = 'Title successfully edited.';
			} else {
				$errors[] = 'Error editing title! Please try again later.';
			}
		}

		// if description was changed
		if ($description != $_SESSION['edit_announcement']['announcement_desc'] ) {
			$description = mysqli_real_escape_string($conn, $description);
			// query to update row in ANNOUNCEMENT table
			$sql = "UPDATE `ANNOUNCEMENT` 
					SET `announcement_desc`='$description' 
					WHERE `announcement_id`='$announcement_id'";

			// if description successfully updated
			if (execute_query($conn, $sql)) {
				// update the session description
				$_SESSION['edit_announcement']['announcement_desc'] = $description;

				// notify user
				$update_msg[] = 'Description successfully edited.';
			} else {
				$errors[] = 'Error editing description! Please try again later.';
			}
		}

		// if due date was changed
		if ($date != $_SESSION['edit_announcement']['due_date'] ) {
			$date = mysqli_real_escape_string($conn, $date);
			// query to update row in ANNOUNCEMENT table
			$sql = "UPDATE `ANNOUNCEMENT` 
					SET `due_date`='$date' 
					WHERE `announcement_id`='$announcement_id'";

			// if date successfully updated
			if (execute_query($conn, $sql)) {
				// update the session date
				$_SESSION['edit_announcement']['due_date'] = $date;

				// notify user
				$update_msg[] = 'Expiry date successfully edited.';
			} else {
				$errors[] = 'Error editing expiry date! Please try again later.';
			}
		}

		// if is_pinned was changed
		if ($is_pinned != $_SESSION['edit_announcement']['is_pinned'] ) {
			$is_pinned = mysqli_real_escape_string($conn, $is_pinned);
			// query to update row in ANNOUNCEMENT table
			$sql = "UPDATE `ANNOUNCEMENT` 
					SET `is_pinned`='$is_pinned' 
					WHERE `announcement_id`='$announcement_id'";

			// if is_pinned successfully updated
			if (execute_query($conn, $sql)) {
				// update the session is_pinned
				$_SESSION['edit_announcement']['is_pinned'] = $is_pinned;

				// notify user
				$update_msg[] = 'Pin status successfully edited.';
			} else {
				$errors[] = 'Error editing pin status! Please try again later.';
			}
		}

		// convert and persist the date
		$date = date("Y-m-d", strtotime($date));

	} else {
		header('Location: announcements.php');
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<div class="content-wrap">
		<section class="container">
			<h3 class="center brand-text">Edit Announcement</h3>
			<form action="edit_announcement.php" method="POST">
				<div class="center">
        			<button type="submit" name="cancel" value="cancel" class="btn z-depth-0 red" formnovalidate>Stop Editing<i class="material-icons left">chevron_left</i></button>
					<button type="submit" name="save_changes" value="save" class="btn z-depth-0 green">Save Changes<i class="material-icons right">check</i></button>
        		</div>
				<div class="card white">
			        <div class="card-content">
		        		<h5 class="center grey-text">Announcement Details</h5>
		        		<div class="row">
		        			<?php if (isset($update_msg)) : ?>
				        		<ul class="browser-default update-msg">
				        			<?php foreach ($update_msg as $msg) : ?>
				        				<li><?php echo $msg; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>
							<?php if (isset($errors)) : ?>
				        		<ul class="browser-default error-msg">
				        			<?php foreach ($errors as $error) : ?>
				        				<li><?php echo $error; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>

							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<input id="title" type="text" name="title" value="<?php echo isset($title) ? $title : '' ?>" class="validate" required>
								<label for="title">Title</label>
								<span id="title_helper" class="helper-text" data-error="Required field."/>
							</div>

							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<textarea id="description" name="description" class="materialize-textarea validate" required><?php echo isset($description) ? $description : '' ?></textarea>
								<label for="description">Description</label>
								<span id="description" class="helper-text" data-error="Required field."/>
							</div>

							<div class="input-field col s4 offset-s4">
								<input id="date" type="date" name="date" class="validate" value="<?php echo isset($date) ? $date : '' ?>" required>
								<label for="date">Expiry Date</label>
								<span id="date_helper" class="helper-text" data-error="Required field."/>
							</div>

							<div class="col s12 center">
								<label>
									<input type="checkbox" name="pin" <?php echo $is_pinned ? "checked='checked'" : ''; ?>/>
									<span>Pin announcement?</span>
								</label>
							</div>
						</div> <!-- end of div.row -->
						<div class="col s12">
							<p class="grey-text">User ID: <?php echo $user_id; ?></p>
						</div>
						<div class="col s12">
							<p class="grey-text">Recorded at: <?php echo date("h:i a, d/m/Y", strtotime($announcement_date)); ?></p>
						</div>
			        </div>	<!-- end of div.card-content -->
			    </div>
		    </form>
		</section>
	</div>

	<?php include('../templates/footer.php') ?>

</html>
