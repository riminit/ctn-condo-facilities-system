<?php 
  //Connect the database
  include('../config/db_connect.php');
  // include database functions
  include('../includes/db_functions.php');

  // start session if session hasnt been started/$_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  if (isset($_SESSION['add_reservation'])) { unset($_SESSION['add_reservation']); }
  if (isset($_SESSION['edit_reservation'])) { unset($_SESSION['edit_reservation']); }
  if (isset($_SESSION['reservation_details'])) { unset($_SESSION['reservation_details']); }
  
  // if user clicked delete button
  if (isset($_POST['delete_reservation'])) {
    $reservation_id = $_POST['delete_reservation'];
    $sql = "DELETE FROM `RESERVATION` 
            WHERE `reservation_id`='$reservation_id'";

    // if reservation successfully deleted
    if (execute_query($conn, $sql)) {
      // notify and redirect user to announcements page
      $update_msg[] = 'Reservation successfully deleted.';

    } else {
      $errors[] = 'Error deleting reservation! Please try again later.';
    }
  }

  // if user clicked active button
  if (isset($_POST['status_active'])) {
    $reservation_id = $_POST['status_active'];

    // query to update row in RESERVATION table
    $sql = "UPDATE `RESERVATION` 
        SET `status`='0' 
        WHERE `reservation_id`='$reservation_id'";

    // if status successfully updated
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Reservation successfully cancelled.';
    } else {
      $errors[] = 'Error setting reservation to cancelled! Please try again later.';
    }
  }

  // if user clicked cancelled button
  if (isset($_POST['status_cancelled'])) {
    $reservation_id = $_POST['status_cancelled'];

    // query for getting the reservation details
    $sql = "SELECT `start_datetime`, `end_datetime`, `facility_id`
            FROM `RESERVATION` 
            WHERE `reservation_id` = '$reservation_id'";
    
    // get result in array
    $reservation = select_single($conn, $sql);
    $facility_id = $reservation['facility_id'];
    $start_datetime = $reservation['start_datetime'];
    $end_datetime = $reservation['end_datetime'];

    // Check if there are reservation clashes
    // https://stackoverflow.com/a/49089737
    $sql = "SELECT COUNT(*) AS `count` 
        FROM `RESERVATION` 
        WHERE `facility_id` = '$facility_id' 
        AND `reservation_id` != '$reservation_id'
        AND `status` = '1'
        AND (
        (`start_datetime` BETWEEN '$start_datetime' AND '$end_datetime' OR `end_datetime` BETWEEN '$start_datetime' AND '$end_datetime' )
        OR
        ('$start_datetime' BETWEEN `start_datetime` AND `end_datetime` OR '$end_datetime' BETWEEN `start_datetime` AND `end_datetime`)
        );";

    // get result in array
    $clashes = select_single($conn, $sql);

    if ($clashes['count']>0) {
      $errors[] = "There are time clashes with an active reservation.";
    }

    if (!isset($errors)) {
      // query to update row in RESERVATION table
      $sql = "UPDATE `RESERVATION` 
          SET `status`='1' 
          WHERE `reservation_id`='$reservation_id'";

      // if status successfully updated
      if (execute_query($conn, $sql)) {
        // notify user
        $update_msg[] = 'Reservation successfully activated.';
      } else {
        $errors[] = 'Error activating reservation! Please try again later.';
      }
    }
  }

  if (isset($_POST['activate_checked'])) {
    // Getting the checked reservations
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query for getting the reservation details
        $sql = "SELECT `start_datetime`, `end_datetime`, `facility_id`
                FROM `RESERVATION` 
                WHERE `reservation_id` = '$key'";
        
        // get result in array
        $reservation = select_single($conn, $sql);
        $facility_id = $reservation['facility_id'];
        $start_datetime = $reservation['start_datetime'];
        $end_datetime = $reservation['end_datetime'];

        // Check if there are reservation clashes
        // https://stackoverflow.com/a/49089737
        $sql = "SELECT COUNT(*) AS `count` 
            FROM `RESERVATION` 
            WHERE `facility_id` = '$facility_id' 
            AND `reservation_id` != '$key'
            AND `status` = '1'
            AND (
            (`start_datetime` BETWEEN '$start_datetime' AND '$end_datetime' OR `end_datetime` BETWEEN '$start_datetime' AND '$end_datetime' )
            OR
            ('$start_datetime' BETWEEN `start_datetime` AND `end_datetime` OR '$end_datetime' BETWEEN `start_datetime` AND `end_datetime`)
            );";

        // get result in array
        $clashes = select_single($conn, $sql);

        if ($clashes['count']>0) {
          $errors[] = "There are time clashes with an active reservation.";
        }

        if (!isset($errors)) {
          // query to update row in RESERVATION table
          $sql = "UPDATE `RESERVATION` 
              SET `status`='1' 
              WHERE `reservation_id`='$key'";

          // if status successfully updated
          if (execute_query($conn, $sql)) {
            // notify user
            $success = true;
          } else {
            $success = false;
            $errors[] = 'Error setting reservation to active! Please try again later.';
            break;
          }
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Reservation(s) successfully activated.';
    }
  }

  if (isset($_POST['cancel_checked'])) {
    // Getting the checked accounts
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in RESERVATION table
        $sql = "UPDATE `RESERVATION` 
            SET `status`='0' 
            WHERE `reservation_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error cancelling reservation(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Reservation(s) successfully cancelled.';
    }
  }

  if (isset($_POST['delete_checked'])) {
    // Getting the checked reservations
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to delete row in RESERVATION table
        $sql = "DELETE FROM `RESERVATION` 
            WHERE `reservation_id`='$key'";

        // if reservation successfully deleted
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error deleting reservation(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Reservation(s) successfully deleted.';
    }
  }

  // SORT & DISPLAY TABLE
  // get the column name
  $col_num = isset($_GET['column']) ? $_GET['column'] : 0;
  switch ($col_num) {
    case 1:
      $column = 'facility_name';
      break;
    
    case 2:
      $column = 'name';
      break;
    
    case 3:
      $column = 'start_datetime';
      break;
    
    case 4:
      $column = 'end_datetime';
      break;

    case 5:
      $column = 'RESERVATION.status';
      break;

    default:
      $column = 'reservation_id';
      break;
  }

  // Reference: https://codeshack.io/how-to-sort-table-columns-php-mysql/
  $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'asc' ? 'ASC' : 'DESC';

  // Manually sort status
  if ($column == 'RESERVATION.status') {
    // query for getting all the reservations in the order of latest to oldest
    $sql = "SELECT `reservation_id`,`facility_name`,`name`, `start_datetime`, `end_datetime`, RESERVATION.status
            FROM `RESERVATION` 
            INNER JOIN `FACILITY` ON RESERVATION.facility_id = FACILITY.facility_id 
            ORDER BY `start_datetime` DESC";

    $reservations = select_multiple($conn, $sql);

    $active_reservations = array();
    $cancelled_reservations = array();
    $past_reservations = array();

    foreach ($reservations as $reservation) {
      // if reservation is still active
      // https://stackoverflow.com/questions/2832467/how-can-i-check-if-the-current-date-time-is-past-a-set-date-time
      if (new DateTime() < new DateTime($reservation['end_datetime'])) {
        // if not cancelled / rejected
        if ($reservation['status']) {
          $active_reservations[] = $reservation;
        } else {
          $cancelled_reservations[] = $reservation;
        }
          
      } else {
        $past_reservations[] = $reservation;
      }
    }

    if ($sort_order=='ASC') {
      // Active > Cancelled > Inactive
      $reservations = $active_reservations;
      $reservations = array_merge($reservations, $cancelled_reservations);
      $reservations = array_merge($reservations, $past_reservations);

    } else {
      // Inactive > Cancelled > Active
      $reservations = $past_reservations;
      $reservations = array_merge($reservations, $cancelled_reservations);
      $reservations = array_merge($reservations, $active_reservations);
    }

  } else {
    // query for getting all the reservations in the order of the date
    $sql = "SELECT `reservation_id`,`facility_name`,`name`, `start_datetime`, `end_datetime`, RESERVATION.status
            FROM `RESERVATION` 
            INNER JOIN `FACILITY` ON RESERVATION.facility_id = FACILITY.facility_id 
            ORDER BY " . $column . ' ' . $sort_order;

    $reservations = select_multiple($conn, $sql);
  }

  $up_or_down = str_replace(array('ASC','DESC'), array('keyboard_arrow_up','keyboard_arrow_down'), $sort_order); 
  $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';

  mysqli_close($conn);
?> 

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <div class="row">
      <h3 class="center brand-text">Reservations</h3>
      <div class="input-field col s4 offset-s2">
        <select id="filterOption" name="filterOption">
          <option value="1" selected>ID</option>
          <option value="2">Facility Name</option>
          <option value="3">Reserved By</option>
          <option value="4">Date</option>
          <option value="5">Start Time</option>
          <option value="6">End Time</option>
          <option value="7">Status</option>
        </select>
        <label>Filter By</label>
      </div>
      <div class="input-field col s4">
        <i class="material-icons prefix">search</i>
        <input type="text" id="searchInput" onkeyup="searchFilter()">
        <label id="searchLabel" for="searchInput">Search ID</label>
      </div>
      <?php if (isset($_GET['column']) && isset($_GET['order'])) : ?>
        <form action="reservations.php?column=<?php echo $col_num; ?>&order=<?php echo $sort_order; ?>" method="POST">
      <?php else : ?>
        <form action="reservations.php" method="POST">
      <?php endif ?>
        <div class="card white col s10 offset-s1">
          <div class="card-content">
            <div class="center">
              <button disabled type="submit" name="activate_checked" value="activate_checked" class="btn z-depth-0 green checked-btn">Activate Reservation<i class="material-icons left">check</i></button>
              <button disabled type="submit" name="cancel_checked" value="cancel_checked" class="btn z-depth-0 red checked-btn">Cancel Reservation<i class="material-icons left">clear</i></button>
              <button disabled type="submit" name="delete_checked" value="delete_checked" class="btn z-depth-0 red checked-btn" onClick="javascript: return confirm('Are you sure you want to delete the selected reservation(s)?');">Delete<i class="material-icons left">delete</i></button>
              <button type="submit" name="add_reservation" value="add_reservation" class="btn z-depth-0 brand-dark" formaction="make_reservation.php">Add New<i class="material-icons left">add</i></button>
            </div>
            <?php if (isset($update_msg)) : ?>
              <div class="container">
                <ul class="browser-default update-msg">
                  <?php foreach ($update_msg as $msg) : ?>
                    <li><?php echo $msg; ?></li>
                  <?php endforeach ?>
                </ul>
              </div>
            <?php endif ?>
            <?php if (isset($errors)) : ?>
              <ul class="browser-default error-msg">
                <?php foreach ($errors as $error) : ?>
                  <li><?php echo $error; ?></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
            <table id="myTable" class="responsive-table highlight centered">
              <thead>
                <tr>
                  <th><label><input type="checkbox" class="chkAll" /><span></span></label></th>
                  <th><a href="reservations.php?column=0&order=<?php echo $asc_or_desc; ?>">ID<i class="material-icons tiny"><?php echo $column == 'reservation_id' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="reservations.php?column=1&order=<?php echo $asc_or_desc; ?>">Facility Name<i class="material-icons tiny"><?php echo $column == 'facility_name' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="reservations.php?column=2&order=<?php echo $asc_or_desc; ?>">Reserved By<i class="material-icons tiny"><?php echo $column == 'name' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="reservations.php?column=3&order=<?php echo $asc_or_desc; ?>">Date<i class="material-icons tiny"><?php echo $column == 'start_datetime' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="reservations.php?column=3&order=<?php echo $asc_or_desc; ?>">From<i class="material-icons tiny"><?php echo $column == 'start_datetime' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="reservations.php?column=4&order=<?php echo $asc_or_desc; ?>">To<i class="material-icons tiny"><?php echo $column == 'end_datetime' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="reservations.php?column=5&order=<?php echo $asc_or_desc; ?>">Status<i class="material-icons tiny"><?php echo $column == 'RESERVATION.status' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($reservations as $reservation) : ?>  
                  <tr>
                    <td><label><input type="checkbox" name="<?php echo $reservation['reservation_id'] ?>" class="chkboxes" /><span></span></label></td>
                    <td><?php echo $reservation['reservation_id']; ?></td>
                    <td><?php echo $reservation['facility_name']; ?></td>
                    <td class="truncate" style="max-width: 10rem"><?php echo $reservation['name']; ?></td>
                    <td><?php echo date("d/m/Y", strtotime($reservation['start_datetime'])); ?></td>
                    <td><?php echo date("h:i a", strtotime($reservation['start_datetime'])); ?></td>
                    <td><?php echo date("h:i a", strtotime($reservation['end_datetime'])); ?></td>
                    <td>
                      <?php if (new DateTime() < new DateTime($reservation['end_datetime'])) : ?>
                        <?php if ($reservation['status']) : ?>
                          <button type="submit" name="status_active" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 green">Active</button>
                        <?php else : ?>
                          <button type="submit" name="status_cancelled" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 red">Cancelled</button>
                        <?php endif ?>
                      <?php else : ?>
                        <p class="red-text">Inactive</p>
                      <?php endif ?>
                    </td>
                    <td>
                      <button type="submit" name="edit_reservation" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 brand-dark" formaction="make_reservation.php">Edit<i class="material-icons left">edit</i></button>
                    </td>
                    <td>
                      <button type="submit" name="delete_reservation" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this reservation?');">Delete<i class="material-icons left">delete</i></button>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php include('../templates/footer.php') ?>
</html>
