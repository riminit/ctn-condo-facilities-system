<?php 
  //Connect the database
  include('../config/db_connect.php');
  // include database functions
  include('../includes/db_functions.php');

  // start session if session hasnt been started/$_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  if (isset($_SESSION['edit_announcement'])) { unset($_SESSION['edit_announcement']); }
  
  // if user clicked delete button
  if (isset($_POST['delete_announcement'])) {
    $announcement_id = $_POST['delete_announcement'];
    $sql = "DELETE FROM `ANNOUNCEMENT` 
            WHERE `announcement_id`='$announcement_id'";

    // if announcement successfully deleted
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Announcement successfully deleted.';

    } else {
      $errors[] = 'Error deleting announcement! Please try again later.';
    }
  }

  // if user clicked yes button
  if (isset($_POST['pinned'])) {
    $announcement_id = $_POST['pinned'];

    // query to update row in ANNOUNCEMENT table
    $sql = "UPDATE `ANNOUNCEMENT` 
        SET `is_pinned`='0' 
        WHERE `announcement_id`='$announcement_id'";

    // if is_pinned successfully updated
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Announcement successfully unpinned.';
    } else {
      $errors[] = 'Error unpinning announcement! Please try again later.';
    }
  }

  // if user clicked no button
  if (isset($_POST['not_pinned'])) {
    $announcement_id = $_POST['not_pinned'];

    // query to update row in ANNOUNCEMENT table
    $sql = "UPDATE `ANNOUNCEMENT` 
        SET `is_pinned`='1' 
        WHERE `announcement_id`='$announcement_id'";

    // if is_pinned successfully updated
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Announcement successfully pinned.';
    } else {
      $errors[] = 'Error pinning announcement! Please try again later.';
    }
  }

  if (isset($_POST['pin_checked'])) {
    // Getting the checked announcements
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in ANNOUNCEMENT table
        $sql = "UPDATE `ANNOUNCEMENT` 
            SET `is_pinned`='1' 
            WHERE `announcement_id`='$key'";

        // if is_pinned successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error pinning announcement(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Announcement(s) successfully pinned.';
    }
  }

  if (isset($_POST['unpin_checked'])) {
    // Getting the checked announcements
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in ANNOUNCEMENT table
        $sql = "UPDATE `ANNOUNCEMENT` 
            SET `is_pinned`='0' 
            WHERE `announcement_id`='$key'";

        // if is_pinned successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error unpinning announcement(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Announcement(s) successfully unpinned.';
    }
  }

  if (isset($_POST['delete_checked'])) {
    // Getting the checked announcements
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to delete row in ANNOUNCEMENT table
        $sql = "DELETE FROM `ANNOUNCEMENT` 
            WHERE `announcement_id`='$key'";

        // if announcement successfully deleted
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error deleting announcement(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Announcement(s) successfully deleted.';
    }
  }

  // SORT & DISPLAY TABLE
  // get the column name
  $col_num = isset($_GET['column']) ? $_GET['column'] : 0;
  switch ($col_num) {
    case 1:
      $column = 'ic_name';
      break;
    
    case 2:
      $column = 'announcement_title';
      break;
    
    case 3:
      $column = 'due_date';
      break;
    
    case 4:
      $column = 'is_pinned';
      break;

    default:
      $column = 'announcement_id';
      break;
  }

  // Reference: https://codeshack.io/how-to-sort-table-columns-php-mysql/
  $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'asc' ? 'ASC' : 'DESC';

  // query for getting all the announcements
  $sql = "SELECT `announcement_id`,`announcement_title`,`due_date`,`is_pinned`,`ic_name` 
          FROM `ANNOUNCEMENT` INNER JOIN `USER` ON ANNOUNCEMENT.user_id = USER.user_id 
          ORDER BY " . $column . ' ' . $sort_order;

  $announcements = select_multiple($conn, $sql);

  $up_or_down = str_replace(array('ASC','DESC'), array('keyboard_arrow_up','keyboard_arrow_down'), $sort_order); 
  $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';

  mysqli_close($conn);
?> 

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <div class="row">
      <h3 class="center brand-text">Announcements</h3>
      <div class="input-field col s4 offset-s2">
        <select id="filterOption" name="filterOption">
          <option value="1" selected>ID</option>
          <option value="2">Announced By</option>
          <option value="3">Title</option>
          <option value="4">Status</option>
          <option value="5">Is Pinned</option>
        </select>
        <label>Filter By</label>
      </div>
      <div class="input-field col s4">
        <i class="material-icons prefix">search</i>
        <input type="text" id="searchInput" onkeyup="searchFilter()">
        <label id="searchLabel" for="searchInput">Search ID</label>
      </div>
      <?php if (isset($_GET['column']) && isset($_GET['order'])) : ?>
        <form action="announcements.php?column=<?php echo $col_num; ?>&order=<?php echo $sort_order; ?>" method="POST">
      <?php else : ?>
        <form action="announcements.php" method="POST">
      <?php endif ?>
        <div class="card white col s10 offset-s1">
          <div class="card-content">
            <div class="center">
              <button disabled type="submit" name="pin_checked" value="pin_checked" class="btn z-depth-0 green checked-btn">Pin<i class="material-icons left">place</i></button>
              <button disabled type="submit" name="unpin_checked" value="unpin_checked" class="btn z-depth-0 red checked-btn">Unpin<i class="material-icons left">location_off</i></button>
              <button disabled type="submit" name="delete_checked" value="delete_checked" class="btn z-depth-0 red checked-btn" onClick="javascript: return confirm('Are you sure you want to delete the selected announcement(s)?');">Delete<i class="material-icons left">delete</i></button>
              <button type="submit" name="add_announcement" value="add_announcement" class="btn z-depth-0 brand-dark" formaction="add_announcement.php">Add New<i class="material-icons left">add</i></button>
            </div>
            <?php if (isset($update_msg)) : ?>
              <div class="container">
                <ul class="browser-default update-msg">
                  <?php foreach ($update_msg as $msg) : ?>
                    <li><?php echo $msg; ?></li>
                  <?php endforeach ?>
                </ul>
              </div>
            <?php endif ?>
            <?php if (isset($errors)) : ?>
              <ul class="browser-default error-msg">
                <?php foreach ($errors as $error) : ?>
                  <li><?php echo $error; ?></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
            <table id="myTable" class="responsive-table highlight centered">
              <thead>
                <tr>
                  <th><label><input type="checkbox" class="chkAll" /><span></span></label></th>
                  <th><a href="announcements.php?column=0&order=<?php echo $asc_or_desc; ?>">ID<i class="material-icons tiny"><?php echo $column == 'announcement_id' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="announcements.php?column=1&order=<?php echo $asc_or_desc; ?>">Announced by<i class="material-icons tiny"><?php echo $column == 'ic_name' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th class="truncate" style="max-width: 10rem"><a href="announcements.php?column=2&order=<?php echo $asc_or_desc; ?>">Title<i class="material-icons tiny"><?php echo $column == 'announcement_title' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="announcements.php?column=3&order=<?php echo $asc_or_desc; ?>">Status<i class="material-icons tiny"><?php echo $column == 'due_date' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="announcements.php?column=4&order=<?php echo $asc_or_desc; ?>">Is Pinned<i class="material-icons tiny"><?php echo $column == 'is_pinned' ? $up_or_down : 'unfold_more'; ?></i></a></th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($announcements as $announcement) : ?>  
                  <tr>
                    <td><label><input type="checkbox" name="<?php echo $announcement['announcement_id'] ?>" class="chkboxes" /><span></span></label></td>
                    <td><?php echo $announcement['announcement_id']; ?></td>
                    <td><?php echo $announcement['ic_name']; ?></td>
                    <td class="truncate" style="max-width: 10rem"><?php echo $announcement['announcement_title']; ?></td>
                    <?php if (new DateTime() < new DateTime($announcement['due_date'])) : ?>
                      <td class="green-text">Active</td>
                    <?php else : ?>
                      <td class="red-text">Inactive</td>
                    <?php endif ?>
                    <td>
                      <?php if ($announcement['is_pinned']) : ?>
                        <button type="submit" name="pinned" value="<?php echo $announcement['announcement_id']; ?>" class="btn z-depth-0 green">Yes</button>
                      <?php else : ?>
                        <button type="submit" name="not_pinned" value="<?php echo $announcement['announcement_id']; ?>" class="btn z-depth-0 red">No</button>
                      <?php endif ?>
                    </td>
                    <td>
                      <button type="submit" name="edit_announcement" value="<?php echo $announcement['announcement_id']; ?>" class="btn z-depth-0 brand-dark" formaction="edit_announcement.php">Edit<i class="material-icons left">edit</i></button>
                    </td>
                    <td>
                      <button type="submit" name="delete_announcement" value="<?php echo $announcement['announcement_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this announcement?');">Delete<i class="material-icons left">delete</i></button>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php include('../templates/footer.php') ?>
</html>
