<?php 
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	//Connect the database
	include('../config/db_connect.php');
	// include database functions
	include('../includes/db_functions.php');


	// if user is still editing
	if (isset($_SESSION['edit_admin'])) {
		$admin = $_SESSION['edit_admin'];
		$user_id = $admin['user_id'];
		$username = $admin['username'];
		$contact = $admin['contact_num'];
		$ic_num = $admin['ic_num'];
		$ic_name = $admin['ic_name'];
	}

	if (isset($_POST['edit_admin'])) {
		// if user clicked edit button
		$user_id = $_POST['edit_admin'];

		// query for getting the account details
		$sql = "SELECT  `username`, `ic_num`, `ic_name`, `contact_num` 
		        FROM `USER` 
		        WHERE `user_id`='$user_id'";

		$admin = select_single($conn, $sql);
		$username = $admin['username'];
		$contact = '0'.$admin['contact_num'];
		$ic_num = $admin['ic_num'];
		$ic_name = $admin['ic_name'];

		$_SESSION['edit_admin'] = $admin;
		$_SESSION['edit_admin']['user_id'] = $user_id;
		$_SESSION['edit_admin']['contact_num'] = $contact;

		$editable = "";
	} elseif (isset($_POST['cancel'])) {
		header("Location: admin_accounts.php");

	} elseif (isset($_POST['save_changes'])) {
		$username = htmlspecialchars($_POST['username']);
		$contact = htmlspecialchars($_POST['contact']);
		$ic_num = htmlspecialchars($_POST['ic_num']);
		$ic_name = htmlspecialchars($_POST['ic_name']);

		// if username was changed
		if ($username != $_SESSION['edit_admin']['username'] ) {
			$username = mysqli_real_escape_string($conn, $username);

			// query for getting data of any user with the same name
			$sql = "SELECT * 
					FROM `USER` 
					WHERE `username`='$username'";

			// get result in array
			$same_username = select_single($conn, $sql);

			// if username has been taken
			if (sizeof($same_username)) {
				$errors[] = "Username has been taken.";

			} else {
				// query to update row in USER table
				$sql = "UPDATE `USER` 
						SET `username`='$username' 
						WHERE `user_id`='$user_id'";

				// if username successfully updated
				if (execute_query($conn, $sql)) {
					// update the session username
					$_SESSION['edit_admin']['username'] = $username;

					// update the current session details if editing their own account
					if ($user_id == $_SESSION['user_details']['user_id']) {
						$_SESSION['user_details']['username'] = $username;
					}

					// notify user
					$update_msg[] = 'Username successfully edited.';
				} else {
					$errors[] = 'Error editing username! Please try again later.';
				}
			}
		}
		
		// if contact number was changed
		if ($contact != $_SESSION['edit_admin']['contact_num'] ) {
			$contact = mysqli_real_escape_string($conn, $contact);

			// query to update row in USER table
			$sql = "UPDATE `USER` 
					SET `contact_num`='$contact' 
					WHERE `user_id`='$user_id'";

			// if contact number successfully updated
			if (execute_query($conn, $sql)) {
				// update the session contact number
				$_SESSION['edit_admin']['contact_num'] = $contact;

				// update the current session details if editing their own account
				if ($user_id == $_SESSION['user_details']['user_id']) {
					$_SESSION['user_details']['contact_num'] = $contact;
				}

				// notify user
				$update_msg[] = 'Contact number successfully edited.';
			} else {
				$errors[] = 'Error editing contact number! Please try again later.';
			}
		}

		// if ic_num was changed
		if ($ic_num != $_SESSION['edit_admin']['ic_num'] ) {
			$ic_num = mysqli_real_escape_string($conn, $ic_num);

			// query for getting data of any user with the same ic number
			$sql = "SELECT * 
					FROM `USER` 
					WHERE `ic_num`='$ic_num'";

			// get result in array
			$same_ic_num = select_single($conn, $sql);

			// if there is an existing user with the same ic number
			if (sizeof($same_ic_num)) {
				$errors[] = "There is already an existing user with the same MyKad number.";

			} else {
				// query to update row in USER table
				$sql = "UPDATE `USER` 
						SET `ic_num`='$ic_num' 
						WHERE `user_id`='$user_id'";

				// if ic_num successfully updated
				if (execute_query($conn, $sql)) {
					// update the session ic_num
					$_SESSION['edit_admin']['ic_num'] = $ic_num;

					// update the current session details if editing their own account
					if ($user_id == $_SESSION['user_details']['user_id']) {
						$_SESSION['user_details']['ic_num'] = $ic_num;
					}

					// notify user
					$update_msg[] = 'MyKad number successfully edited.';
				} else {
					$errors[] = 'Error editing MyKad number! Please try again later.';
				}
			}
		}

		// if ic_name was changed
		if ($ic_name != $_SESSION['edit_admin']['ic_name'] ) {
			$ic_name = mysqli_real_escape_string($conn, $ic_name);

			// query to update row in USER table
			$sql = "UPDATE `USER` 
					SET `ic_name`='$ic_name' 
					WHERE `user_id`='$user_id'";

			// if ic_name successfully updated
			if (execute_query($conn, $sql)) {
				// update the session ic_name
				$_SESSION['edit_admin']['ic_name'] = $ic_name;

				// update the current session details if editing their own account
				if ($user_id == $_SESSION['user_details']['user_id']) {
					$_SESSION['user_details']['ic_name'] = $ic_name;
				}

				// notify user
				$update_msg[] = 'Full name successfully edited.';
			} else {
				$errors[] = 'Error editing full name! Please try again later.';
			}
		}

		$_SESSION['edit_msgs']['update_msg'] = isset($update_msg) ? $update_msg : null;
		$_SESSION['edit_msgs']['errors'] = isset($errors) ? $errors : null;

		$editable = "";

	} elseif (isset($_POST['change_pwd'])) {
		$_SESSION['change_pwd'] = "change_pwd";
		$editable = "";

	} elseif (isset($_POST['cancel_pwd'])) {
		unset($_SESSION['change_pwd']);
		$editable = "";

	} elseif (isset($_POST['save_pwd'])) {
		$pwd = htmlspecialchars($_POST['pwd']);
		$pwd = mysqli_real_escape_string($conn, $pwd);

		// password hashing
		$hash = password_hash($pwd, PASSWORD_DEFAULT);

		// query to update row in USER table
		$sql = "UPDATE `USER` 
				SET `password`='$hash' 
				WHERE `user_id`='$user_id'";

		// if contact number successfully updated
		if (execute_query($conn, $sql)) {
			// notify user
			$update_msg[] = 'Password changed successfully.';
		} else {
			$errors[] = 'Error changing password! Please try again later.';
		}

		unset($_SESSION['change_pwd']);
		$editable = "";
	} else {
		header('Location: admin_accounts.php');
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<section class="container content-wrap">
		<h3 class="center brand-text">Edit <?php echo $username ?>'s Account</h3>
		<form action="edit_admin.php" method="POST">
			<?php include('edit_admin_form.php') ?>
		</form>
	</section>

	<?php include('../templates/footer.php') ?>
</html>