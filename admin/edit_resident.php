<?php 
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// Connect the database
	include('../config/db_connect.php');
	// include database functions
	include('../includes/db_functions.php');

	// get the states from config
	$config = include('../config/config.php');
	$states = $config['states'];

	// if user is still editing
	if (isset($_SESSION['edit_resident'])) {
		$resident = $_SESSION['edit_resident'];
		$user_id = $resident['user_id'];
		$username = $resident['username'];
		$contact = $resident['contact_num'];
		$ic_num = $resident['ic_num'];
		$ic_name = $resident['ic_name'];

		$current_address_details = $resident['current_address'];
		$current_level = $current_address_details['level'];
		$current_unit = $current_address_details['unit'];
		$current_address = $current_address_details['address'];
		$current_postcode = $current_address_details['postcode'];
		$current_city = $current_address_details['city'];
		$current_state = $current_address_details['state'];

		if (isset($resident['alt_address'])) {
			$alt_address_details = $resident['alt_address'];
			$alt_level = $alt_address_details['level'];
			$alt_unit = $alt_address_details['unit'];
			$alt_address = $alt_address_details['address'];
			$alt_postcode = $alt_address_details['postcode'];
			$alt_city = $alt_address_details['city'];
			$alt_state = $alt_address_details['state'];
		}
	}

	if (isset($_POST['edit_resident'])) {
		// if user clicked edit button
		$user_id = $_POST['edit_resident'];

		// query for getting the account details
		$sql = "SELECT  `username`, `ic_num`, `ic_name`, `contact_num` 
		        FROM `USER` 
		        WHERE `user_id`='$user_id'";

		$resident = select_single($conn, $sql);
		$username = $resident['username'];
		$contact = '0'.$resident['contact_num'];
		$ic_num = $resident['ic_num'];
		$ic_name = $resident['ic_name'];

		$_SESSION['edit_resident'] = $resident;
		$_SESSION['edit_resident']['user_id'] = $user_id;
		$_SESSION['edit_resident']['contact_num'] = $contact;

		// query for getting the address details
		$sql = "SELECT *
				FROM `ADDRESS`
				WHERE `user_id`='$user_id'";

		// get address(es)
		$addresses = select_multiple($conn, $sql);

		foreach ($addresses as $addr) {
			if ($addr['type'] == 'current') {
				$current_address_details = $addr;
				$current_level = $current_address_details['level'];
				$current_unit = $current_address_details['unit'];
				$current_address = $current_address_details['address'];
				$current_postcode = $current_address_details['postcode'];
				$current_city = $current_address_details['city'];
				$current_state = $current_address_details['state'];

				$_SESSION['edit_resident']['current_address'] = $current_address_details;
			} else {
				$alt_address_details = $addr;
				$alt_level = $alt_address_details['level'];
				$alt_unit = $alt_address_details['unit'];
				$alt_address = $alt_address_details['address'];
				$alt_postcode = $alt_address_details['postcode'];
				$alt_city = $alt_address_details['city'];
				$alt_state = $alt_address_details['state'];

				$_SESSION['edit_resident']['alt_address'] = $alt_address_details;
				$_SESSION['update_alt'] = "update_alt";
			}
		}
		
	} elseif (isset($_POST['cancel'])) {
		header("Location: resident_accounts.php");

	} elseif (isset($_POST['save_changes'])) {
		$username = htmlspecialchars($_POST['username']);
		$contact = htmlspecialchars($_POST['contact']);
		$ic_num = htmlspecialchars($_POST['ic_num']);
		$ic_name = htmlspecialchars($_POST['ic_name']);

		// if username was changed
		if ($username != $_SESSION['edit_resident']['username'] ) {
			$username = mysqli_real_escape_string($conn, $username);

			// query for getting data of any user with the same name
			$sql = "SELECT * 
					FROM `USER` 
					WHERE `username`='$username'";

			// get result in array
			$same_username = select_single($conn, $sql);

			// if username has been taken
			if (sizeof($same_username)) {
				$errors[] = "Username has been taken.";

			} else {
				// query to update row in USER table
				$sql = "UPDATE `USER` 
						SET `username`='$username' 
						WHERE `user_id`='$user_id'";

				// if username successfully updated
				if (execute_query($conn, $sql)) {
					// update the session username
					$_SESSION['edit_resident']['username'] = $username;

					// notify user
					$update_msg[] = 'Username successfully edited.';
				} else {
					$errors[] = 'Error editing username! Please try again later.';
				}
			}
		}
		
		// if contact number was changed
		if ($contact != "0".$_SESSION['edit_resident']['contact_num'] ) {
			$contact = mysqli_real_escape_string($conn, $contact);

			// query to update row in USER table
			$sql = "UPDATE `USER` 
					SET `contact_num`='$contact' 
					WHERE `user_id`='$user_id'";

			// if contact number successfully updated
			if (execute_query($conn, $sql)) {
				// update the session contact number
				$_SESSION['edit_resident']['contact_num'] = $contact;

				// notify user
				$update_msg[] = 'Contact number successfully edited.';
			} else {
				$errors[] = 'Error editing contact number! Please try again later.';
			}
		}

		// if ic_num was changed
		if ($ic_num != $_SESSION['edit_resident']['ic_num'] ) {
			$ic_num = mysqli_real_escape_string($conn, $ic_num);

			// query for getting data of any user with the same ic number
			$sql = "SELECT * 
					FROM `USER` 
					WHERE `ic_num`='$ic_num'";

			// get result in array
			$same_ic_num = select_single($conn, $sql);

			// if there is an existing user with the same ic number
			if (sizeof($same_ic_num)) {
				$errors[] = "There is already an existing user with the same MyKad number.";

			} else {
				// query to update row in USER table
				$sql = "UPDATE `USER` 
						SET `ic_num`='$ic_num' 
						WHERE `user_id`='$user_id'";

				// if ic_num successfully updated
				if (execute_query($conn, $sql)) {
					// update the session ic_num
					$_SESSION['edit_resident']['ic_num'] = $ic_num;

					// notify user
					$update_msg[] = 'MyKad number successfully edited.';
				} else {
					$errors[] = 'Error editing MyKad number! Please try again later.';
				}
			}
		}

		// if ic_name was changed
		if ($ic_name != $_SESSION['edit_resident']['ic_name'] ) {
			$ic_name = mysqli_real_escape_string($conn, $ic_name);

			// query to update row in USER table
			$sql = "UPDATE `USER` 
					SET `ic_name`='$ic_name' 
					WHERE `user_id`='$user_id'";

			// if ic_name successfully updated
			if (execute_query($conn, $sql)) {
				// update the session ic_name
				$_SESSION['edit_resident']['ic_name'] = $ic_name;

				// notify user
				$update_msg[] = 'Full name successfully edited.';
			} else {
				$errors[] = 'Error editing full name! Please try again later.';
			}
		}

		$level = htmlspecialchars($_POST['level']);
		$level = mysqli_real_escape_string($conn, $level);

		$unit = htmlspecialchars($_POST['unit']);
		$unit = mysqli_real_escape_string($conn, $unit);

		// if current address details were changed
		if (($current_level != $level) || ($current_unit != $unit)) {
			$current_address_id = $current_address_details['address_id'];

			$current_level = mysqli_real_escape_string($conn, $current_level);
			$current_unit = mysqli_real_escape_string($conn, $current_unit);

			// query to update row in ADDRESS table
			$sql = "UPDATE `ADDRESS` 
					SET `level`='$current_level', `unit`='$current_unit' 
					WHERE `address_id`='$current_address_id'";

			// if contact number successfully updated
			if (execute_query($conn, $sql)) {
				// notify user
				$update_msg[] = 'Current address successfully edited.';
			} else {
				$errors[] = 'Error editing current address! Please try again later.';
			}
		}

		if (isset($_SESSION['add_alt']) || isset($_SESSION['update_alt'])) {
			$alt_level = htmlspecialchars($_POST['alt_level']);
			$alt_level = mysqli_real_escape_string($conn, $alt_level);

			$alt_unit = htmlspecialchars($_POST['alt_unit']);
			$alt_unit = mysqli_real_escape_string($conn, $alt_unit);

			$alt_address = htmlspecialchars($_POST['alt_address']);
			$alt_address = mysqli_real_escape_string($conn, $alt_address);

			$alt_postcode = htmlspecialchars($_POST['alt_postcode']);
			$alt_postcode = mysqli_real_escape_string($conn, $alt_postcode);

			$alt_city = htmlspecialchars($_POST['alt_city']);
			$alt_city = mysqli_real_escape_string($conn, $alt_city);
		}

		if (isset($_SESSION['update_alt'])) {
			$alt_state = $_POST['alt_state'];

			// if alternative address details were changed
			if (($alt_level!=$alt_address_details['level']) || ($alt_unit!=$alt_address_details['unit']) || ($alt_address!=$alt_address_details['address']) || ($alt_postcode!=$alt_address_details['postcode']) || ($alt_city!=$alt_address_details['city']) || ($alt_state!=$alt_address_details['state'])) {

				$alt_address_id = $alt_address_details['address_id'];

				// query to update row in ADDRESS table
				$sql = "UPDATE `ADDRESS` 
						SET `level`='$alt_level', `unit`='$alt_unit', `address`='$alt_address', `postcode`='$alt_postcode', `city`='$alt_city', `state`='$alt_state' 
						WHERE `address_id`='$alt_address_id'";

				// if alternative address successfully updated
				if (execute_query($conn, $sql)) {
					// notify user
					$update_msg[] = 'Alternative address successfully edited.';
				} else {
					$errors[] = 'Error editing alternative address! Please try again later.';
				}
			}
		}

		if (isset($_SESSION['add_alt'])) {
			if (isset($_POST['alt_state'])) {
				$alt_state = $_POST['alt_state'];

				// add the alternative address in the ADDRESS table
				$sql = "INSERT INTO `ADDRESS` (`user_id`, `level`, `unit`, `address`, `postcode`, `city`, `state`, `type`)
						VALUES ('$user_id', '$alt_level', '$alt_unit', '$alt_address', '$alt_postcode', '$alt_city', '$alt_state', 'alternative')";

				// if alternative address successfully added
				if (execute_query($conn, $sql)) {
					// notify user
					$update_msg[] = 'Alternative address successfully added.';
					unset($_SESSION['add_alt']);
					$_SESSION['update_alt'] = "update_alt";
				} else {
					$errors[] = 'Error adding alternative address! Please try again later.';
				}

			} else {
				$errors[] = "Please select a state for the alternative address.";
			}
		}

		$_SESSION['edit_msgs']['update_msg'] = isset($update_msg) ? $update_msg : null;
		$_SESSION['edit_msgs']['errors'] = isset($errors) ? $errors : null;
	} elseif (isset($_POST['delete_alt'])) {
		// persist details if it was set
		if (isset($_POST['level'])) { $level = htmlspecialchars($_POST['level']);}
		if (isset($_POST['unit'])) { $unit = htmlspecialchars($_POST['unit']);}
		if (isset($_POST['address'])) { $address = htmlspecialchars($_POST['address']);}
		if (isset($_POST['postcode'])) { $postcode = htmlspecialchars($_POST['postcode']);}
		if (isset($_POST['city'])) { $city = htmlspecialchars($_POST['city']);}
		if (isset($_POST['state'])) { $state = $_POST['state'];}

		if (isset($alt_address_details)) {
			$alt_address_id = $alt_address_details['address_id'];
			$sql = "DELETE FROM `ADDRESS` 
					WHERE `address_id`='$alt_address_id'";

			// if alt address successfully deleted
			if (execute_query($conn, $sql)) {
				unset($_SESSION['edit_resident']['alt_address']);
				unset($_SESSION['update_alt']);

				// notify user
				$update_msg[] = 'Alternative address successfully deleted.';
			} else {
				$errors[] = 'Error deleting alternative! Please try again later.';
			}
		} else {
			unset($_SESSION['add_alt']);
		}
	} elseif (isset($_POST['add_alt'])) {
		// persist details if it was set
		if (isset($_POST['level'])) { $level = htmlspecialchars($_POST['level']);}
		if (isset($_POST['unit'])) { $unit = htmlspecialchars($_POST['unit']);}
		if (isset($_POST['address'])) { $address = htmlspecialchars($_POST['address']);}
		if (isset($_POST['postcode'])) { $postcode = htmlspecialchars($_POST['postcode']);}
		if (isset($_POST['city'])) { $city = htmlspecialchars($_POST['city']);}
		if (isset($_POST['state'])) { $state = $_POST['state'];}

		$_SESSION['add_alt'] = "add_alt";
		
	} elseif (isset($_POST['change_pwd'])) {
		$_SESSION['change_pwd'] = "change_pwd";	

	} elseif (isset($_POST['cancel_pwd'])) {
		unset($_SESSION['change_pwd']);		

	} elseif (isset($_POST['save_pwd'])) {
		$pwd = htmlspecialchars($_POST['pwd']);
		$pwd = mysqli_real_escape_string($conn, $pwd);

		// password hashing
		$hash = password_hash($pwd, PASSWORD_DEFAULT);

		// query to update row in USER table
		$sql = "UPDATE `USER` 
				SET `password`='$hash' 
				WHERE `user_id`='$user_id'";

		// if contact number successfully updated
		if (execute_query($conn, $sql)) {
			// notify user
			$update_msg[] = 'Password changed successfully.';
		} else {
			$errors[] = 'Error changing password! Please try again later.';
		}

		unset($_SESSION['change_pwd']);		
	} else {
		header('Location: resident_accounts.php');
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<section class="container content-wrap">
		<h3 class="center brand-text">Edit <?php echo $username ?>'s Account</h3>
		<form action="edit_resident.php" method="POST">
			<div class="center">
				<?php if (!isset($_SESSION['change_pwd'])) : ?>
					<button type="submit" name="cancel" value="cancel" class="btn z-depth-0 red" formnovalidate>Stop Editing<i class="material-icons left">chevron_left</i></button>
					<button type="submit" name="save_changes" value="save" class="btn z-depth-0 green">Save Changes<i class="material-icons right">check</i></button>
				
					<button type="submit" name="change_pwd" value="change_pwd" class="btn z-depth-0 brand-dark" formnovalidate>Change Password<i class="material-icons left">edit</i></button>
				<?php else : ?>
					<button type="submit" name="cancel_pwd" value="cancel" class="btn z-depth-0 red" formnovalidate>Cancel Changes<i class="material-icons left">chevron_left</i></button>
					<button type="submit" name="save_pwd" value="save" class="btn z-depth-0 green">Save Password<i class="material-icons right">check</i></button>
				<?php endif ?>
			</div>
			<div class="row">
				<div class="col s12 card white">
					<div class="card-content container">
						<div class="row">
							<?php if (isset($update_msg)) : ?>
				        		<ul class="browser-default update-msg">
				        			<?php foreach ($update_msg as $msg) : ?>
				        				<li><?php echo $msg; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>
							<?php if (isset($errors)) : ?>
				        		<ul class="browser-default error-msg">
				        			<?php foreach ($errors as $error) : ?>
				        				<li><?php echo $error; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>

				        	<?php if (isset($_SESSION['change_pwd'])) : ?>
				        		<!-- CHANGE PASSWORD -->
								<h5 class="center grey-text">Change Password</h5>
				        		<!-- Password input -->
								<div class="input-field col s6">
									<i class="material-icons prefix">lock</i>
									<input id="su_pwd" type="password" name="pwd" class="validate" required onchange="check_pwd_con()">
									<label for="su_pwd">Password</label>
									<span id="pwd_helper" class="helper-text" data-error="Required field."/>
								</div>

								<!-- Confirm Password input -->
								<div class="input-field col s6">
									<input id="su_pwd_con" type="password" name="pwd_con" class="validate" required onchange="check_pwd_con()" onblur="check_pwd_con()">
									<label for="su_pwd_con">Confirm Password</label>
									<span id="pwd_con_helper" class="helper-text" data-error="Required field."/>
								</div>
				        	<?php else : // show profile details ?>
								<!-- START OF ACCOUNT DETAILS -->
								<h5 class="center grey-text">Account Details</h5>

								<!-- Username input -->
								<div class="input-field col s6 tooltipped" data-position="bottom" 
								data-tooltip="
								<ul class='left-align browser-default'>
									<li>Max. Characters is 10.</li>
									<li>Only alphanumeric characters, '_' and '-'.</li>
									<li>Cannot have two or more underscores or hyphens in a row.</li>
									<li>Cannot have an underscore or hypen at the start or end.</li>
								</ul>
								">
									<i class="material-icons prefix">account_circle</i>
									<input id="su_username" type="text" name="username" 
									value="<?php echo $username ?>" class="validate" 
									required pattern="^[a-zA-Z]+((_|-)?[a-zA-Z0-9])*[a-zA-Z0-9]*$" 
									maxlength="10"
									onchange="check_username()">
									<label for="username">Username</label>
									<span id="usr_helper" class="helper-text" data-error="Required field."/>
								</div>

								<!-- Contacts input -->
								<div class="input-field col s6">
									<i class="material-icons prefix">phone</i>
									<input id="contact" type="tel" name="contact" value="<?php echo $contact ?>"  class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
									<label for="contact">Contact Number</label>
									<span id="contact_helper" class="helper-text" data-error="Required field."/>
								</div>

								<div class="col s12">
									<div class="divider"></div>
									<br>
								</div>

								<!-- START OF PERSONAL DETAILS -->
								<h5 class="center grey-text">Personal Details</h5>

								<br>

								<!-- IC Number input -->
								<div class="input-field col s6">
									<input id="ic_num" type="tel" name="ic_num" value="<?php echo $ic_num ?>" class="validate" maxlength="12" required pattern="^[0-9]{12}$" onchange="check_ic_num()">
									<label for="ic_num">MyKad Number</label>
									<span id="ic_num_helper" class="helper-text" data-error="Required field."/>
								</div>

								<!-- IC Name input -->
								<div class="input-field col s6">
									<input id="ic_name" type="text" name="ic_name" value="<?php echo $ic_name ?>" class="validate" required>
									<label for="ic_name">Full Name (According to MyKad)</label>
									<span id="ic_name_helper" class="helper-text" data-error="Required field."/>
								</div>

								<!-- CURRENT ADDRESS -->
								<ul class="collection with-header">
									<li class="collection-header grey-text"><h6>Current Address</h6></li>
									<li class="collection-item">
										<!-- Level input -->
										<div class="input-field col s3">
											<input id="level" type="number" name="level" value="<?php echo isset($current_level) ? $current_level : '' ?>" class="validate" required>
											<label for="level">Level</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- Unit input -->
										<div class="input-field col s3">
											<input id="unit" type="number" name="unit" value="<?php echo isset($current_unit) ? $current_unit : '' ?>" class="validate" required>
											<label for="unit">Unit Number</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- Address input -->
										<div class="input-field col s6">
											<input disabled id="address" type="text" name="address" value="<?php echo isset($current_address) ? $current_address : '' ?>">
											<label for="address">Address</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- Postcode input -->
										<div class="input-field col s3">
											<input disabled id="postcode" type="number" name="postcode" value="<?php echo isset($current_postcode) ? $current_postcode : '' ?>">
											<label for="postcode">Postcode</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- City input -->
										<div class="input-field col s3">
											<input disabled id="city" type="text" name="city" value="<?php echo isset($current_city) ? $current_city : '' ?>">
											<label for="city">City</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- State input -->
										<div class="input-field col s6">
											<input disabled id="state" type="text" name="state" value="<?php echo isset($current_state) ? $current_state : '' ?>">
											<label for="state">State</label>
										</div>
									</li>
								</ul>

								<?php if (isset($_SESSION['add_alt']) || isset($_SESSION['update_alt'])) : ?>
									<!-- ALTERNATIVE ADDRESS -->
									<ul class="collection with-header">
										<!-- https://stackoverflow.com/questions/25617583/how-add-confirmation-box-in-php-before-deleting/45468719 -->
										<button type="submit" name="delete_alt" value="delete_alt" class="btn z-depth-0 red secondary-content" onClick="javascript: return confirm('Are you sure you want to delete this address?');" formnovalidate>Delete<i class="material-icons left">clear</i></button>

										<li class="collection-header grey-text"><h6>Alternative Address</h6></li>
										
										<li class="collection-item">
											<!-- Level input -->
											<div class="input-field col s3">
												<input id="alt_level" type="number" name="alt_level" min="0" value="<?php echo isset($alt_level) ? $alt_level : '' ?>" class="validate" required>
												<label for="alt_level">Level</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- Unit input -->
											<div class="input-field col s3">
												<input id="alt_unit" type="number" name="alt_unit" min="0" value="<?php echo isset($alt_unit) ? $alt_unit : '' ?>" class="validate" required>
												<label for="alt_unit">Unit Number</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- Address input -->
											<div class="input-field col s6">
												<input id="alt_address" type="text" name="alt_address" value="<?php echo isset($alt_address) ? $alt_address : '' ?>" class="validate" required>
												<label for="alt_address">Address</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- Postcode input -->
											<div class="input-field col s3">
												<input id="alt_postcode" type="number" name="alt_postcode" min="0" max="99999" value="<?php echo isset($alt_postcode) ? $alt_postcode : '' ?>" class="validate" required>
												<label for="alt_postcode">Postcode</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- City input -->
											<div class="input-field col s3">
												<input id="alt_city" type="text" name="alt_city" value="<?php echo isset($alt_city) ? $alt_city : '' ?>">
												<label for="alt_city">City</label>
											</div>

											<!-- State input -->
											<div class="input-field col s6">
												<select id="alt_state" name="alt_state">
													<?php 
													// set selected option if state has been previously selected
													foreach ($states as $option => $value) {
														$attribute = '';

														// if the state matches the states option value
														if ((isset($alt_state) ? $alt_state : '') == $value) {
															$attribute = ' selected';
														}

														// disable the 'Choose your option' option
														if ($value == '') {
															$attribute .= ' disabled';
														}

														echo '<option value="'.$value.'"'.$attribute.'>'.$option.'</option>';
													} ?>
												</select>
												<label>State</label>
											</div>
										</li>
									</ul>
								<?php else : ?>
									<button type="submit" name="add_alt" value="add_alt" class="btn z-depth-0 green" formnovalidate>Add Alternative Address<i class="material-icons left">add</i></button>
								<?php endif ?>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>

	<?php include('../templates/footer.php') ?>
</html>