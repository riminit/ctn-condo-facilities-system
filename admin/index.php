<?php 
  // start session if session hasnt been started/$_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  include('../includes/dashboard_data.php');
?> 

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <div class="row">
      <h3 class="center brand-text">Dashboard</h3>
      <div class="col s6">
        <div class="card">
          <div class="card-content">
            <h4 class="grey-text text-darken-3"><?php echo count($facilities) ?></h4>
            <h6 class="teal-text text-lighten-3">Facilities</h6>
            <p>Total number of facilities </p>
          </div>
          <div class="row teal lighten-3 brand-dark-text" style="margin: auto !important;">
            <div class="col s4 center">
              <h5><?php echo $facility_status_fre[1]; ?></h5>
              <p>Reservation Required</p>
            </div>
            <div class="col s4 center">
              <h5><?php echo $facility_status_fre[0]; ?></h5>
              <p>Reservation Not Required</p>
            </div>
            <div class="col s4 center">
              <h5><?php echo $facility_status_fre[2]; ?></h5>
              <p>Not Available</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col s6">
        <div class="card">
          <div class="card-content">
            <h4 class="grey-text text-darken-3"><?php echo count($announcements) ?></h4>
            <h6 class="brown-text text-lighten-3">Active Announcements</h6>
            <p>Total number of active announcements </p>
          </div>
          <div class="row brown lighten-3 brand-dark-text" style="margin: auto !important;">
            <div class="col s4 center">
              <h5><?php echo $announcement_status_fre[1]; ?></h5>
              <p>Pinned</p>
            </div>
            <div class="col s4 center">
              <h5><?php echo $announcement_status_fre[0]; ?></h5>
              <p>Not Pinned</p>
            </div>
            <div class="col s4 center">
              <h5><?php echo $month_announcements[0]['count']; ?></h5>
              <p>This Month</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col s4">
        <div class="card">
          <div class="card-content">
            <h4 class="grey-text text-darken-3"><?php echo count($active_res_results) ?></h4>
            <h6 class="indigo-text text-lighten-3">Active Reservations</h6>
            <p>Total number of reservations </p>
          </div>
          <div class="card-tabs">
            <ul class="tabs tabs-fixed-width">
              <li class="tab"><a href="#reservations_today">Today <strong>(<?php echo count($today_res_results); ?>)</strong></a></li>
              <li class="tab"><a href="#reservations_month">Month <strong>(<?php echo count($month_res_results); ?>)</strong></a></li>
              <li class="tab"><a class="active" href="#reservations_active">Active <strong>(<?php echo count($active_res_results); ?>)</strong></a></li>
            </ul>
          </div>
          <div class="card-content grey lighten-4 tabs-content">
            <div id="reservations_today" style="width: 100%;height: 100%">
              <span><?php echo (count($today_res_results) == 0) ? 'There are no reservations today.' : '' ?></span>
              <canvas id="reservationsTodayChart" width="400" height="400"></canvas>
            </div>
            <div id="reservations_month" style="width: 100%;height: 100%">
              <span><?php echo (count($month_res_results) == 0) ? 'There are no reservations this month.' : '' ?></span>
              <canvas id="reservationsMonthChart" width="400" height="400"></canvas>
            </div>
            <div id="reservations_active" style="width: 100%;height: 100%">
              <span><?php echo (count($active_res_results) == 0) ? 'There are no active reservations.' : '' ?></span>
              <canvas id="reservationsActiveChart" width="400" height="400"></canvas>
            </div>
          </div>
        </div>
      </div>
      <div class="col s4">
        <div class="card">
          <div class="card-content">
            <h4 class="grey-text text-darken-3"><?php echo count($unsolved_rep_results) ?></h4>
            <h6 class="deep-purple-text text-lighten-3">Unsolved Reports</h6>
            <p>Total number of reports </p>
          </div>
          <div class="card-tabs">
            <ul class="tabs tabs-fixed-width">
              <li class="tab"><a href="#reports_today">Today <strong>(<?php echo count($today_rep_results); ?>)</strong></a></li>
              <li class="tab"><a href="#reports_month">Month <strong>(<?php echo count($month_rep_results); ?>)</strong></a></li>
              <li class="tab"><a class="active" href="#reports_unsolved">Unsolved <strong>(<?php echo count($unsolved_rep_results); ?>)</strong></a></li>
            </ul>
          </div>
          <div class="card-content grey lighten-4 tabs-content">
            <div id="reports_today" style="width: 100%;height: 100%">
              <span><?php echo (count($today_rep_results) == 0) ? 'There are no reports today.' : '' ?></span>
              <canvas id="reportsTodayChart" width="400" height="400"></canvas>
            </div>
            <div id="reports_month" style="width: 100%;height: 100%">
              <span><?php echo (count($month_rep_results) == 0) ? 'There are no reports this month.' : '' ?></span>
              <canvas id="reportsMonthChart" width="400" height="400"></canvas>
            </div>
            <div id="reports_unsolved" style="width: 100%;height: 100%">
              <span><?php echo (count($unsolved_rep_results) == 0) ? 'There are no unsolved reports.' : '' ?></span>
              <canvas id="reportsUnsolvedChart" width="400" height="400"></canvas>
            </div>
          </div>
        </div>
      </div>
      <div class="col s4">
        <div class="card">
          <div class="card-content">
            <h4 class="grey-text text-darken-3"><?php echo $res_status_fre[1] ?></h4>
            <h6 class=" red-text text-lighten-3">Unverified / Disabled Accounts</h6>
            <p>Total number of accounts </p>
          </div>
          <div class="card-tabs">
            <ul class="tabs tabs-fixed-width">
              <li class="tab"><a class="active" href="#resident_acc">Resident <strong>(<?php echo count($resident_results); ?>)</strong></a></li>
              <li class="tab"><a href="#all_acc">All Enabled <strong>(<?php echo count($enabled_acc); ?>)</strong></a></li>
            </ul>
          </div>
          <div class="card-content grey lighten-4 tabs-content">
            <div id="resident_acc" style="width: 100%;height: 100%">
              <span><?php echo (count($resident_results) == 0) ? 'There are no resident accounts.' : '' ?></span>
              <canvas id="residentAccChart" width="400" height="400"></canvas>
            </div>
            <div id="all_acc" style="width: 100%;height: 100%">
              <span><?php echo (count($enabled_acc) == 0) ? 'There are no accounts.' : '' ?></span>
              <canvas id="allAccChart" width="400" height="400"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    <?php include('../includes/dashboard_charts.js') ?>
  </script>

  <?php include('../templates/footer.php') ?>
</html>
