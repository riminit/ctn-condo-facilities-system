<?php 
  //Connect the database
  include('../config/db_connect.php');
  // include database functions
  include('../includes/db_functions.php');

  // start session if session hasnt been started/$_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  if (isset($_SESSION['edit_facilities'])) { unset($_SESSION['edit_facilities']); }
  
  // if user clicked delete button
  if (isset($_POST['delete_facility'])) {
    $facility_id = $_POST['delete_facility'];
    // query for getting the facility_img
    $sql = "SELECT `facility_img` 
            FROM `FACILITY` 
            WHERE `facility_id`='$facility_id'";

    $facility = select_single($conn, $sql);
    $img_path = "../img/facilities/".$facility['facility_img'];

    $sql = "DELETE FROM `FACILITY` 
            WHERE `facility_id`='$facility_id'";

    // if facility successfully deleted
    if (execute_query($conn, $sql)) {
      // delete facility image file from server
      if (file_exists($img_path)) {
        unlink($img_path);
      }

      // notify and redirect facilities page
      $update_msg[] = 'Facility successfully deleted.';

    } else {
      $errors[] = 'Error deleting Facility! Please try again later.';
    }
  }

  if (isset($_POST['required_checked'])) {
    // Getting the checked facilities
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in FACILITY table
        $sql = "UPDATE `FACILITY` 
            SET `status`='1' 
            WHERE `facility_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error setting facility(s) status to Reservation Required! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Facility(s) status successfully set to Reservation Required.';
    }
  }

  if (isset($_POST['not_required_checked'])) {
    // Getting the checked facilities
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in FACILITY table
        $sql = "UPDATE `FACILITY` 
            SET `status`='0' 
            WHERE `facility_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $return_msg = sendNotification($conn, '0', $key);
          if (isset($return_msg['errors'])) {
            foreach ($return_msg['errors'] as $msg) {
              $errors[] = $msg;
            }

            $success = false;
            break;

          } else {
            $success = true;
          }
          
        } else {
          $success = false;
          $errors[] = 'Error setting facility(s) status to reservation not required! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      // Get all the update messages
      foreach ($return_msg['update_msg'] as $msg) {
        $update_msg[] = $msg;
      }
      $update_msg[] = 'Facility(s) status successfully set to Reservation Not Required.';
    }
  }

  if (isset($_POST['na_checked'])) {
    // Getting the checked facilities
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in FACILITY table
        $sql = "UPDATE `FACILITY` 
            SET `status`='3' 
            WHERE `facility_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $return_msg = sendNotification($conn, '3', $key);
          if (isset($return_msg['errors'])) {
            foreach ($return_msg['errors'] as $msg) {
              $errors[] = $msg;
            }

            $success = false;
            break;

          } else {
            $success = true;
          }
          
        } else {
          $success = false;
          $errors[] = 'Error setting facility(s) status to reservation not required! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      // Get all the update messages
      foreach ($return_msg['update_msg'] as $msg) {
        $update_msg[] = $msg;
      }
      $update_msg[] = 'Facility(s) status successfully set to Reservation Not Required.';
    }
  }

  if (isset($_POST['delete_checked'])) {
    // Getting the checked facilities
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query for getting the facility_img
        $sql = "SELECT `facility_img` 
                FROM `FACILITY` 
                WHERE `facility_id`='$key'";

        $facility_img = select_single($conn, $sql);

        $sql = "DELETE FROM `FACILITY` 
                WHERE `facility_id`='$key'";

        // if facility successfully deleted
        if (execute_query($conn, $sql)) {
          // delete facility image file from server
          if ($facility_img['facility_img'] != null) {
            $img_path = "../img/facilities/".$facility_img['facility_img'];
            if (file_exists($img_path)) {
              unlink($img_path);
            }
          }
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error deleting facility(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Report(s) successfully deleted.';
    }
  }

  function sendNotification($conn, $status, $facility_id) {
    $return_msg = array();

    // query for getting the user ids of residents whose reservations got cancelled
    $sql = "SELECT `facility_name`
        FROM `FACILITY` 
        WHERE `facility_id` = '$facility_id'";

    $facility = execute_query($conn, $sql);
    $name = $facility['facility_name'];

    $user_id = $_SESSION['user_details']['user_id'];
    $subject = "Cancelled Reservation";
    $msg_description = "We regret to inform you that your reservation(s) for ". $name . " has been cancelled.";
    if ($status == '0') {
      $msg_description .= " The facility no longer require a reservation to use.";
    } else {
      $msg_description .= " The facility is temporarily unavailable / will no longer be available.";
    }
    
    // query to insert in NOTIFICATION table
    $sql = "INSERT INTO `NOTIFICATION` (`user_id`, `subject`, `description`)
        VALUES ('$user_id', '$subject', '$msg_description')";

    // if notification successfully added
    if (execute_query($conn, $sql)) {
      // get the last id
      $notification_id = mysqli_insert_id($conn);

      // query for getting the user ids of residents whose reservations got cancelled
      $sql = "SELECT RESERVATION.user_id
          FROM `RESERVATION` 
          INNER JOIN `USER` ON RESERVATION.user_id = USER.user_id 
          WHERE `facility_id` = '$facility_id'
          AND RESERVATION.status = '1'
          AND `start_datetime` > NOW() 
          AND USER.user_type = 'resident'";

      // get result in array
      $results = select_multiple($conn, $sql);

      $recipient_ids = array();
      foreach ($results as $result) {
        $recipient_ids[] = $result['user_id'];
      }

      // remove repeating user ids
      $recipient_ids = array_unique($recipient_ids);

      foreach ($recipient_ids as $user_id) {
        // query to insert in RECIPIENT table
        $sql = "INSERT INTO `RECIPIENT` (`notification_id`, `user_id`, `status`)
        VALUES ('$notification_id', '$user_id', '0')";

        execute_query($conn, $sql);
      }

      $return_msg['update_msg'][] = 'Notifications for cancelled reservations successfully sent.';

      // cancel all the reservations for the facility
      // query to update row in RESERVATION table
      $sql = "UPDATE `RESERVATION` 
          SET `status`='0' 
          WHERE `facility_id`='$facility_id'";

      // if reservations successfully updated
      if (execute_query($conn, $sql)) {
        $return_msg['update_msg'][] = 'All ' . $name .' reservations successfully cancelled.';
      } else {
        $return_msg['errors'][] = 'Error cancelling ' . $name .' reservations.';
      }
      
    } else {
      $return_msg['errors'][] = "Error sending notifications for cancelled reservations!";
    }

    return $return_msg;
  }

  // SORT & DISPLAY TABLE
  // get the column name
  $col_num = isset($_GET['column']) ? $_GET['column'] : 0;
  switch ($col_num) {
    case 1:
      $column = 'facility_name';
      break;
    
    case 2:
      $column = 'facility_desc';
      break;
    
    case 3:
      $column = 'status';
      break;

    default:
      $column = 'facility_id';
      break;
  }

  // Reference: https://codeshack.io/how-to-sort-table-columns-php-mysql/
  $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'asc' ? 'ASC' : 'DESC';

  // query for getting all the facilities
  $sql = "SELECT `facility_id`, `facility_name`, `facility_desc`, `status` 
          FROM `FACILITY` 
          ORDER BY " . $column . ' ' . $sort_order;

  $facilities = select_multiple($conn, $sql);

  $up_or_down = str_replace(array('ASC','DESC'), array('keyboard_arrow_up','keyboard_arrow_down'), $sort_order); 
  $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';

  mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <div class="row">
      <h3 class="center brand-text">Facilities</h3>
      <div class="input-field col s4 offset-s2">
        <select id="filterOption" name="filterOption">
          <option value="1" selected>ID</option>
          <option value="2">Name</option>
          <option value="3">Description</option>
          <option value="4">Status</option>
        </select>
        <label>Filter By</label>
      </div>
      <div class="input-field col s4">
        <i class="material-icons prefix">search</i>
        <input type="text" id="searchInput" onkeyup="searchFilter()">
        <label id="searchLabel" for="searchInput">Search ID</label>
      </div>
      <form action="facilities.php?column=<?php echo $col_num; ?>&order=<?php echo $sort_order; ?>" method="POST">
        <div class="card white col s10 offset-s1">
          <div class="card-content">
            <div class="center">
              <button disabled type="submit" name="required_checked" value="required_checked" class="btn z-depth-0 green checked-btn">Reservation Required<i class="material-icons left">check</i></button>
              <button disabled type="submit" name="not_required_checked" value="not_required_checked" class="btn z-depth-0 red checked-btn">Reservation not Required<i class="material-icons left">clear</i></button>
              <button disabled type="submit" name="na_checked" value="na_checked" class="btn z-depth-0 orange checked-btn">Not Available<i class="material-icons left">block</i></button>
              <button disabled type="submit" name="delete_checked" value="delete_checked" class="btn z-depth-0 red checked-btn" onClick="javascript: return confirm('Are you sure you want to delete the selected accounts(s)?');">Delete<i class="material-icons left">delete</i></button>
              <a type="submit" class="btn z-depth-0 brand-dark" href="add_facilities.php">Add New<i class="material-icons left">add</i></a>
            </div>
            <?php if (isset($update_msg)) : ?>
              <div class="container">
                <ul class="browser-default update-msg">
                  <?php foreach ($update_msg as $msg) : ?>
                    <li><?php echo $msg; ?></li>
                  <?php endforeach ?>
                </ul>
              </div>
            <?php endif ?>
            <?php if (isset($errors)) : ?>
              <ul class="browser-default error-msg">
                <?php foreach ($errors as $error) : ?>
                  <li><?php echo $error; ?></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
            <table id="myTable" class="responsive-table highlight centered">
              <thead>
                <tr>
                  <th><label><input type="checkbox" class="chkAll" /><span></span></label></th>

                  <th><a href="facilities.php?column=0&order=<?php echo $asc_or_desc; ?>">ID<i class="material-icons tiny"><?php echo $column == 'facility_id' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="facilities.php?column=1&order=<?php echo $asc_or_desc; ?>">Name<i class="material-icons tiny"><?php echo $column == 'facility_name' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="facilities.php?column=2&order=<?php echo $asc_or_desc; ?>">Description<i class="material-icons tiny"><?php echo $column == 'facility_desc' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="facilities.php?column=3&order=<?php echo $asc_or_desc; ?>">Status<i class="material-icons tiny"><?php echo $column == 'status' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($facilities as $facility) : ?>  
                  <tr>
                    <td><label><input type="checkbox" name="<?php echo $facility['facility_id'] ?>" class="chkboxes" /><span></span></label></td>
                    <td><?php echo $facility['facility_id']; ?></td>
                    <td><?php echo $facility['facility_name']; ?></td>
                    <td><?php echo $facility['facility_desc']; ?></td>

                    <?php if ($facility['status'] == 1) : ?>
                      <td class="green-text">Reservation Required</td>
                    <?php elseif ($facility['status'] == 0) : ?>
                      <td class="grey-text">Reservation not Required</td>
                    <?php else : ?>
                      <td class="red-text">Not Available</td>
                    <?php endif ?>

                    <td>
                      <button type="submit" name="edit_facility" value="<?php echo $facility['facility_id']; ?>" class="btn z-depth-0 brand-dark" formaction="edit_facility.php">Edit<i class="material-icons left">edit</i></button>
                    </td>
                    <td>
                      <button type="submit" name="delete_facility" value="<?php echo $facility['facility_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this facility?');">Delete<i class="material-icons left">delete</i></button>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php include('../templates/footer.php') ?>
</html>
