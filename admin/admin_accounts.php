<?php 
  //Connect the database
  include('../config/db_connect.php');
  // include database functions
  include('../includes/db_functions.php');

  // start session if session hasnt been started/$_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  if (isset($_SESSION['edit_admin'])) { unset($_SESSION['edit_admin']); }
  if (isset($_SESSION['add_alt'])) { unset($_SESSION['add_alt']); }
  if (isset($_SESSION['edit_msgs'])) { unset($_SESSION['edit_msgs']); }
  
  // if user clicked delete button
  if (isset($_POST['delete_user'])) {
    $user_id = $_POST['delete_user'];
    $sql = "DELETE FROM `USER` 
            WHERE `user_id`='$user_id'";

    // if user successfully deleted
    if (execute_query($conn, $sql)) {
      // notify and redirect user to admin_accounts page
      $update_msg[] = 'Admin account successfully deleted.';

    } else {
      $errors[] = 'Error deleting user! Please try again later.';
    }
  }

  if (isset($_POST['delete_checked'])) {
    // Getting the checked accounts
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to delete row in USER table
        $sql = "DELETE FROM `USER` 
            WHERE `user_id`='$key'";

        // if account successfully deleted
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error deleting account(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Account(s) successfully deleted.';
    }
  }

  // SORT & DISPLAY TABLE
  // get the column name
  $col_num = isset($_GET['column']) ? $_GET['column'] : 0;
  switch ($col_num) {
    case 1:
      $column = 'username';
      break;
    
    case 2:
      $column = 'ic_num';
      break;
    
    case 3:
      $column = 'ic_name';
      break;
    
    case 4:
      $column = 'contact_num';
      break;
    
    default:
      $column = 'user_id';
      break;
  }

  // Reference: https://codeshack.io/how-to-sort-table-columns-php-mysql/
  $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'asc' ? 'ASC' : 'DESC';

  // query for getting all the admin accounts
  $sql = "SELECT `user_id`, `username`, `ic_num`, `ic_name`, `contact_num` 
          FROM `USER` 
          WHERE `user_type`='admin' ORDER BY " . $column . ' ' . $sort_order;

  $admins = select_multiple($conn, $sql);

  $up_or_down = str_replace(array('ASC','DESC'), array('keyboard_arrow_up','keyboard_arrow_down'), $sort_order); 
  $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';

  mysqli_close($conn);
?> 

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <div class="row">
      <h3 class="center brand-text">Admin Accounts</h3>
      <div class="input-field col s4 offset-s2">
        <select id="filterOption" name="filterOption">
          <option value="1" selected>ID</option>
          <option value="2">Username</option>
          <option value="3">MyKad Number</option>
          <option value="4">Full Name</option>
          <option value="5">Contact Number</option>
        </select>
        <label>Filter By</label>
      </div>
      <div class="input-field col s4">
        <i class="material-icons prefix">search</i>
        <input type="text" id="searchInput" onkeyup="searchFilter()">
        <label id="searchLabel" for="searchInput">Search ID</label>
      </div>
      <form action="admin_accounts.php?column=<?php echo $col_num; ?>&order=<?php echo $sort_order; ?>" method="POST">
        <div class="card white col s10 offset-s1">
          <div class="card-content">
            <div class="center">
              <button disabled type="submit" name="delete_checked" value="delete_checked" class="btn z-depth-0 red checked-btn" onClick="javascript: return confirm('Are you sure you want to delete the selected accounts(s)?');">Delete<i class="material-icons left">delete</i></button>
              <button type="submit" name="add_admin" value="add_admin" class="btn z-depth-0 brand-dark" formaction="add_admin.php">Add New<i class="material-icons left">add</i></button>
            </div>
            <?php if (isset($update_msg)) : ?>
              <div class="container">
                <ul class="browser-default update-msg">
                  <?php foreach ($update_msg as $msg) : ?>
                    <li><?php echo $msg; ?></li>
                  <?php endforeach ?>
                </ul>
              </div>
            <?php endif ?>
            <?php if (isset($errors)) : ?>
              <ul class="browser-default error-msg">
                <?php foreach ($errors as $error) : ?>
                  <li><?php echo $error; ?></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
            <table id="myTable" class="responsive-table highlight centered">
              <thead>
                <tr>
                  <th><label><input type="checkbox" class="chkAll" /><span></span></label></th>
                  <th><a href="admin_accounts.php?column=0&order=<?php echo $asc_or_desc; ?>">ID<i class="material-icons tiny"><?php echo $column == 'user_id' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="admin_accounts.php?column=1&order=<?php echo $asc_or_desc; ?>">Username<i class="material-icons tiny"><?php echo $column == 'username' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="admin_accounts.php?column=2&order=<?php echo $asc_or_desc; ?>">MyKad Number<i class="material-icons tiny"><?php echo $column == 'ic_num' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="admin_accounts.php?column=3&order=<?php echo $asc_or_desc; ?>">Full Name<i class="material-icons tiny"><?php echo $column == 'ic_name' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="admin_accounts.php?column=4&order=<?php echo $asc_or_desc; ?>">Contact Number<i class="material-icons tiny"><?php echo $column == 'contact_num' ? $up_or_down : 'unfold_more'; ?></i></a></th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($admins as $admin) : ?>  
                  <tr>
                    <td><label><input type="checkbox" name="<?php echo $admin['user_id'] ?>" class="chkboxes" /><span></span></label></td>
                    <td><?php echo $admin['user_id']; ?></td>
                    <td><?php echo $admin['username']; ?></td>
                    <td><?php echo $admin['ic_num']; ?></td>
                    <td><?php echo $admin['ic_name']; ?></td>
                    <td>0<?php echo $admin['contact_num']; ?></td>
                    <td>
                      <button type="submit" name="edit_admin" value="<?php echo $admin['user_id']; ?>" class="btn z-depth-0 brand-dark" formaction="edit_admin.php">Edit<i class="material-icons left">edit</i></button>
                    </td>
                    <td>
                      <button type="submit" name="delete_user" value="<?php echo $admin['user_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this user?');">Delete<i class="material-icons left">delete</i></button>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php include('../templates/footer.php') ?>
</html>
