<?php 
	// start session if session hasnt been started / $_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	$status_types = array('Reservation Required' => '1',
							'Reservation not Required' => '0',
							'Not Available' => '2');

	// if user is still editing
	if (isset($_SESSION['edit_facility'])) {
		$facility = $_SESSION['edit_facility'];
		$facility_id = $facility['facility_id'];
		$name = $facility['facility_name'];
		$description = $facility['facility_desc'];
		$facility_img = $facility['facility_img'];
		$status = $facility['status'];
	}

	// if user clicked edit button
	if (isset($_POST['edit_facility'])) {
		$facility_id = $_POST['edit_facility'];

		// query for getting the facility details
		$sql = "SELECT  `facility_name`, `facility_desc`, `facility_img`, `status` 
		        FROM `FACILITY` 
		        WHERE `facility_id`='$facility_id'";

		$facility = select_single($conn, $sql);
		$name = $facility['facility_name'];
		$description = $facility['facility_desc'];
		$facility_img = $facility['facility_img'];
		$status = $facility['status'];

		$_SESSION['edit_facility'] = $facility;
		$_SESSION['edit_facility']['facility_id'] = $facility_id;

	} elseif (isset($_POST['save_changes'])) {
		$name = htmlspecialchars($_POST['name']);
		$description = htmlspecialchars($_POST['description']);
		$status = $_POST['status'];

		// if name was changed
		if ($name != $_SESSION['edit_facility']['facility_name']) {
			$name = mysqli_real_escape_string($conn, $name);
			// query to update row in FACILITY table
			$sql = "UPDATE `FACILITY` 
					SET `facility_name`='$name' 
					WHERE `facility_id`='$facility_id'";

			// if name successfully updated
			if (execute_query($conn, $sql)) {
				// update the session name
				$_SESSION['edit_facility']['facility_name'] = $name;

				// notify user
				$update_msg[] = 'Facility name successfully edited.';
			} else {
				$errors[] = 'Error editing facility name! Please try again later.';
			}
		}

		// if description was changed
		if ($description != $_SESSION['edit_facility']['facility_desc']) {
			$description = mysqli_real_escape_string($conn, $description);
			// query to update row in FACILITY table
			$sql = "UPDATE `FACILITY` 
					SET `facility_desc`='$description' 
					WHERE `facility_id`='$facility_id'";

			// if description successfully updated
			if (execute_query($conn, $sql)) {
				// update the session description
				$_SESSION['edit_facility']['facility_desc'] = $description;

				// notify user
				$update_msg[] = 'Description successfully edited.';
			} else {
				$errors[] = 'Error editing description! Please try again later.';
			}
		}

		// if status was changed
		if ($status != $_SESSION['edit_facility']['status']) {
			$status = mysqli_real_escape_string($conn, $status);
			// query to update row in FACILITY table
			$sql = "UPDATE `FACILITY` 
					SET `status`='$status' 
					WHERE `facility_id`='$facility_id'";

			// if status successfully updated
			if (execute_query($conn, $sql)) {
				// update the session status
				$_SESSION['edit_facility']['status'] = $status;

				// if does not require reservation / not available anymore
				if ($status != '1') { 
					$user_id = $_SESSION['user_details']['user_id'];
					$subject = "Cancelled Reservation";
					$msg_description = "We regret to inform you that your reservation(s) for ". $name . " has been cancelled.";
					if ($status == '0') {
						$msg_description .= " The facility no longer require a reservation to use.";
					} else {
						$msg_description .= " The facility is temporarily unavailable / will no longer be available.";
					}
					
					// query to insert in NOTIFICATION table
					$sql = "INSERT INTO `NOTIFICATION` (`user_id`, `subject`, `description`)
							VALUES ('$user_id', '$subject', '$msg_description')";

					// if notification successfully added
					if (execute_query($conn, $sql)) {
						// get the last id
						$notification_id = mysqli_insert_id($conn);

						// query for getting the user ids of residents whose reservations got cancelled
						$sql = "SELECT RESERVATION.user_id
								FROM `RESERVATION` 
								INNER JOIN `USER` ON RESERVATION.user_id = USER.user_id 
								WHERE `facility_id` = '$facility_id'
								AND RESERVATION.status = '1'
								AND `start_datetime` > NOW() 
								AND USER.user_type = 'resident'";

						// get result in array
						$results = select_multiple($conn, $sql);

						$recipient_ids = array();
						foreach ($results as $result) {
							$recipient_ids[] = $result['user_id'];
						}

						// remove repeating user ids
						$recipient_ids = array_unique($recipient_ids);

						foreach ($recipient_ids as $user_id) {
							// query to insert in RECIPIENT table
							$sql = "INSERT INTO `RECIPIENT` (`notification_id`, `user_id`, `status`)
							VALUES ('$notification_id', '$user_id', '0')";

							execute_query($conn, $sql);
						}

						$update_msg[] = 'Notifications for cancelled reservations successfully sent.';

						// cancel all the reservations for the facility
						// query to update row in RESERVATION table
						$sql = "UPDATE `RESERVATION` 
								SET `status`='0' 
								WHERE `facility_id`='$facility_id'";

						// if reservations successfully updated
						if (execute_query($conn, $sql)) {
							$update_msg[] = 'All ' . $name .' reservations successfully cancelled.';
						} else {
							$errors[] = 'Error cancelling ' . $name .' reservations.';
						}
						
					} else {
						$errors[] = "Error sending notifications for cancelled reservations!";
					}
				}

				// notify user
				$update_msg[] = 'Facility status successfully edited.';
			} else {
				$errors[] = 'Error editing facility status! Please try again later.';
			}
		}

		// if an image has been uploaded
	    if ($_FILES['facility_img']['name'] != null) {
			$file = $_FILES['facility_img'];
			$filename = $file['name'];
			$file_tmp_name = $file['tmp_name'];
			$file_error = $file['error'];
			$file_size = $file['size'];

			$file_ext = explode('.', $filename);
			$file_actual_ext = strtolower(end($file_ext));

			$allowed = array('png', 'jpeg', 'jpg');

			// if it is an allowed file type
			if (in_array($file_actual_ext, $allowed)) {
				// if there are no file errors
				if ($file_error === 0) {
					// create unique id based on microseconds for the new filename
					// (to prevent other images from being overwritten)
					$filename_new = uniqid('', true) . "." . $file_actual_ext;

					try {
						$file_destination = $config['doc_root'] . "img/facilities/" . $filename_new;
						move_uploaded_file($file_tmp_name, $file_destination);

						// query to update row in FACILITY table
						$sql = "UPDATE `FACILITY` 
								SET `facility_img`='$filename_new' 
								WHERE `facility_id`='$facility_id'";

						// if facility image successfully updated
						if (execute_query($conn, $sql)) {
							// get old facility image path
							$img_path = "../img/facilities/".$facility_img;

							// delete old facility image file from server
							if (file_exists($img_path)) {
								unlink($img_path);
							}

							// update the session facility image
							$_SESSION['edit_facility']['facility_img'] = $filename_new;
							$facility_img = $filename_new;

							// notify user
							$update_msg[] = 'Facility image successfully edited.';
						} else {
							$errors[] = 'Error editing facility facility image! Please try again later.';
						}

					} catch (Exception $e) {
						$errors[] = "There was an error uploading your file. If you are on a Darwin(Mac) OS, please set the file permission of the images folders for everyone to Read & Write";
					}

				} else {
					$errors[] = "There was an error uploading your file.";
				}
			} else {
				$errors[] = "Incorrect file type. Only .png, .jpg, and .jpeg files are accepted.";
			}
		}

	} else {
		header('Location: facilities.php');
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<div class="content-wrap">
		<section class="container">
			<h3 class="center brand-text">Edit Facility</h3>
			<form action="edit_facility.php" method="POST" enctype="multipart/form-data">
				<div class="center">
        			<button type="submit" name="cancel" value="cancel" class="btn z-depth-0 red" formnovalidate>Stop Editing<i class="material-icons left">chevron_left</i></button>
					<button type="submit" name="save_changes" value="save" class="btn z-depth-0 green">Save Changes<i class="material-icons right">check</i></button>
        		</div>
				<div class="card white">
			        <div class="card-content">
		        		<h5 class="center grey-text">Facility Details</h5>
		        		<div class="row">
		        			<?php if (isset($update_msg)) : ?>
				        		<ul class="browser-default update-msg">
				        			<?php foreach ($update_msg as $msg) : ?>
				        				<li><?php echo $msg; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>
							<?php if (isset($errors)) : ?>
				        		<ul class="browser-default error-msg">
				        			<?php foreach ($errors as $error) : ?>
				        				<li><?php echo $error; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>

							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<input id="name" type="text" name="name" value="<?php echo isset($name) ? $name : '' ?>" class="validate" required>
								<label for="name">Name</label>
								<span id="name_helper" class="helper-text" data-error="Required field."/>
							</div>

							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<textarea id="description" name="description" class="materialize-textarea validate" required><?php echo isset($description) ? $description : ''; ?></textarea>
								<label for="description">Description</label>
								<span id="description" class="helper-text" data-error="Required field."/>
							</div>

							<div class="col s8">
								<div class="file-field input-field center">
									<div class="btn-small brand-dark z-depth-0">
										<i class="material-icons left">file_upload</i>
										<span>Upload</span>
										<input type="file" name="facility_img" class="validate">
									</div>
									<div class="file-path-wrapper">
										<input id="image" disabled class="file-path" type="text">
										<?php if (is_null($facility_img)) : ?>
											<p class="left grey-text">*Upload facility image</p>
										<?php endif ?>
									</div>
								</div>

								<!-- Display facility image -->
								<h6 class="grey-text">Current Facility Image</h6>
								<div class="card white">
									<div class="card-image">
										<?php if (is_null($facility_img)) : ?>
											<p class="left red-text">*NO FACILITY IMAGE</p>
										<?php else : ?>
											<img src="../img/facilities/<?php echo $facility_img ?>" alt="Facility Image">
										<?php endif ?>
									</div>
								</div>
							</div>

							<div class="input-field col s4">
								<select id="status" name="status">
									<?php 
										// set selected option if state has been previously selected
										foreach ($status_types as $option => $value) {
											$attribute = '';

											// if the state matches the status option value
											if ((isset($status) ? $status : '') == $value) {
												$attribute = ' selected';
											}

											// disable the 'Choose your option' option
											if ($value == '') {
												$attribute .= ' disabled';
											}

											echo '<option value="'.$value.'"'.$attribute.'>'.$option.'</option>';
										} 
									?>
								</select>
								<label>Status</label>
							</div>

							
						</div> <!-- end of div.row -->
			        </div>	<!-- end of div.card-content -->
			    </div>
		    </form>
		</section>
	</div>

	<?php include('../templates/footer.php') ?>

</html>
