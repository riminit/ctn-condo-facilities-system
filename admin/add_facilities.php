<?php 
  // start session if session hasnt been started / $_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  // connect to database
  include('../config/db_connect.php');

  // include database functions
  include('../includes/db_functions.php');

  // include config file
  $config = include('../config/config.php');

  $errors = array();

  // if admin add the facilities
  if (isset($_POST['submit'])) {
    $name = mysqli_real_escape_string($conn, htmlspecialchars($_POST['name']));
    $description = mysqli_real_escape_string($conn, htmlspecialchars($_POST['description']));
    $status = $_POST['status'];

    // if an image has been uploaded
    if ($_FILES['facility_img']['name'] != null) {
      $file = $_FILES['facility_img'];
      $filename = $file['name'];
      $file_tmp_name = $file['tmp_name'];
      $file_error = $file['error'];
      $file_size = $file['size'];

      $file_ext = explode('.', $filename);
      $file_actual_ext = strtolower(end($file_ext));

      $allowed = array('png', 'jpeg', 'jpg');

      // if it is an allowed file type
      if (in_array($file_actual_ext, $allowed)) {
        // if there are no file errors
        if ($file_error === 0) {
          // create unique id based on microseconds for the new filename
          // (to prevent other images from being overwritten)
          $filename_new = uniqid('', true) . "." . $file_actual_ext;

          try {
            $file_destination = $config['doc_root'] . "img/facilities/" . $filename_new;
            move_uploaded_file($file_tmp_name, $file_destination);

            // query to insert in FACILITY table
            $sql = "INSERT INTO `FACILITY` (`facility_name`, `facility_desc`, `facility_img`, `status`)
                VALUES ('$name', '$description', '$filename_new', '$status')";

            // if facility successfully added
            if (execute_query($conn, $sql)) {
              // notify and redirect user to the facilities page
              echo "<script type='text/javascript'>alert('Facility added successfully.'); window.location.href = 'facilities.php'</script>";
            } else {
              $errors[] = "Error adding facilities ! Please try again later.";
            }

          } catch (Exception $e) {
            $errors[] = "There was an error uploading your file. If you are on a Darwin(Mac) OS, please set the file permission of the images folders for everyone to Read & Write";
          }

        } else {
          $errors[] = "There was an error uploading your file.";
        }
      } else {
        $errors[] = "Incorrect file type. Only .png, .jpg, and .jpeg files are accepted.";
      }
    }
  }

  // close connection
  mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap" >
    <section class="container">
      <h3 class="center brand-text">Add a Facility</h3>
      <div class="card white">
        <div class="card-content">
          <h5 class="center grey-text">Fill in the Facility details</h5>
          <form action="add_facilities.php" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="input-field col s12">
                <i class="material-icons prefix">mode_edit</i>
                <input id="name" type="text" name="name" class="validate" required>
                <label for="name">Name</label>
                <span id="name_helper" class="helper-text" data-error="Required field."/>
              </div>

              <div class="input-field col s12">
                <i class="material-icons prefix">mode_edit</i>
                <textarea id="description" name="description" class="materialize-textarea validate" required></textarea>
                <label for="description">Description</label>
                <span id="description" class="helper-text" data-error="Required field."/>
              </div>

              <div class="col s8">
                <div class="file-field input-field center">
                  <div class="btn-small brand-dark z-depth-0">
                    <i class="material-icons left">file_upload</i>
                    <span>Upload</span>
                    <input type="file" name="facility_img" class="validate" required>
                  </div>
                  <div class="file-path-wrapper">
                    <input id="image" disabled class="file-path" type="text">
                    <p class="left grey-text">*Upload facility image</p>
                  </div>
                </div>
              </div>

              <div class="input-field col s4">
                <select id="status" name="status">
                  <option value="1">Reservation Required</option>
                  <option value="0">Reservation not Required</option>
                  <option value="2">Not Available</option>
                </select>
                <label>Status</label>
              </div>

              <div class="col s12">
                <?php if ($errors) : ?>
                      <ul class="browser-default error-msg">
                        <?php foreach ($errors as $error) : ?>
                          <li><?php echo $error; ?></li>
                        <?php endforeach ?>
                      </ul>
                    <?php endif ?>
              </div>
            </div>  <!-- end of div.row -->

            <div class="row">
              <div class="col s4 center">
                <a href="facilities.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
              </div>

              <div class="col s4 center offset-s4">
                <button type="submit" name="submit" value="Confirm" class="btn green z-depth-0"><i class="material-icons right">send</i>Submit</button>
              </div>
            </div>
          </form>
        </div>  <!-- end of div.card-content -->
      </div>
    </section>
  </div>

  <?php include('../templates/footer.php') ?>

</html>
