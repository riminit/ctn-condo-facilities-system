<?php 
	// connect to database
	include('../config/db_connect.php');

	//include database functions
	include('../includes/db_functions.php');

	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// get the states from config
	$config = include('../config/config.php');
	$states = $config['states'];

	$errors = array();

	if (isset($_POST['submit'])) {
		$username = htmlspecialchars($_POST['username']);
		$username = mysqli_real_escape_string($conn, $username);

		$ic_num = htmlspecialchars($_POST['ic_num']);
		$ic_num = mysqli_real_escape_string($conn, $ic_num);

		$level = htmlspecialchars($_POST['level']);
		$level = mysqli_real_escape_string($conn, $level);

		$unit = htmlspecialchars($_POST['unit']);
		$unit = mysqli_real_escape_string($conn, $unit);

		$address = htmlspecialchars($_POST['address']);
		$address = mysqli_real_escape_string($conn, $address);

		$postcode = htmlspecialchars($_POST['postcode']);
		$postcode = mysqli_real_escape_string($conn, $postcode);

		$city = htmlspecialchars($_POST['city']);
		$city = mysqli_real_escape_string($conn, $city);

		// if a state has already been selected
		if (isset($_POST['state'])) {
			$state = $_POST['state'];

		} else {
			$errors[] = "Please select a state for the current address.";
		}

		// query for getting data of any user with the same name
		$sql = "SELECT * 
				FROM `USER` 
				WHERE `username`='$username'";

		// get result in array
		$same_username = select_single($conn, $sql);

		// if username has been taken
		if (sizeof($same_username)) {
			$errors[] = "Username has been taken.";

		}

		// query for getting data of any user with the same ic number
		$sql = "SELECT * 
				FROM `USER` 
				WHERE `ic_num`='$ic_num'";

		// get result in array
		$same_ic_num = select_single($conn, $sql);

		// if there is an existing user with the same ic number
		if (sizeof($same_ic_num)) {
			$errors[] = "There is already an existing user with the same MyKad number.";
		}

		// get the default address
		$config = include('../config/config.php');
		$default_address = $config['default_address'];

		// compare to check current address (return 0 if match)
		$invalid_address = strcasecmp($default_address['address'], $address);

		foreach ($default_address['city'] as $default_city) {
			// compare to check current city
			if (strcasecmp($default_city, $city)) {
				$invalid_city = 1;
			} else {
				// if there is a match
				$invalid_city = 0;
				break;
			}
		}
		
		// if address that was keyed in wasnt CTN Condominium
		if (($invalid_address || $postcode!=$default_address['postcode'] || $invalid_city || $state!=$default_address['state']) && !isset($_POST['alt_address'])) {
			// set the address details as alternative address
			if (!isset($_SESSION['add_alt'])) {
				$alt_level = $level;
				$alt_unit = $unit;
				$alt_address = $address;
				$alt_postcode = $postcode;
				$alt_city = $city;

				if (isset($state)) {
					$alt_state = $state;
				}

				$_SESSION['add_alt'] = "add_alt";
			}

			$level = $unit = '';
			$address = $default_address['address'];
			$postcode = $default_address['postcode'];
			$city = $default_address['city'][0];
			$state = $default_address['state'];

			$errors[] = "Please fill in the current(CTN Condominium) address.";
		}

		if (!$errors) {
			$contact = htmlspecialchars($_POST['contact']);
			$contact = mysqli_real_escape_string($conn, $contact);
			
			$ic_name = htmlspecialchars($_POST['ic_name']);
			$ic_name = mysqli_real_escape_string($conn, $ic_name);

			// default pass (format: [last 5 letters of name] + @ + [last 4 digits of MyKad number])
			$default_pass = substr($ic_name,-5) . '@' . substr($ic_num,-4);

			// password hashing
			$hash = password_hash($default_pass, PASSWORD_DEFAULT);

			// query to insert in USER table (status = 1 for verified users)
			$sql = "INSERT INTO `USER` (`user_type`, `username`, `password`, `ic_num`, `ic_name`, `contact_num`, `status`)
					VALUES ('resident', '$username', '$hash', '$ic_num', '$ic_name', '$contact', '1')";

			if (execute_query($conn, $sql)) {
				// get the user_id of the new user
				$sql = "SELECT `user_id`
						FROM `USER`
						WHERE `username`='$username'";

				$user_id = select_single($conn, $sql)['user_id'];

				// add the current address in the ADDRESS table
				$sql = "INSERT INTO `ADDRESS` (`user_id`, `level`, `unit`, `address`, `postcode`, `city`, `state`, `type`)
						VALUES ('$user_id', '$level', '$unit', '$address', '$postcode', '$city', '$state', 'current')";

				execute_query($conn, $sql);

				if (isset($_SESSION['add_alt'])) {
					$alt_level = htmlspecialchars($_POST['alt_level']);
					$alt_level = mysqli_real_escape_string($conn, $alt_level);

					$alt_unit = htmlspecialchars($_POST['alt_unit']);
					$alt_unit = mysqli_real_escape_string($conn, $alt_unit);

					$alt_address = htmlspecialchars($_POST['alt_address']);
					$alt_address = mysqli_real_escape_string($conn, $alt_address);

					$alt_postcode = htmlspecialchars($_POST['alt_postcode']);
					$alt_postcode = mysqli_real_escape_string($conn, $alt_postcode);

					$alt_city = htmlspecialchars($_POST['alt_city']);
					$alt_city = mysqli_real_escape_string($conn, $alt_city);

					if (isset($_POST['alt_state'])) {
						$alt_state = $_POST['alt_state'];

					} else {
						$errors[] = "Please select a state for the alternative address.";
					}

					// add the alternative address in the ADDRESS table
					$sql = "INSERT INTO `ADDRESS` (`user_id`, `level`, `unit`, `address`, `postcode`, `city`, `state`, `type`)
							VALUES ('$user_id', '$alt_level', '$alt_unit', '$alt_address', '$alt_postcode', '$alt_city', '$alt_state', 'alternative')";

					execute_query($conn, $sql);

					unset($_SESSION['add_alt']);
				}

				// notify and redirect user to resident_accounts page if succesfully added
				echo "<script type='text/javascript'>alert('Resident account succesfully added.'); window.location.href = 'resident_accounts.php'</script>";

			} else {
				$errors[] = 'Error adding resident account.';
			}
		}
	}

	if (isset($_POST['delete_alt'])) {
		// persist details if it was set
		if (isset($_POST['level'])) { $level = htmlspecialchars($_POST['level']);}
		if (isset($_POST['unit'])) { $unit = htmlspecialchars($_POST['unit']);}
		if (isset($_POST['address'])) { $address = htmlspecialchars($_POST['address']);}
		if (isset($_POST['postcode'])) { $postcode = htmlspecialchars($_POST['postcode']);}
		if (isset($_POST['city'])) { $city = htmlspecialchars($_POST['city']);}
		if (isset($_POST['state'])) { $state = $_POST['state'];}

		unset($_SESSION['add_alt']);
	}

	if (isset($_POST['add_alt'])) {
		// persist details if it was set
		if (isset($_POST['level'])) { $level = htmlspecialchars($_POST['level']);}
		if (isset($_POST['unit'])) { $unit = htmlspecialchars($_POST['unit']);}
		if (isset($_POST['address'])) { $address = htmlspecialchars($_POST['address']);}
		if (isset($_POST['postcode'])) { $postcode = htmlspecialchars($_POST['postcode']);}
		if (isset($_POST['city'])) { $city = htmlspecialchars($_POST['city']);}
		if (isset($_POST['state'])) { $state = $_POST['state'];}

		$_SESSION['add_alt'] = "add_alt";
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<section class="container content-wrap">
		<h3 id="title" class="center brand-text">New Resident Account</h3>
		<div class="col s12 card white">
			<div class="card-content container">
				<form action="add_resident.php" method="POST" enctype="multipart/form-data">
					<div class="row">
						<!-- START OF ACCOUNT DETAILS -->
						<h6 class="center grey-text">Account Details</h6>

						<!-- Username input -->
						<div class="input-field tooltipped col s6" data-position="bottom" 
						data-tooltip="
						<ul class='left-align browser-default'>
							<li>Max. Characters is 10.</li>
							<li>Only alphanumeric characters, '_' and '-'.</li>
							<li>Cannot have two or more underscores or hyphens in a row.</li>
							<li>Cannot have an underscore or hypen at the start or end.</li>
						</ul>
						">
							<i class="material-icons prefix">account_circle</i>
							<input id="su_username" type="text" name="username" 
							value="<?php echo isset($_POST['username']) ? $_POST['username'] : '' ?>" 
							class="validate" 
							required pattern="^[a-zA-Z]+((_|-)?[a-zA-Z0-9])*[a-zA-Z0-9]*$" 
							maxlength="10"
							onchange="check_username()">
							<label for="su_username">Username</label>
							<span id="usr_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- Contacts input -->
						<div class="input-field col s6">
							<i class="material-icons prefix">phone</i>
							<input id="contact" type="tel" name="contact" value="<?php echo isset($_POST['contact']) ? $_POST['contact'] : '' ?>"   class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
							<label for="contact">Contact Number</label>
							<span id="contact_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- START OF PERSONAL DETAILS -->
						<h6 class="center grey-text">Personal Details</h6>

						<!-- IC Number input -->
						<div class="input-field col s6">
							<input id="ic_num" type="tel" name="ic_num" value="<?php echo isset($_POST['ic_num']) ? $_POST['ic_num'] : '' ?>"  min="0" class="validate" maxlength="12" required pattern="^[0-9]{12}$" onchange="check_ic_num()">
							<label for="ic_num">MyKad Number</label>
							<span id="ic_num_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- IC Name input -->
						<div class="input-field col s6">
							<input id="ic_name" type="text" name="ic_name" value="<?php echo isset($_POST['ic_name']) ? $_POST['ic_name'] : '' ?>"  class="validate" required>
							<label for="ic_name">Full Name (According to MyKad)</label>
							<span id="ic_name_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- CURRENT ADDRESS -->
						<ul class="collection with-header">
							<li class="collection-header grey-text"><h6>Current Address</h6></li>
							<li class="collection-item">
								<!-- Level input -->
								<div class="input-field col s3">
									<input id="level" type="number" name="level" value="<?php echo isset($level) ? $level : '' ?>" class="validate" required>
									<label for="level">Level</label>
									<span class="helper-text" data-error="Required field."/>
								</div>

								<!-- Unit input -->
								<div class="input-field col s3">
									<input id="unit" type="number" name="unit" value="<?php echo isset($unit) ? $unit : '' ?>" class="validate" required>
									<label for="unit">Unit Number</label>
									<span class="helper-text" data-error="Required field."/>
								</div>

								<!-- Address input -->
								<div class="input-field col s6">
									<input id="address" type="text" name="address" value="<?php echo isset($address) ? $address : '' ?>">
									<label for="address">Address</label>
									<span class="helper-text" data-error="Required field."/>
								</div>

								<!-- Postcode input -->
								<div class="input-field col s3">
									<input id="postcode" type="number" name="postcode" value="<?php echo isset($postcode) ? $postcode : '' ?>">
									<label for="postcode">Postcode</label>
									<span class="helper-text" data-error="Required field."/>
								</div>

								<!-- City input -->
								<div class="input-field col s3">
									<input id="city" type="text" name="city" value="<?php echo isset($city) ? $city : '' ?>">
									<label for="city">City</label>
									<span class="helper-text" data-error="Required field."/>
								</div>
								
								<!-- State input -->
								<div class="input-field col s6">
									<select id="state" name="state">
										<?php 
										// set selected option if state has been previously selected
										foreach ($states as $option => $value) {
											$attribute = '';

											// if the state matches the states option value
											if ((isset($state) ? $state : '') == $value) {
												$attribute = ' selected';
											}

											// disable the 'Choose your option' option
											if ($value == '') {
												$attribute .= ' disabled';
											}

											echo '<option value="'.$value.'"'.$attribute.'>'.$option.'</option>';
										} ?>
									</select>
									<label>State</label>
								</div>
							</li>
						</ul>

						<?php if (isset($_SESSION['add_alt'])) : ?>
							<!-- ALTERNATIVE ADDRESS -->
							<ul class="collection with-header">
								<!-- https://stackoverflow.com/questions/25617583/how-add-confirmation-box-in-php-before-deleting/45468719 -->
								<button type="submit" name="delete_alt" value="delete_alt" class="btn z-depth-0 red secondary-content" onClick="javascript: return confirm('Are you sure you want to delete this address?');" formnovalidate>Delete<i class="material-icons left">clear</i></button>

								<li class="collection-header grey-text"><h6>Alternative Address</h6></li>
								
								<li class="collection-item">
									<!-- Level input -->
									<div class="input-field col s3">
										<input id="alt_level" type="number" name="alt_level" min="0" value="<?php echo isset($alt_level) ? $alt_level : '' ?>" class="validate" required>
										<label for="alt_level">Level</label>
										<span class="helper-text" data-error="Required field."/>
									</div>

									<!-- Unit input -->
									<div class="input-field col s3">
										<input id="alt_unit" type="number" name="alt_unit" min="0" value="<?php echo isset($alt_unit) ? $alt_unit : '' ?>" class="validate" required>
										<label for="alt_unit">Unit Number</label>
										<span class="helper-text" data-error="Required field."/>
									</div>

									<!-- Address input -->
									<div class="input-field col s6">
										<input id="alt_address" type="text" name="alt_address" value="<?php echo isset($alt_address) ? $alt_address : '' ?>" class="validate" required>
										<label for="alt_address">Address</label>
										<span class="helper-text" data-error="Required field."/>
									</div>

									<!-- Postcode input -->
									<div class="input-field col s3">
										<input id="alt_postcode" type="number" name="alt_postcode" min="0" max="99999" value="<?php echo isset($alt_postcode) ? $alt_postcode : '' ?>" class="validate" required>
										<label for="alt_postcode">Postcode</label>
										<span class="helper-text" data-error="Required field."/>
									</div>

									<!-- City input -->
									<div class="input-field col s3">
										<input id="alt_city" type="text" name="alt_city" value="<?php echo isset($alt_city) ? $alt_city : '' ?>">
										<label for="alt_city">City</label>
									</div>

									<!-- State input -->
									<div class="input-field col s6">
										<select id="alt_state" name="alt_state">
											<?php 
											// set selected option if state has been previously selected
											foreach ($states as $option => $value) {
												$attribute = '';

												// if the state matches the states option value
												if ((isset($alt_state) ? $alt_state : '') == $value) {
													$attribute = ' selected';
												}

												// disable the 'Choose your option' option
												if ($value == '') {
													$attribute .= ' disabled';
												}

												echo '<option value="'.$value.'"'.$attribute.'>'.$option.'</option>';
											} ?>
										</select>
										<label>State</label>
									</div>
								</li>
							</ul>
						<?php else : ?>
							<button type="submit" name="add_alt" value="add_alt" class="btn z-depth-0 green" formnovalidate>Add Alternative Address<i class="material-icons left">add</i></button>
						<?php endif ?>
					</div>

					<div class="row">
						<?php if ($errors) : ?>
							<ul class="browser-default error-msg">
								<?php foreach ($errors as $error) : ?>
									<li><?php echo $error; ?></li>
								<?php endforeach ?>
							</ul>
						<?php endif ?>

						<div class="col s4 center">
							<a href="resident_accounts.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
						</div>

						<div class="col s4 center offset-s4">
							<button type="submit" name="submit" value="submit" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

	<?php include('../templates/footer.php') ?>
</html>