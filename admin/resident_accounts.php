<?php 
  //Connect the database
  include('../config/db_connect.php');
  // include database functions
  include('../includes/db_functions.php');

  // start session if session hasnt been started/$_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  if (isset($_SESSION['add_alt'])) { unset($_SESSION['add_alt']); }
  if (isset($_SESSION['update_alt'])) { unset($_SESSION['update_alt']); }
  if (isset($_SESSION['edit_resident'])) { unset($_SESSION['edit_resident']); }
  
  // if user clicked delete button
  if (isset($_POST['delete_user'])) {
    $user_id = $_POST['delete_user'];
    $sql = "DELETE FROM `USER` 
            WHERE `user_id`='$user_id'";

    // if user successfully deleted
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Resident account successfully deleted.';

    } else {
      $errors[] = 'Error deleting user! Please try again later.';
    }
  }

  // if user clicked enabled button
  if (isset($_POST['status_enabled'])) {
    $user_id = $_POST['status_enabled'];

    // query to update row in USER table
    $sql = "UPDATE `USER` 
        SET `status`='0' 
        WHERE `user_id`='$user_id'";

    // if status successfully updated
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Account successfully disabled.';
    } else {
      $errors[] = 'Error disabling account! Please try again later.';
    }
  }

  // if user clicked disabled button
  if (isset($_POST['status_disabled'])) {
    $user_id = $_POST['status_disabled'];

    // query to update row in USER table
    $sql = "UPDATE `USER` 
        SET `status`='1' 
        WHERE `user_id`='$user_id'";

    // if status successfully updated
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Account successfully enabled.';
    } else {
      $errors[] = 'Error enabling account! Please try again later.';
    }
  }

  if (isset($_POST['enable_checked'])) {
    // Getting the checked accounts
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in USER table
        $sql = "UPDATE `USER` 
            SET `status`='1' 
            WHERE `user_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error enabling account(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Account(s) successfully enabled.';
    }
  }

  if (isset($_POST['disable_checked'])) {
    // Getting the checked accounts
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in USER table
        $sql = "UPDATE `USER` 
            SET `status`='0' 
            WHERE `user_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error disabling account(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Account(s) successfully disabled.';
    }
  }

  if (isset($_POST['delete_checked'])) {
    // Getting the checked accounts
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to delete row in USER table
        $sql = "DELETE FROM `USER` 
            WHERE `user_id`='$key'";

        // if account successfully deleted
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error deleting account(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Account(s) successfully deleted.';
    }
  }

  // SORT & DISPLAY TABLE
  // get the column name
  $col_num = isset($_GET['column']) ? $_GET['column'] : 0;
  switch ($col_num) {
    case 1:
      $column = 'username';
      break;
    
    case 2:
      $column = 'ic_num';
      break;
    
    case 3:
      $column = 'ic_name';
      break;
    
    case 4:
      $column = 'contact_num';
      break;

    case 5:
      $column = 'status';
      break;
    
    default:
      $column = 'user_id';
      break;
  }

  // Reference: https://codeshack.io/how-to-sort-table-columns-php-mysql/
  $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'asc' ? 'ASC' : 'DESC';

  // query for getting all the resident accounts
  $sql = "SELECT `user_id`, `username`, `ic_num`, `ic_name`, `contact_num`, `status` 
          FROM `USER` 
          WHERE `user_type`='resident' ORDER BY " . $column . ' ' . $sort_order;

  $residents = select_multiple($conn, $sql);

  $up_or_down = str_replace(array('ASC','DESC'), array('keyboard_arrow_up','keyboard_arrow_down'), $sort_order); 
  $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';

  mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <div class="row">
      <h3 class="center brand-text">Resident Accounts</h3>
      <div class="input-field col s4 offset-s2">
        <select id="filterOption" name="filterOption">
          <option value="1" selected>ID</option>
          <option value="2">Username</option>
          <option value="3">MyKad Number</option>
          <option value="4">Full Name</option>
          <option value="5">Contact Number</option>
          <option value="6">Status</option>
        </select>
        <label>Filter By</label>
      </div>
      <div class="input-field col s4">
        <i class="material-icons prefix">search</i>
        <input type="text" id="searchInput" onkeyup="searchFilter()">
        <label id="searchLabel" for="searchInput">Search ID</label>
      </div>
      <?php if (isset($_GET['column']) && isset($_GET['order'])) : ?>
        <form action="resident_accounts.php?column=<?php echo $col_num; ?>&order=<?php echo $sort_order; ?>" method="POST">
      <?php else : ?>
        <form action="resident_accounts.php" method="POST">
      <?php endif ?>
        <div class="card white col s10 offset-s1">
          <div class="card-content">
            <div class="center">
              <button disabled type="submit" name="enable_checked" value="enable_checked" class="btn z-depth-0 green checked-btn">Enable<i class="material-icons left">check</i></button>
              <button disabled type="submit" name="disable_checked" value="disable_checked" class="btn z-depth-0 red checked-btn">Disable<i class="material-icons left">clear</i></button>
              <button disabled type="submit" name="delete_checked" value="delete_checked" class="btn z-depth-0 red checked-btn" onClick="javascript: return confirm('Are you sure you want to delete the selected accounts(s)?');">Delete<i class="material-icons left">delete</i></button>
              <button type="submit" name="add_resident" value="add_resident" class="btn z-depth-0 brand-dark" formaction="add_resident.php">Add New<i class="material-icons left">add</i></button>
            </div>
            <?php if (isset($update_msg)) : ?>
              <div class="container">
                <ul class="browser-default update-msg">
                  <?php foreach ($update_msg as $msg) : ?>
                    <li><?php echo $msg; ?></li>
                  <?php endforeach ?>
                </ul>
              </div>
            <?php endif ?>
            <?php if (isset($errors)) : ?>
              <ul class="browser-default error-msg">
                <?php foreach ($errors as $error) : ?>
                  <li><?php echo $error; ?></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
            <table id="myTable" class="responsive-table highlight centered">
              <thead>
                <tr>
                  <th><label><input type="checkbox" class="chkAll" /><span></span></label></th>
                  <th><a href="resident_accounts.php?column=0&order=<?php echo $asc_or_desc; ?>">ID<i class="material-icons tiny"><?php echo $column == 'user_id' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="resident_accounts.php?column=1&order=<?php echo $asc_or_desc; ?>">Username<i class="material-icons tiny"><?php echo $column == 'username' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="resident_accounts.php?column=2&order=<?php echo $asc_or_desc; ?>">MyKad Number<i class="material-icons tiny"><?php echo $column == 'ic_num' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="resident_accounts.php?column=3&order=<?php echo $asc_or_desc; ?>">Full Name<i class="material-icons tiny"><?php echo $column == 'ic_name' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="resident_accounts.php?column=4&order=<?php echo $asc_or_desc; ?>">Contact Number<i class="material-icons tiny"><?php echo $column == 'contact_num' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="resident_accounts.php?column=5&order=<?php echo $asc_or_desc; ?>">Status<i class="material-icons tiny"><?php echo $column == 'status' ? $up_or_down : 'unfold_more'; ?></i></a></th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($residents as $resident) : ?>  
                  <tr>
                    <td><label><input type="checkbox" name="<?php echo $resident['user_id'] ?>" class="chkboxes" /><span></span></label></td>
                    <td><?php echo $resident['user_id']; ?></td>
                    <td><?php echo $resident['username']; ?></td>
                    <td><?php echo $resident['ic_num']; ?></td>
                    <td><?php echo $resident['ic_name']; ?></td>
                    <td>0<?php echo $resident['contact_num']; ?></td>
                    <td>
                      <?php if ($resident['status']) : ?>
                        <button type="submit" name="status_enabled" value="<?php echo $resident['user_id']; ?>" class="btn z-depth-0 green">Enabled</button>
                      <?php else : ?>
                        <button type="submit" name="status_disabled" value="<?php echo $resident['user_id']; ?>" class="btn z-depth-0 red">Disabled</button>
                      <?php endif ?>
                    </td>
                    <td>
                      <button type="submit" name="edit_resident" value="<?php echo $resident['user_id']; ?>" class="btn z-depth-0 brand-dark" formaction="edit_resident.php">Edit<i class="material-icons left">edit</i></button>
                    </td>
                    <td>
                      <button type="submit" name="delete_user" value="<?php echo $resident['user_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this user?');">Delete<i class="material-icons left">delete</i></button>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php include('../templates/footer.php') ?>
</html>
