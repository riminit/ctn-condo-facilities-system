<?php 
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

    // get all facilities that requires reservation
    $sql = "SELECT `facility_name`, `facility_id` 
    		FROM `FACILITY`
    		WHERE `status`='1'";
	$facilities = select_multiple($conn,$sql);

	// if user is still editing (for persisting form data)
	if (isset($_SESSION['edit_reservation'])) {
		$reservation = $_SESSION['edit_reservation'];

		$user_id = $reservation['user_id'];
		$reservation_id = $reservation['reservation_id'];
		$facility_name = $reservation['facility_name'];
		$name = $reservation['name'];
		$contact = "0".$reservation['contact_num'];
		$facility_id = $reservation['facility_id'];
		$recorded_timestamp = $reservation['recorded_timestamp'];

		$start_datetime = $reservation['start_datetime'];
		$end_datetime = $reservation['end_datetime'];

		$date = date("Y-m-d", strtotime($start_datetime));
		$start_time = date("H:i:s", strtotime($start_datetime));
		$end_time = date("H:i:s", strtotime($end_datetime));
	}

    if (isset($_POST['add_reservation'])) {
    	$errors = array();

    	$current_date = date("Y-m-d");
		$day_after_current = date('Y-m-d', strtotime($current_date.'+ 1 day'));

    	if (isset($_SESSION['reservation_details'])) {
			// if user confirm edit
			$facility_id = $_SESSION['reservation_details']['facility_id'];
			$name = $_SESSION['reservation_details']['name'];
			$contact = $_SESSION['reservation_details']['contact'];
			$date = $_SESSION['reservation_details']['date'];
			$start_time = $_SESSION['reservation_details']['start_time'];
			$end_time = $_SESSION['reservation_details']['end_time'];

			$_POST['add_reservation'] = "add";
		}
    } elseif (isset($_POST['confirm_reservation'])) {	// confirm add reservation
    	$errors = array();

    	// if user confirm reservation
		$facility_id = $_POST['facility_id'];
		$name = htmlspecialchars($_POST['name']);
		$contact = htmlspecialchars($_POST['contact']);
		$date = htmlspecialchars($_POST['date']);
		$start_time = htmlspecialchars($_POST['start_time']);
		$end_time = htmlspecialchars($_POST['end_time']);

		// Check if time range is at least 30 minutes
		$temp_start = strtotime($start_time . " +30 minutes");
		$temp_end = strtotime($end_time);

		if (!($temp_start <= $temp_end)) {
			$errors[] = "Must book for at least 30 minutes.";
		}

		// Check if time range is more than 2 hours
		$temp_start = strtotime($start_time . " +2 hours");
		$temp_end = strtotime($end_time);

		if (($temp_start < $temp_end)) {
			$errors[] = "Cannot book more than 2 hours.";
		}

		$name = mysqli_real_escape_string($conn, $name);
		$contact = mysqli_real_escape_string($conn, $contact);
		$date = mysqli_real_escape_string($conn, $date);
		$start_time = mysqli_real_escape_string($conn, $start_time);
		$end_time = mysqli_real_escape_string($conn, $end_time);

		// convert to datetime format
		// https://stackoverflow.com/questions/2215354/php-date-format-when-inserting-into-datetime-in-mysql
		$start_datetime = strtotime($start_time." ".$date);
		$start_datetime = date("Y-m-d H:i:s", $start_datetime);

		$end_datetime = strtotime($end_time." ".$date);
		$end_datetime = date("Y-m-d H:i:s", $end_datetime);

		// Check if there are reservation clashes
		// https://stackoverflow.com/a/49089737
		$sql = "SELECT COUNT(*) AS `count` 
				FROM `RESERVATION` 
				WHERE `facility_id` = '$facility_id'
				AND `status` = '1'
				AND (
                    (`start_datetime` BETWEEN '$start_datetime' AND '$end_datetime' OR `end_datetime` BETWEEN '$start_datetime' AND '$end_datetime' )
                    OR
                    ('$start_datetime' BETWEEN `start_datetime` AND `end_datetime` OR '$end_datetime' BETWEEN `start_datetime` AND `end_datetime`)
                    );";

        // get result in array
		$clashes = select_single($conn, $sql);

		if ($clashes['count']>0) {
			$errors[] = "There are time clashes with an existing reservation. Please choose another time.";
		}

		if (!$errors) {
			$user_id = $_SESSION['user_details']['user_id'];

			// query to insert in RESERVATION table
			$sql = "INSERT INTO `RESERVATION` (`user_id`, `facility_id`, `name`, `contact_num`, `start_datetime`, `end_datetime`, `status`, `visibility`)
					VALUES ('$user_id', '$facility_id', '$name', '$contact', '$start_datetime', '$end_datetime', '1', '1')";

			// if reservation successfully added
			if (execute_query($conn, $sql)) {
				// notify and redirect user to reservations page
				echo "<script type='text/javascript'>alert('Reservation successful.'); window.location.href = 'reservations.php'</script>";
			} else {
				echo "<script type='text/javascript'>alert('Error adding reservation! Please try again later.'); window.location.href = 'reservations.php'</script>";
			}

		} else {
			// persist the form data if there are errors
			$_SESSION['reservation_details'] = array('facility_id' => $facility_id,
													'name' => $name,
													'contact' => $contact,
													'date' => $date,
													'start_time' => $start_time,
													'end_time' => $end_time);
			$_POST['add_reservation']="add";
		}

	} elseif (isset($_POST['edit_reservation'])) {
		$reservation_id = $_POST['edit_reservation'];

		// query for getting the reservation details
		$sql = "SELECT `reservation_id`, `user_id`, `name`, `contact_num`, `start_datetime`, `end_datetime`, reservation.facility_id, `facility_name`, `recorded_timestamp`
				FROM `RESERVATION` 
				INNER JOIN `FACILITY` ON RESERVATION.facility_id=FACILITY.facility_id 
				WHERE `reservation_id` = '$reservation_id'";
		
		// get result in array
		$reservation = select_single($conn, $sql);
		$user_id = $reservation['user_id'];
		$facility_name = $reservation['facility_name'];
		$name = $reservation['name'];
		$contact = "0".$reservation['contact_num'];
		$reservation_id = $reservation['reservation_id'];
		$facility_id = $reservation['facility_id'];
		$recorded_timestamp = $reservation['recorded_timestamp'];

		$start_datetime = $reservation['start_datetime'];
		$end_datetime = $reservation['end_datetime'];

		$date = date("Y-m-d", strtotime($start_datetime));
		$start_time = date("H:i:s", strtotime($start_datetime));
		$end_time = date("H:i:s", strtotime($end_datetime));

		$_SESSION['edit_reservation'] = $reservation;
		$_SESSION['edit_reservation']['reservation_id'] = $reservation_id;
		$_SESSION['edit_reservation']['contact_num'] = $contact;

    } elseif (isset($_POST['save_changes'])) {
    	if (isset($update_msg)) {
    		$update_msg = null;
    	}

    	if (isset($errors)) {
    		$errors = null;
    	}

    	// if user confirm changes
		$facility_id = $_POST['facility_id'];
		$name = htmlspecialchars($_POST['name']);
		$contact = htmlspecialchars($_POST['contact']);
		$date = htmlspecialchars($_POST['date']);
		$start_time = htmlspecialchars($_POST['start_time']);
		$end_time = htmlspecialchars($_POST['end_time']);

		$name = mysqli_real_escape_string($conn, $name);
		$contact = mysqli_real_escape_string($conn, $contact);
		$date = mysqli_real_escape_string($conn, $date);
		$start_time = mysqli_real_escape_string($conn, $start_time);
		$end_time = mysqli_real_escape_string($conn, $end_time);

		// if name was changed
		if ($name != $_SESSION['edit_reservation']['name'] ) {
			$name = mysqli_real_escape_string($conn, $name);
			// query to update row in RESERVATION table
			$sql = "UPDATE `RESERVATION` 
					SET `name`='$name' 
					WHERE `reservation_id`='$reservation_id'";

			// if name successfully updated
			if (execute_query($conn, $sql)) {
				// update the session name
				$_SESSION['edit_reservation']['name'] = $name;

				// notify user
				$update_msg[] = 'Name successfully edited.';
			} else {
				$errors[] = 'Error editing name! Please try again later.';
			}
		}

		// if contact was changed
		if ($contact != $_SESSION['edit_reservation']['contact_num'] ) {
			$contact = mysqli_real_escape_string($conn, $contact);
			// query to update row in RESERVATION table
			$sql = "UPDATE `RESERVATION` 
					SET `contact_num`='$contact' 
					WHERE `reservation_id`='$reservation_id'";

			// if contact successfully updated
			if (execute_query($conn, $sql)) {
				// update the session contact
				$_SESSION['edit_reservation']['contact_num'] = $contact;

				// notify user
				$update_msg[] = 'Contact number successfully edited.';
			} else {
				$errors[] = 'Error editing contact! Please try again later.';
			}
		}

		// Check if time range is at least 30 minutes
		$temp_start = strtotime($start_time . " +30 minutes");
		$temp_end = strtotime($end_time);

		if (!($temp_start <= $temp_end)) {
			$errors[] = "Must book for at least 30 minutes.";
		}

		// Check if time range is more than 2 hours
		$temp_start = strtotime($start_time . " +2 hours");
		$temp_end = strtotime($end_time);

		if (($temp_start < $temp_end)) {
			$errors[] = "Cannot book more than 2 hours.";
		}

		// convert to datetime format
		// https://stackoverflow.com/questions/2215354/php-date-format-when-inserting-into-datetime-in-mysql
		$start_datetime = strtotime($start_time." ".$date);
		$start_datetime = date("Y-m-d H:i:s", $start_datetime);

		$end_datetime = strtotime($end_time." ".$date);
		$end_datetime = date("Y-m-d H:i:s", $end_datetime);

		// Check if there are reservation clashes
		// https://stackoverflow.com/a/49089737
		$sql = "SELECT COUNT(*) AS `count` 
				FROM `RESERVATION` 
				WHERE `facility_id` = '$facility_id' 
				AND `reservation_id` != '$reservation_id'
				AND `status` = '1'
				AND (
                    (`start_datetime` BETWEEN '$start_datetime' AND '$end_datetime' OR `end_datetime` BETWEEN '$start_datetime' AND '$end_datetime' )
                    OR
                    ('$start_datetime' BETWEEN `start_datetime` AND `end_datetime` OR '$end_datetime' BETWEEN `start_datetime` AND `end_datetime`)
                    );";

        // get result in array
		$clashes = select_single($conn, $sql);

		if ($clashes['count']>0) {
			$errors[] = "There are time clashes with an existing reservation. Please choose another time.";
		}

		// if no errors
		if (!isset($errors)) {
			// update the reservation details (Facility / Date / StartTime / EndTime)
			// query to update row in RESERVATION table
			$sql = "UPDATE `RESERVATION` 
					SET `facility_id`='$facility_id', `start_datetime`='$start_datetime', `end_datetime`='$end_datetime' 
					WHERE `reservation_id`='$reservation_id'";

			// if details successfully updated
			if (execute_query($conn, $sql)) {
				// update the session details
				$_SESSION['edit_reservation']['facility_id'] = $facility_id;
				$_SESSION['edit_reservation']['start_datetime'] = $start_datetime;
				$_SESSION['edit_reservation']['end_datetime'] = $end_datetime;

				// notify user
				$update_msg[] = 'Details successfully edited.';
			} else {
				$errors[] = 'Error editing details (Facility / Date / StartTime / EndTime)! Please try again later.';
			}
		}
		$_POST['edit_reservation'] = $reservation_id;

    } else {
		header('Location: reservations.php');
	}

	mysqli_close($conn);
		
 ?>
 <!DOCTYPE html>
 <html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<form action="make_reservation.php" method="POST">
		<?php include('reservation_form.php') ?>
	</form>

	<?php include('../templates/footer.php') ?>
 </html>