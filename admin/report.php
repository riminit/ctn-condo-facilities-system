<?php 
  //Connect the database
  include('../config/db_connect.php');
  // include database functions
  include('../includes/db_functions.php');

  // start session if session hasnt been started/$_SESSION not set
  if (!isset($_SESSION)) { session_start(); }

  if (isset($_SESSION['view_report'])) { unset($_SESSION['view_report']); }
  if (isset($_SESSION['delete_report'])) { unset($_SESSION['delete_report']); }
  
  // if user clicked delete button
  if (isset($_POST['delete_report'])) {
    $report_id = $_POST['delete_report'];
    // query for getting the report_img
    $sql = "SELECT `report_img` 
            FROM `REPORT` 
            WHERE `report_id`='$report_id'";

    $report_img = select_single($conn, $sql);

    $sql = "DELETE FROM `REPORT` 
            WHERE `report_id`='$report_id'";

    // if report successfully deleted
    if (execute_query($conn, $sql)) {
      // delete report image file from server
      if ($report_img['report_img'] != null) {
        $img_path = "../img/reports/".$report_img['report_img'];
        if (file_exists($img_path)) {
          unlink($img_path);
        }
      }

      // notify and redirect report page
      $update_msg[] = 'Report successfully deleted.';

    } else {
      $errors[] = 'Error deleting report! Please try again later.';
    }
  }

  // if user clicked resolved button
  if (isset($_POST['status_resolved'])) {
    $report_id = $_POST['status_resolved'];

    // query to update row in REPORT table
    $sql = "UPDATE `REPORT` 
        SET `status`='0' 
        WHERE `report_id`='$report_id'";

    // if status successfully updated
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Report successfully set to unresolved.';
    } else {
      $errors[] = 'Error setting report to unresolved! Please try again later.';
    }
  }

  // if user clicked unresolved button
  if (isset($_POST['status_unresolved'])) {
    $report_id = $_POST['status_unresolved'];

    // query to update row in REPORT table
    $sql = "UPDATE `REPORT` 
        SET `status`='1' 
        WHERE `report_id`='$report_id'";

    // if status successfully updated
    if (execute_query($conn, $sql)) {
      // notify user
      $update_msg[] = 'Report successfully set to resolved.';
    } else {
      $errors[] = 'Error setting report to resolved! Please try again later.';
    }
  }

  if (isset($_POST['solve_checked'])) {
    // Getting the checked reports
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in REPORT table
        $sql = "UPDATE `REPORT` 
            SET `status`='1' 
            WHERE `report_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error setting report(s) to solved! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Report(s) successfully solved.';
    }
  }

  if (isset($_POST['unsolve_checked'])) {
    // Getting the checked reports
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query to update row in REPORT table
        $sql = "UPDATE `REPORT` 
            SET `status`='0' 
            WHERE `report_id`='$key'";

        // if status successfully updated
        if (execute_query($conn, $sql)) {
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error setting report(s) to unsolved! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Report(s) successfully unsolved.';
    }
  }

  if (isset($_POST['delete_checked'])) {
    // Getting the checked reports
    // Reference: https://stackoverflow.com/questions/63083021/how-can-i-see-which-check-boxes-are-checked-in-php
    $success = false;
    foreach($_POST as $key => $value) {
      if($value === 'on'){
        // query for getting the report_img
        $sql = "SELECT `report_img` 
                FROM `REPORT` 
                WHERE `report_id`='$key'";

        $report_img = select_single($conn, $sql);

        $sql = "DELETE FROM `REPORT` 
                WHERE `report_id`='$key'";

        // if report successfully deleted
        if (execute_query($conn, $sql)) {
          // delete report image file from server
          if ($report_img['report_img'] != null) {
            $img_path = "../img/reports/".$report_img['report_img'];
            if (file_exists($img_path)) {
              unlink($img_path);
            }
          }
          $success = true;
        } else {
          $success = false;
          $errors[] = 'Error deleting report(s)! Please try again later.';
          break;
        }
      }
    }

    if ($success) {
      $update_msg[] = 'Report(s) successfully deleted.';
    }
  }

  // SORT & DISPLAY TABLE
  // get the column name
  $col_num = isset($_GET['column']) ? $_GET['column'] : 0;
  switch ($col_num) {
    case 1:
      $column = 'username';
      break;
    
    case 2:
      $column = 'contact_num';
      break;
    
    case 3:
      $column = 'report_title';
      break;
    
    case 4:
      $column = 'report_subject';
      break;

    case 5:
      $column = 'report_date';
      break;

    case 6:
      $column = 'REPORT.status';
      break;
    
    default:
      $column = 'report_id';
      break;
  }

  // Reference: https://codeshack.io/how-to-sort-table-columns-php-mysql/
  $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'asc' ? 'ASC' : 'DESC';

  // query for getting all the admin accounts
  $sql = "SELECT `report_id`, REPORT.user_id, `username`, `ic_name`, REPORT.contact_num, `report_title`, `report_subject`, `report_date`, `report_desc`, `report_img`, REPORT.status, `user_type`
          FROM `REPORT` INNER JOIN `USER` ON REPORT.user_id = USER.user_id 
          ORDER BY " . $column . ' ' . $sort_order;

  $reports = select_multiple($conn, $sql);

  $up_or_down = str_replace(array('ASC','DESC'), array('keyboard_arrow_up','keyboard_arrow_down'), $sort_order); 
  $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';

  mysqli_close($conn);
?> 

<!DOCTYPE html>
<html>
  <?php include('../templates/header.php') ?>
  <?php include('../templates/navbar.php') ?>

  <div class="content-wrap">
    <div class="row">
      <h3 class="center brand-text">Reports</h3>
      <div class="input-field col s4 offset-s2">
        <select id="filterOption" name="filterOption">
          <option value="1" selected>ID</option>
          <option value="2">Reported By</option>
          <option value="3">Contact Number</option>
          <option value="4">Subject</option>
          <option value="5">Report Date</option>
        </select>
        <label>Filter By</label>
      </div>
      <div class="input-field col s4">
        <i class="material-icons prefix">search</i>
        <input type="text" id="searchInput" onkeyup="searchFilter()">
        <label id="searchLabel" for="searchInput">Search ID</label>
      </div>
      <?php if (isset($_GET['column']) && isset($_GET['order'])) : ?>
        <form action="report.php?column=<?php echo $col_num; ?>&order=<?php echo $sort_order; ?>" method="POST">
      <?php else : ?>
        <form action="report.php" method="POST">
      <?php endif ?>
      <div class="center">
      </div>
        <div class="card white col s10 offset-s1">
          <div class="card-content">
            <div class="center">
              <button disabled type="submit" name="solve_checked" value="solve_checked" class="btn z-depth-0 green checked-btn">Set Solved<i class="material-icons left">check</i></button>
              <button disabled type="submit" name="unsolve_checked" value="unsolve_checked" class="btn z-depth-0 red checked-btn">Set Unsolved<i class="material-icons left">clear</i></button>
              <button disabled type="submit" name="delete_checked" value="delete_checked" class="btn z-depth-0 red checked-btn" onClick="javascript: return confirm('Are you sure you want to delete the selected report(s)?');">Delete<i class="material-icons left">delete</i></button>
              <button type="submit" name="make_report" value="make_report" class="btn z-depth-0 brand-dark" formaction="make_report.php">Add New<i class="material-icons left">add</i></button>
            </div>
            <?php if (isset($update_msg)) : ?>
              <div class="container">
                <ul class="browser-default update-msg">
                  <?php foreach ($update_msg as $msg) : ?>
                    <li><?php echo $msg; ?></li>
                  <?php endforeach ?>
                </ul>
              </div>
            <?php endif ?>
            <?php if (isset($errors)) : ?>
              <ul class="browser-default error-msg">
                <?php foreach ($errors as $error) : ?>
                  <li><?php echo $error; ?></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
            <table id="myTable" class="table highlight responsive-table centered">
              <thead>
                <tr>
                  <th><label><input type="checkbox" class="chkAll" /><span></span></label></th>
                  <th><a href="report.php?column=0&order=<?php echo $asc_or_desc; ?>">ID<i class="material-icons tiny"><?php echo $column == 'report_id' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="report.php?column=1&order=<?php echo $asc_or_desc; ?>">By<i class="material-icons tiny"><?php echo $column == 'username' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="report.php?column=2&order=<?php echo $asc_or_desc; ?>">Contact Number<i class="material-icons tiny"><?php echo $column == 'contact_num'  ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <!-- <th class="truncate" style="max-width: 10rem"><a href="report.php?column=3&order=<?php echo $asc_or_desc; ?>">Title<i class="material-icons tiny"><?php echo $column == 'report_title' ? $up_or_down : 'unfold_more'; ?></i></a></th> -->

                  <th><a href="report.php?column=4&order=<?php echo $asc_or_desc; ?>">Subject<i class="material-icons tiny"><?php echo $column == 'report_subject' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="report.php?column=5&order=<?php echo $asc_or_desc; ?>">Report Date<i class="material-icons tiny"><?php echo $column == 'report_date' ? $up_or_down : 'unfold_more'; ?></i></a></th>

                  <th><a href="report.php?column=6&order=<?php echo $asc_or_desc; ?>">Status<i class="material-icons tiny"><?php echo $column == 'REPORT.status' ? $up_or_down : 'unfold_more'; ?></i></a></th>
                  
                  <th>View</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($reports as $report) : ?>  
                  <tr>
                    <td><label><input type="checkbox" name="<?php echo $report['report_id'] ?>" class="chkboxes" /><span></span></label></td>
                    <td><?php echo $report['report_id']; ?></td>
                    <td><?php echo $report['username']; ?></td>
                    <td>0<?php echo $report['contact_num']; ?></td>
                    <!-- <td class="truncate" style="max-width: 10rem"><?php echo $report['report_title']; ?></td> -->
                    <td><?php echo $report['report_subject']; ?></td>
                    <td><?php echo date("d/m/Y", strtotime($report['report_date'])); ?></td>
                    <td>
                      <?php if ($report['status']) : ?>
                        <button type="submit" name="status_resolved" value="<?php echo $report['report_id']; ?>" class="btn z-depth-0 green">Solved</button>
                      <?php else : ?>
                        <button type="submit" name="status_unresolved" value="<?php echo $report['report_id']; ?>" class="btn z-depth-0 red">Unsolved</button>
                      <?php endif ?>
                    </td>
                    <td>
                      <a 
                        class="btn z-depth-0 brand-dark modal-trigger open-viewReport"
                        data-by="<?php echo $report['ic_name'] ?>"
                        data-contact="0<?php echo $report['contact_num'] ?>"
                        data-title="<?php echo $report['report_title'] ?>"
                        data-subject="<?php echo $report['report_subject'] ?>"
                        data-desc="<?php echo $report['report_desc'] ?>"
                        data-img="<?php echo isset($report['report_img']) ? '../img/reports/'.$report['report_img'] : '' ?>"
                        data-date="<?php echo date("h:i a, d/m/Y", strtotime($report['report_date'])) ?>"
                        data-userid="<?php echo $report['user_id'] ?>"
                        data-usertype="<?php echo $report['user_type'] ?>"
                        href="#viewDetails">
                        View<i class ="material-icons left">visibility</i>
                      </a>
                        
                    </td>
                    <td>
                      <button type="submit" name="delete_report" value="<?php echo $report['report_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this ?');">Delete<i class="material-icons left">delete</i></button>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div id="viewDetails" class="modal modal-fixed-footer">
    <div class="modal-content">
      <a href="#!" class="modal-close right"><i class="material-icons red-text">close</i></a>
      <h4 class="center">Report Details</h4>
      <div class="row">
        <div class="col s6">
          <label for="reportedBy">Reported By</label>
          <p id="reportedBy"></p>
          <br>
        </div>

        <div class="col s6">
          <label for="reportContact">Contact</label>
          <p id="reportContact"></p>
          <br>
        </div>

        <div class="col s6">
          <label for="reportDate">Time/Date</label>
          <p id="reportDate"></p>
          <br>
        </div>

        <div class="col s6">
          <label for="reportSubject">Subject</label>
          <p id="reportSubject"></p>
          <br>
        </div>
        
        <div class="col s12">
          <label for="reportTitle">Title</label>
          <p id="reportTitle"></p>
          <br>
        </div>
        
        <div class="col s12">
          <label for="reportDesc">Description</label>
          <p id="reportDesc"></p>
          <br>
        </div>

        <div class="col s12">
          <label id="labelImg" for="reportImg">Submitted Image</label>
          <img id="reportImg" class="responsive-img" src="" alt="No Image Submitted">
        </div>        
      </div>
    </div>
    <div class="modal-footer">
      <a id="replyHref" href="" class="modal-close waves-effect waves-green btn-flat">Reply</a>
    </div>
  </div>

<?php include('../templates/footer.php') ?>
 <script>
  // Passing and setting modal details
  // REFERENCE: https://stackoverflow.com/questions/10626885/passing-data-to-a-bootstrap-modal
  $(document).on("click", ".open-viewReport", function () {
    // reported by
    var reportedBy = $(this).data('by');
    $(".modal-content #reportedBy").text( reportedBy );

    // contact
    var reportContact = $(this).data('contact');
    $(".modal-content #reportContact").text( reportContact );

    // date
    var reportDate = $(this).data('date');
    $(".modal-content #reportDate").text( reportDate );

    // subject
    var reportSubject = $(this).data('subject');
    $(".modal-content #reportSubject").text( reportSubject );

    // title
    var reportTitle = $(this).data('title');
    $(".modal-content #reportTitle").text( reportTitle );

    // description
    var reportDesc = $(this).data('desc');
    $(".modal-content #reportDesc").text( reportDesc );

    // image
    var reportImg = $(this).data('img');
    if (reportImg != '') {  // if there is a report image
      $(".modal-content #labelImg").show();
      $(".modal-content #reportImg").show();
      $(".modal-content #reportImg").attr('src', reportImg );

    } else {  // if there is no report image
      $(".modal-content #labelImg").hide();
      $(".modal-content #reportImg").hide();
    }

    var userType = $(this).data('usertype');
    if (userType=='admin') {
      $(".modal-footer #replyHref").attr( "visibility", "hidden" );
    } else {
      // reply href
      var replyHref = "reply_report.php?userid=" + $(this).data('userid');
      $(".modal-footer #replyHref").attr( "href", replyHref );
      $(".modal-footer #replyHref").attr( "visibility", "visible" );
    }
  });
</script>
</html>
