<section class="container content-wrap">
	<h3 class="center brand-text"><?php echo isset($_POST['edit_reservation']) ? 'Edit' : 'Make' ?> Reservation</h3>
	<?php if (isset($_POST['edit_reservation'])) : ?>
		<div class="center">
			<button type="submit" name="cancel" value="cancel" class="btn z-depth-0 red" formnovalidate>Stop Editing<i class="material-icons left">chevron_left</i></button>
			<button type="submit" name="save_changes" value="save" class="btn z-depth-0 green">Save Changes<i class="material-icons right">check</i></button>
		</div>
	<?php endif ?>
  	<div class="card white">
        <div class="card-content">
    		<h5 class="center grey-text"><?php echo isset($_POST['edit_reservation']) ? 'Reservation' : 'Fill in the' ?> Details</h5>
			<div class="row">
				<?php if (isset($_POST['edit_reservation'])) : ?>
					<?php if (isset($update_msg)) : ?>
		        		<ul class="browser-default update-msg">
		        			<?php foreach ($update_msg as $msg) : ?>
		        				<li><?php echo $msg; ?></li>
		        			<?php endforeach ?>
		        		</ul>
		        	<?php endif ?>
					<?php if (isset($errors)) : ?>
		        		<ul class="browser-default error-msg">
		        			<?php foreach ($errors as $error) : ?>
		        				<li><?php echo $error; ?></li>
		        			<?php endforeach ?>
		        		</ul>
		        	<?php endif ?>
				<?php endif ?>

				<!-- Name input -->
				<div class="input-field col s6">
					<i class="material-icons prefix">account_circle</i>
					<input id="name" type="text" name="name" value="<?php echo isset($name) ? $name : '' ?>" class="validate" required>
					<label for="name">Name</label>
					<span id="name_helper" class="helper-text" data-error="Required field."/>
				</div>

				<!-- Contact input -->
				<div class="input-field col s6">
					<i class="material-icons prefix">phone</i>
					<input id="contact" type="tel" name="contact" value="<?php echo (isset($contact) ? $contact : "") ?>" class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
					<label for="contact">Contact Number</label>
					<span id="contact_helper" class="helper-text" data-error="Required field."/>
				</div>

				<!-- Facility Selection -->
				<div class="input-field col s6">
					<select id="facility_name" name="facility_id">
						<?php 
							// set selected option if facility has been previously selected
							foreach ($facilities as $facility) {
								$attribute = '';

								// if the facility matches the facilities option value
								if ((isset($facility_id) ? $facility_id : '') == $facility['facility_id']) {
									$attribute = ' selected';
								}

								echo '<option value="'.$facility['facility_id'].'"'.$attribute.'>'.$facility['facility_name'].'</option>';
							}
						?>
					</select>
					<label for="name">Facility Name</label>
				</div>

				<!-- Date input -->
				<div class="input-field col s6">
					<input id="date" type="date" name="date" value="<?php echo (isset($date) ? $date : $day_after_current) ?>" class="validate" required min="<?php echo $day_after_current ?>" onchange="check_date()">
					<label for="date">Date</label>
					<span id="date_helper" class="helper-text" data-error="Required field."/>
				</div>

				<!-- Start time input -->
				<div class="input-field col s6">
					<input id="start_time" type="time" name="start_time" value="<?php echo (isset($start_time) ? $start_time : "") ?>" class="validate" required min="09:00" max="23:00" onchange="check_time('start')">
					<label for="start_time">Start Time</label>
					<small class="grey-text">Min: 30 Minutes, Max: 2 hours</small>
					<span id="start_time_helper" class="helper-text" data-error="Required field."/>
				</div>

				<!-- End time input -->
				<div class="input-field col s6">
					<input id="end_time" type="time" name="end_time" value="<?php echo (isset($end_time) ? $end_time : "") ?>" class="validate" required min="09:00" max="23:00" onchange="check_time('end')">
					<label for="end_time">End Time</label>
					<small class="grey-text">Min: 30 Minutes, Max: 2 hours</small>
					<span id="end_time_helper" class="helper-text" data-error="Required field."/>
				</div>
			</div>

			<?php if (!isset($_POST['edit_reservation'])) : ?>
				<?php if ($errors) : ?>
	        		<ul class="browser-default error-msg">
	        			<?php foreach ($errors as $error) : ?>
	        				<li><?php echo $error; ?></li>
	        			<?php endforeach ?>
	        		</ul>
	        	<?php endif ?>
				<div class="row">
					<div class="col s4 center">
						<a href="reservations.php" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
					</div>

					<div class="col s4 offset-s4 center">
						<button type="submit" name="<?php echo isset($_POST['edit_reservation']) ? 'save_changes' : 'confirm_reservation' ?>" value="Confirm" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
					</div>
				</div>
			<?php else : ?>
				<div class="col s12">
					<p class="grey-text">User ID: <?php echo $user_id; ?></p>
				</div>
				<div class="col s12">
					<p class="grey-text">Recorded at: <?php echo date("h:i a, d/m/Y", strtotime($recorded_timestamp)); ?></p>
				</div>
			<?php endif ?>
		</div>
	</div>
</section>