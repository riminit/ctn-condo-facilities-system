<div class="center">
	<?php if ($editable != "") : ?>
			<button type="submit" name="edit_profile" value="edit" class="btn z-depth-0 brand-dark">Edit Profile<i class="material-icons left">edit</i></button>
	<?php else : ?>
		<?php if (!isset($_SESSION['change_pwd'])) : ?>
			<button type="submit" name="cancel" value="cancel" class="btn z-depth-0 red" formnovalidate>Stop Editing<i class="material-icons left">chevron_left</i></button>
			<button type="submit" name="save_changes" value="save" class="btn z-depth-0 green">Save Changes<i class="material-icons right">check</i></button>
		
			<button type="submit" name="change_pwd" value="change_pwd" class="btn z-depth-0 brand-dark" formnovalidate>Change Password<i class="material-icons left">edit</i></button>
		<?php else : ?>
			<button type="submit" name="cancel_pwd" value="cancel" class="btn z-depth-0 red" formnovalidate>Cancel Changes<i class="material-icons left">chevron_left</i></button>
			<button type="submit" name="save_pwd" value="save" class="btn z-depth-0 green">Save Password<i class="material-icons right">check</i></button>
		<?php endif ?>
	<?php endif ?>
</div>
<div class="row">
	<div class="col s12 card white">
		<div class="card-content container">
			<div class="row">
				<?php if (isset($update_msg)) : ?>
	        		<ul class="browser-default update-msg">
	        			<?php foreach ($update_msg as $msg) : ?>
	        				<li><?php echo $msg; ?></li>
	        			<?php endforeach ?>
	        		</ul>
	        	<?php endif ?>
				<?php if (isset($errors)) : ?>
	        		<ul class="browser-default error-msg">
	        			<?php foreach ($errors as $error) : ?>
	        				<li><?php echo $error; ?></li>
	        			<?php endforeach ?>
	        		</ul>
	        	<?php endif ?>

	        	<?php if (isset($_SESSION['change_pwd'])) : ?>
	        		<!-- CHANGE PASSWORD -->
					<h5 class="center grey-text">Change Password</h5>
	        		<!-- Password input -->
					<div class="input-field col s6">
						<i class="material-icons prefix">lock</i>
						<input id="su_pwd" type="password" name="pwd" class="validate" required onchange="check_pwd_con()">
						<label for="su_pwd">Password</label>
						<span id="pwd_helper" class="helper-text" data-error="Required field."/>
					</div>

					<!-- Confirm Password input -->
					<div class="input-field col s6">
						<input id="su_pwd_con" type="password" name="pwd_con" class="validate" required onchange="check_pwd_con()" onblur="check_pwd_con()">
						<label for="su_pwd_con">Confirm Password</label>
						<span id="pwd_con_helper" class="helper-text" data-error="Required field."/>
					</div>
	        	<?php else : // show profile details ?>
					<!-- START OF ACCOUNT DETAILS -->
					<h5 class="center grey-text">Account Details</h5>

					<!-- Username input -->
					<div class="input-field col s6 <?php echo !$editable ? 'tooltipped' : '' ?>" data-position="bottom" 
					data-tooltip="
					<ul class='left-align browser-default'>
						<li>Max. Characters is 10.</li>
						<li>Only alphanumeric characters, '_' and '-'.</li>
						<li>Cannot have two or more underscores or hyphens in a row.</li>
						<li>Cannot have an underscore or hypen at the start or end.</li>
					</ul>
					">
						<i class="material-icons prefix">account_circle</i>
						<input <?php echo $editable; ?> id="su_username" type="text" name="username" 
						value="<?php echo $username ?>" class="validate" 
						required pattern="^[a-zA-Z]+((_|-)?[a-zA-Z0-9])*[a-zA-Z0-9]*$" 
						maxlength="10"
						onchange="check_username()">
						<label for="username">Username</label>
						<span id="usr_helper" class="helper-text" data-error="Required field."/>
					</div>

					<!-- Contacts input -->
					<div class="input-field col s6">
						<i class="material-icons prefix">phone</i>
						<input <?php echo $editable; ?> id="contact" type="tel" name="contact" value="<?php echo $contact ?>"  class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
						<label for="contact">Contact Number</label>
						<span id="contact_helper" class="helper-text" data-error="Required field."/>
					</div>

					<div class="col s12">
						<div class="divider"></div>
						<br>
					</div>

					<!-- START OF PERSONAL DETAILS -->
					<h5 class="center grey-text">Personal Details</h5>

					<br>

					<!-- IC Number input -->
					<div class="input-field col s6">
						<input <?php echo $editable; ?> id="ic_num" type="tel" name="ic_num" value="<?php echo $ic_num ?>" class="validate" maxlength="12" required pattern="^[0-9]{12}$" onchange="check_ic_num()">
						<label for="ic_num">MyKad Number</label>
						<span id="ic_num_helper" class="helper-text" data-error="Required field."/>
					</div>

					<!-- IC Name input -->
					<div class="input-field col s6">
						<input <?php echo $editable; ?> id="ic_name" type="text" name="ic_name" value="<?php echo $ic_name ?>" class="validate" required>
						<label for="ic_name">Full Name (According to MyKad)</label>
						<span id="ic_name_helper" class="helper-text" data-error="Required field."/>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>
