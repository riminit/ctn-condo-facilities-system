<?php 
	// start session if session hasnt been started / $_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	$background_img = '../img/others/aboutUs_background.jpg';
?>

<head>
	<title>CTN Condominium Facilities</title>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Charts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>

	<!-- Charts Color Scheme Plugin -->
	<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-colorschemes"></script>

	<!--Favicon-->
	<link rel="icon" href="<?php echo (isset($_SESSION['user_details']) || strpos($_SERVER['PHP_SELF'], "signup")) ? '../' : ''?>img/icons/ctncondo_bluelogo.ico" type="image/ico">

	<style type="text/css">
		.brand {
			background: #9A8C98 !important;
		}
		.brand-text {
			color: #9A8C98 !important;
		}
		.brand-light {
			background: #F8F4F1 !important;
		}
		.brand-light-text {
			color: #F8F4F1 !important;
		}
		.brand-blue {
			background: #B2CFE6 !important;
		}
		.brand-blue-text {
			color: #B2CFE6 !important;
		}
		.brand-dark {
			background: #4A4E69 !important;
		}
		.brand-dark-text {
			color: #4A4E69 !important;
		}
		form {
			padding: 10px;
		}
		form.no-padding {
			padding: 0px;
		}
		.page-container {
		  position: relative;
		  min-height: 100vh;
		}
		.content-wrap {
		  padding-bottom: 25rem;    /* Footer height + 2.5rem */
		}
		footer {
			display: flex;
			flex-flow: row wrap;
			margin-top: 5%;
			position: absolute;
			bottom: 0;
			width: 100%;
			height: 22.5rem;            /* Footer height */
		}
		.vert-divider{
			position: absolute;
			top: 20%;
			bottom: 20%;
			border-left: 1px solid black;
		}
		.error-msg {
			padding-top: 10px;
			padding-bottom: 10px;
			background: crimson;
			color: white;
		}
		.update-msg {
			padding-top: 10px;
			padding-bottom: 10px;
			background: green;
			color: white;
		}
		.nav-dropdown {
			white-space: nowrap;
			position: absolute;
			top: -35px;
		}
		.tabs .tab a{
            color:#4A4E69 !important;
        } /* Tab text*/

        .tabs .tab a:hover {
            background-color:#DAE8F2 !important;
            color:#4A4E69 !important;
        } /*Text color on hover*/

        .tabs .tab a.active {
            background-color:#B2CFE6 !important;
            color:#4A4E69 !important;
        } /*Background and text color when a tab is active*/

        .tabs .indicator {
            background-color:#4A4E69 !important;
        } /*Color of underline*/
        .chart-wrapper {
			height: 400px !important;
		}

		.header {
			height: 85vh;
			background-image: linear-gradient(to right bottom, rgba(15, 16, 16, 0.8),
			rgba(30, 108, 217, 0.8)),
			url(<?php echo $background_img; ?>);
			background-size: cover;
			background-position: top;
			position: relative;
			clip-path: polygon(0 0, 100% 0, 100% 75vh, 0 100%);
		}

		.heading_primary {
			text-transform: uppercase;
		}

		.heading_primary_main {
			letter-spacing: 5px;
		}

		.heading_primary_sub {
			letter-spacing: 7.4px;
		}

		.justify {
			text-align: justify;
			text-justify: inter-word;
		}
	</style>
</head>

<body class="brand-light">

	<div class="page-container">

