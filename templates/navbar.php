<?php 
	// set timezone to Kuala Lumpur
	ini_set("date.timezone", "Asia/Kuala_Lumpur");

	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// Check if there is no user logged in
	if (!isset($_SESSION['user_details'])) {
		if (strpos($_SERVER['PHP_SELF'], "admin") || strpos($_SERVER['PHP_SELF'], "resident")) {
			header("Location: ../login.php");
		} else {
			header("Location: login.php");
		}
	}

	// Check if user is logged in as a resident or admin
	$user_type = $_SESSION['user_details']['user_type'];
	$current_dir = $_SERVER['PHP_SELF'];

	// Separate the dir by '/'
	$explode_dir = explode("/",$current_dir);
	$account_dir = $explode_dir[2];

	// Check and make sure user is in the correct directory
	if (($user_type == 'resident') && ($account_dir == 'admin')) {
		$redirect_dir = str_ireplace("admin", $user_type, $_SERVER['PHP_SELF']);
		header("Location: " . $redirect_dir);

	} else if (($user_type == 'admin') && ($account_dir == 'resident')) {
		$redirect_dir = str_ireplace("resident", $user_type, $_SERVER['PHP_SELF']);
		header("Location: " . $redirect_dir);
	}

	if ($user_type == 'resident') {
		// connect to database
		include('../config/db_connect.php');

		// include database functions
		include_once('../includes/db_functions.php');

		$user_id = $_SESSION['user_details']['user_id'];

		// query for number of new notifications
		$sql = "SELECT COUNT(*) AS `count` 
				FROM `RECIPIENT` 
				INNER JOIN `NOTIFICATION` ON RECIPIENT.notification_id = NOTIFICATION.notification_id 
				WHERE RECIPIENT.user_id='$user_id' 
				AND `status` = '0'";

		// get result in array
		$new_notifications = select_multiple($conn, $sql);

		mysqli_close($conn);
	}
?>

<!-- Dropdown Structure -->
<ul id="profile-dropdown" class="dropdown-content brand-dark nav-dropdown">
  <li><a href="manage_profile.php" class="white-text">Manage Profile</a></li>
  <?php if ($_SESSION['user_details']['user_type'] == 'resident') : ?>
  	<li><a href="my_reservations.php" class="white-text">My Reservations</a></li>
  	<li><a href="my_reports.php" class="white-text">My Reports</a></li>
  <?php endif ?>
  <li class="divider"></li>
  <li><a href="../logout.php" class="white-text">Logout</a></li>
</ul>

<!-- Side Navigation Bar -->
<ul class="sidenav" id="side-menu">
	<li class="center brand-blue" style="padding-top: 1rem; padding-bottom: 1rem">
		<img src="../img/icons/ctncondo_bluelogo.ico" width="100rem" height="100rem" class="responsive-img">
		<h5 class="brand-dark-text">Hello <?php echo htmlspecialchars($_SESSION['user_details']['username']); ?>!</h5>
	</li>
	<?php if ($user_type == 'resident') : ?>
		<li><a href="index.php" class="brand-dark-text">About Us</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="view_facilities.php" class="brand-dark-text">Facilities</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="announcements.php" class="brand-dark-text">Announcements</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="make_report.php" class="brand-dark-text">Make a Report</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="notifications.php" class="brand-dark-text">Notifications
			<?php if ($new_notifications[0]['count']>0) : ?>
				<span class="new badge brand brand-light-text"><?php echo $new_notifications[0]['count']; ?></span>
			<?php endif ?>
		</a></li>
	
	<?php elseif ($user_type == 'admin') : ?>
		<li><a href="index.php" class="brand-dark-text">Dashboard</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="reservations.php" class="brand-dark-text">Reservations</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="facilities.php" class="brand-dark-text">Facilities</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="announcements.php" class="brand-dark-text">Announcements</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="report.php" class="brand-dark-text">Reports</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="resident_accounts.php" class="brand-dark-text">Residents</a></li>
		<li><div class="vert-divider"></div></li>
		<li><a href="admin_accounts.php" class="brand-dark-text">Admins</a></li>
		
	<?php endif ?>

	<li><a href="manage_profile.php" class="brand-dark-text">Manage Profile</a></li>
	<?php if ($_SESSION['user_details']['user_type'] == 'resident') : ?>
		<li><a href="my_reservations.php" class="brand-dark-text">My Reservations</a></li>
		<li><a href="my_reports.php" class="brand-dark-text">My Reports</a></li>
	<?php endif ?>
	<li class="divider"></li>
	<li><a href="../logout.php" class="red-text"><i class="material-icons red-text">exit_to_app</i>Logout</a></li>
</ul>

<div class="navbar-fixed">
	<nav class="brand-blue">
		<div class="nav-wrapper">
			<a href="index.php" class="brand-logo brand-dark-text" style="margin-left: 1%"><img src="../img/icons/ctncondo_bluelogo.ico" width="25rem" height="25rem" style="margin-right: 1rem" class="responsive-img">CTN Condominium</a>
			<a href="#" data-target="side-menu" class="sidenav-trigger brand-dark-text"><i class="material-icons">menu</i></a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<?php if ($user_type == 'resident') : ?>
					<li><a href="index.php" class="brand-dark-text">About Us</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="view_facilities.php" class="brand-dark-text">Facilities</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="announcements.php" class="brand-dark-text">Announcements</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="make_report.php" class="brand-dark-text">Make a Report</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="notifications.php" class="brand-dark-text">Notifications
						<?php if ($new_notifications[0]['count']>0) : ?>
							<span class="new badge brand brand-light-text"><?php echo $new_notifications[0]['count']; ?></span>
						<?php endif ?>
					</a></li>
				
				<?php elseif ($user_type == 'admin') : ?>
					<li><a href="index.php" class="brand-dark-text">Dashboard</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="reservations.php" class="brand-dark-text">Reservations</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="facilities.php" class="brand-dark-text">Facilities</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="announcements.php" class="brand-dark-text">Announcements</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="report.php" class="brand-dark-text">Reports</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="resident_accounts.php" class="brand-dark-text">Residents</a></li>
					<li><div class="vert-divider"></div></li>
					<li><a href="admin_accounts.php" class="brand-dark-text">Admins</a></li>
					
				<?php endif ?>
				<li><a class="dropdown-trigger waves-effect btn brand-dark z-depth-0" href="#!" data-target="profile-dropdown" style="min-width: 10rem"><i class="material-icons left">account_circle</i><?php echo htmlspecialchars($_SESSION['user_details']['username']); ?></a></li>
			</ul>
		</div>
	</nav>
</div>