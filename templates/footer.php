		<footer class="section brand brand-light-text">
			<div class="container left">
				<div class="row valign-wrapper">
					<div class="col s1">
						<img src="<?php echo (isset($_SESSION['user_details']) || strpos($_SERVER['PHP_SELF'], "signup")) ? '../' : ''?>img/icons/ctncondo_brandlogo.ico" class="responsive-img">
					</div>
					<div class="col s11">
						<h3>CTN Condominium</h3>
						<h6>Design by: Creative Team Name Group</h6>
					</div>
				</div>
				<div class="row">
					<div class="col s6">
						<h5>Location</h5>
						<p>
							32 Jalan Anson, George Town,
							<br>
							10400 George Town,
							<br>
							Penang,
							Malaysia
						</p>
					</div>
					<div class="col s6">
						<h5>Contact Us</h5>
						<p>
							<b>Phone:</b> +6012 345 6789
							<br>
							<b>Email:</b> sales-pr@CTNCondo.com
						</p>
					</div>
				</div>
			</div>
		</footer>
	</div>	<!-- end of container -->

	<script type="text/javascript">
		$(document).ready(function() {
			// scroll (Resident About Us Page)
			$('.About').click(function(event) {
				$('body, html').animate({
					scrollTop: $("#about_us").offset().top - 100
				}, 600);
			});

		  	// Side navigation bar
			$('.sidenav').sidenav();

			// Dropdown trigger
			$(".dropdown-trigger").dropdown();

			// Modals
			$('.modal').modal();

			// Collapsible
			$('.collapsible').collapsible();

			$('.tabs').tabs();
		});

		document.addEventListener('DOMContentLoaded', function() {
			// Tooltips
			var elems = document.querySelectorAll('.tooltipped');
		    var instances = M.Tooltip.init(elems, {enterDelay:2000, margin:-30});

		    // Select Input / Dropdown List
		    var elems = document.querySelectorAll('select');
		    var instances = M.FormSelect.init(elems);
		});

		// Delete component
		$('.delete').on('click', function(){
	    	$(this).parent().remove();
	 	});

		// Toggle buttons based on wether checkbox checked
    	// Reference: https://stackoverflow.com/questions/14534568/jquery-show-button-when-checkbox-is-checked
		$('.chkboxes').change(function(){
			$('.checked-btn').prop('disabled', $('.chkboxes:checked').length == 0);
			if ($('.chkboxes:checked').length >0) {
				$('.chkAll').prop('checked', false);
			}
	    });

	    $('.chkAll').change(function(){
			$('.checked-btn').prop('disabled', $('.chkboxes:checked').length == 0);
	    });

	    // check all visible checkboxes if check all checkbox was selected
	    $(function(){
	        $('.chkAll').on('click', function() {
	            $('.chkboxes:visible').prop('checked', this.checked)
	        });
	    });

	    // Set search label when filter option changes
	    $('#filterOption').on('change', function (e) {
			var optionSelected = $("option:selected", this);
			$('#searchLabel').text('Search ' + optionSelected.text());
		});

		// set fixed height of tab content
		$(".tabs-content").css('height','400px');

		// Update notification status(Post request notification id)
	 	function update_status(id){
			var ajax_id = id;
			var ajaxurl  = $(this).data('url');

			$.ajax({
				type: "POST",
				url: ajaxurl,
				data: { id: ajax_id }
			})
		}

		// Search Filter (Admin Pages Tables)
	    // Reference: https://www.w3schools.com/howto/howto_js_filter_table.asp
	    function searchFilter() {
			var input = document.getElementById("searchInput");
			var filter = input.value.toUpperCase();
			var table = document.getElementById("myTable");
			var tr = table.getElementsByTagName("tr");

			var filterOption = document.getElementById("filterOption");
			var columnNum = filterOption.value;

			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[columnNum];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}       
			}
		}

		// VALIDATION -------------------------------------------------------------
		// Check if username was entered correctly
		function check_username() {
			var input = document.getElementById('su_username');
			var validityState_object = input.validity;

			var spanTxt = document.getElementById('usr_helper');

			if (validityState_object) {
				var val = input.value;
				var errorMsg = '';
				if (val == '') {
					errorMsg = "Required field.";

				} else if (val.startsWith('-') || val.startsWith('_') || val.endsWith('-') || val.endsWith('_')) {
					errorMsg = "Cannot have an underscore or hyphen at the start or end."

				} else if (/[a-zA-Z]+((_|-){2,})[a-zA-Z0-9]*/.test(val)) {
					errorMsg = "Cannot have two or more underscores or hyphens in a row."

				} else {
					errorMsg = "Only alphanumeric characters, underscores and hyphens."
				}

				spanTxt.setAttribute("data-error", errorMsg)
			} else {
				spanTxt.setAttribute("data-error", null)
			}
		}

		// Check if confirm password matches password
		function check_pwd_con() {
			var pwd = document.getElementById('su_pwd').value;

			var input = document.getElementById('su_pwd_con');
			var pwd_con = input.value;

			var spanTxt = document.getElementById('pwd_con_helper');
			var errorMsg = '';

			if (pwd_con == '') {
				errorMsg = "Required field.";

			} else if (pwd != pwd_con) {
				input.classList.remove("valid");
	  			input.classList.add("invalid");
	  			input.setCustomValidity("Password does not match.");

				errorMsg = "Password does not match.";

			} else {
				input.classList.remove("invalid");
	  			input.classList.add("valid");
	  			input.setCustomValidity('');
				errorMsg = null;
			}

			spanTxt.setAttribute("data-error", errorMsg);
		}
		
		// Check if contact number was entered correctly
		function check_contact() {
			var input = document.getElementById('contact');
			var validityState_object = input.validity;

			var spanTxt = document.getElementById('contact_helper');

			if (validityState_object) {
				var val = input.value;
				var errorMsg = '';
				if (val == '') {
					errorMsg = "Required field.";

				} else {
					errorMsg = "Malaysian Mobile Number format only. (E.g: 0123456789)"
				}

				spanTxt.setAttribute("data-error", errorMsg)
			} else {
				spanTxt.setAttribute("data-error", null)
			}
		}

		// Check if ic number was entered correctly
		function check_ic_num() {
			var input = document.getElementById('ic_num');
			var validityState_object = input.validity;

			var spanTxt = document.getElementById('ic_num_helper');

			if (validityState_object) {
				var val = input.value;
				var errorMsg = '';
				if (val == '') {
					errorMsg = "Required field.";

				} else {
					errorMsg = "MyKad Number format only. (E.g: 010101070001)"
				}

				spanTxt.setAttribute("data-error", errorMsg)
			} else {
				spanTxt.setAttribute("data-error", null)
			}
		}

		// Check if date was entered correctly
		function check_date(){
			var input = document.getElementById('date');
			var validityState_object = input.validity;

			var spanTxt = document.getElementById('date_helper');

			if (validityState_object) {
				var val = document.getElementById("date").value;
				var errorMsg = '';
				if (val == '') {
					errorMsg = "Required field.";

				} else {
					errorMsg = "Cannot be a past date."
				}

				spanTxt.setAttribute("data-error", errorMsg)
			} else {
				spanTxt.setAttribute("data-error", null)
			}
		}

		// Check if time was entered correctly
		function check_time(name){
			var input = document.getElementById(name.concat('_time'));
			var validityState_object = input.validity;

			var spanTxt = document.getElementById(name.concat('_time_helper'));

			if (validityState_object) {
				var val = document.getElementById(name.concat('_time')).value;
				var errorMsg = '';
				if (val == '') {
					errorMsg = "Required field.";

				} else {
					errorMsg = "Must be between 9 am - 11 pm."
				}

				spanTxt.setAttribute("data-error", errorMsg)
			} else {
				spanTxt.setAttribute("data-error", null)
			}
		}
	</script>
</body>


