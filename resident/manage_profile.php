<?php 
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	// get the states from config
	$config = include('../config/config.php');
	$states = $config['states'];

	$user_id = $_SESSION['user_details']['user_id'];

	// query for getting the address details
	$sql = "SELECT *
			FROM `ADDRESS`
			WHERE `user_id`='$user_id'";

	// get address(es)
	$addresses = select_multiple($conn, $sql);

	foreach ($addresses as $address) {
		if ($address['type'] == 'current') {
			$current_address = $address;
		} else {
			$alt_address = $address;
		}
	}

	$editable = "disabled";

	if (isset($_POST['edit_profile'])) {
		$editable = "";

	} elseif (isset($_POST['cancel'])) {
		if (isset($_SESSION['add_alt'])) { unset($_SESSION['add_alt']); }
		if (isset($_SESSION['edit_msgs'])) { unset($_SESSION['edit_msgs']); }
		header("Location: manage_profile.php");

	} elseif (isset($_POST['save_changes'])) {
		$contact = htmlspecialchars($_POST['contact']);
		$current_level = htmlspecialchars($_POST['level']);
		$current_unit = htmlspecialchars($_POST['unit']);

		// if contact number was changed
		if ($contact != "0".$_SESSION['user_details']['contact_num'] ) {
			$contact = mysqli_real_escape_string($conn, $contact);

			// query to update row in USER table
			$sql = "UPDATE `USER` 
					SET `contact_num`='$contact' 
					WHERE `user_id`='$user_id'";

			// if contact number successfully updated
			if (execute_query($conn, $sql)) {
				// update the session contact number
				$_SESSION['user_details']['contact_num'] = $contact;

				// notify user
				$update_msg[] = 'Contact number successfully edited.';
			} else {
				$errors[] = 'Error editing contact number! Please try again later.';
			}
		}

		// if current address details were changed
		if (($current_level != $current_address['level']) || ($current_unit != $current_address['unit'])) {
			$current_address_id = $current_address['address_id'];

			$current_level = mysqli_real_escape_string($conn, $current_level);
			$current_unit = mysqli_real_escape_string($conn, $current_unit);

			// query to update row in ADDRESS table
			$sql = "UPDATE `ADDRESS` 
					SET `level`='$current_level', `unit`='$current_unit' 
					WHERE `address_id`='$current_address_id'";

			// if contact number successfully updated
			if (execute_query($conn, $sql)) {
				// notify user
				$update_msg[] = 'Current address successfully edited.';
			} else {
				$errors[] = 'Error editing current address! Please try again later.';
			}
		}

		if (isset($alt_address)) {
			$alt_address_id = $alt_address['address_id'];

			$alt_level = htmlspecialchars($_POST['alt_level']);
			$alt_unit = htmlspecialchars($_POST['alt_unit']);
			$alt_add = htmlspecialchars($_POST['alt_address']);
			$alt_postcode = htmlspecialchars($_POST['alt_postcode']);
			$alt_city = htmlspecialchars($_POST['alt_city']);
			$alt_state = $_POST['alt_state'];

			// if alternative address details were changed
			if (($alt_level!=$alt_address['level']) || ($alt_unit!=$alt_address['unit']) || ($alt_add!=$alt_address['address']) || ($alt_postcode!=$alt_address['postcode']) || ($alt_city!=$alt_address['city']) || ($alt_state!=$alt_address['state'])) {

				$alt_level = mysqli_real_escape_string($conn, $alt_level);
				$alt_unit = mysqli_real_escape_string($conn, $alt_unit);
				$alt_add = mysqli_real_escape_string($conn, $alt_add);
				$alt_postcode = mysqli_real_escape_string($conn, $alt_postcode);
				$alt_city = mysqli_real_escape_string($conn, $alt_city);

				// query to update row in ADDRESS table
				$sql = "UPDATE `ADDRESS` 
						SET `level`='$alt_level', `unit`='$alt_unit', `address`='$alt_add', `postcode`='$alt_postcode', `city`='$alt_city', `state`='$alt_state' 
						WHERE `address_id`='$alt_address_id'";

				// if alternative address successfully updated
				if (execute_query($conn, $sql)) {
					// notify user
					$update_msg[] = 'Alternative address successfully edited.';
				} else {
					$errors[] = 'Error editing alternative address! Please try again later.';
				}
			}
		}

		if (isset($_SESSION['add_alt'])) {
			$alt_level = htmlspecialchars($_POST['alt_level']);
			$alt_unit = htmlspecialchars($_POST['alt_unit']);
			$alt_add = htmlspecialchars($_POST['alt_address']);
			$alt_postcode = htmlspecialchars($_POST['alt_postcode']);
			$alt_city = htmlspecialchars($_POST['alt_city']);

			if (isset($_POST['alt_state'])) {
				$alt_state = $_POST['alt_state'];

				$alt_level = mysqli_real_escape_string($conn, $alt_level);
				$alt_unit = mysqli_real_escape_string($conn, $alt_unit);
				$alt_add = mysqli_real_escape_string($conn, $alt_add);
				$alt_postcode = mysqli_real_escape_string($conn, $alt_postcode);
				$alt_city = mysqli_real_escape_string($conn, $alt_city);

				// add the alternative address in the ADDRESS table
				$sql = "INSERT INTO `ADDRESS` (`user_id`, `level`, `unit`, `address`, `postcode`, `city`, `state`, `type`)
						VALUES ('$user_id', '$alt_level', '$alt_unit', '$alt_add', '$alt_postcode', '$alt_city', '$alt_state', 'alternative')";

				// if alternative address successfully added
				if (execute_query($conn, $sql)) {
					// notify user
					$update_msg[] = 'Alternative address successfully added.';
					unset($_SESSION['add_alt']);
				} else {
					$errors[] = 'Error adding alternative address! Please try again later.';
				}

			} else {
				$errors[] = "Please select a state for the alternative address.";
			}
		}

		$_SESSION['edit_msgs']['update_msg'] = isset($update_msg) ? $update_msg : null;
		$_SESSION['edit_msgs']['errors'] = isset($errors) ? $errors : null;

		// UPDATE THE FORM -------------------------------------------------
		// query for getting the address(es)
		$sql = "SELECT *
				FROM `ADDRESS`
				WHERE `user_id`='$user_id'";

		// get resident id
		$addresses = select_multiple($conn, $sql);

		foreach ($addresses as $address) {
			if ($address['type'] == 'current') {
				$current_address = $address;
			} else {
				$alt_address = $address;
			}
		}

		$editable = "";

	}

	if (isset($_POST['delete_alt'])) {
		if (isset($alt_address)) {
			$alt_address_id = $alt_address['address_id'];
			$sql = "DELETE FROM `ADDRESS` 
					WHERE `address_id`='$alt_address_id'";

			// if alt address successfully deleted
			if (execute_query($conn, $sql)) {
				unset($alt_address);

				// notify user
				$update_msg[] = 'Alternative address successfully deleted.';
			} else {
				$errors[] = 'Error deleting alternative address! Please try again later.';
			}
		} else {
			unset($_SESSION['add_alt']);
		}

		$editable = "";
	}

	if (isset($_POST['add_alt'])) {
		$_SESSION['add_alt'] = "add_alt";

		$editable = "";
	}

	if (isset($_POST['change_pwd'])) {
		$_SESSION['change_pwd'] = "change_pwd";
		$editable = "";
	}

	if (isset($_POST['cancel_pwd'])) {
		unset($_SESSION['change_pwd']);
		$editable = "";
	}

	if (isset($_POST['save_pwd'])) {
		$pwd = htmlspecialchars($_POST['pwd']);
		$pwd = mysqli_real_escape_string($conn, $pwd);

		// password hashing
		$hash = password_hash($pwd, PASSWORD_DEFAULT);

		// query to update row in USER table
		$sql = "UPDATE `USER` 
				SET `password`='$hash' 
				WHERE `user_id`='$user_id'";

		// if contact number successfully updated
		if (execute_query($conn, $sql)) {
			// notify user
			$update_msg[] = 'Password changed successfully.';
		} else {
			$errors[] = 'Error changing password! Please try again later.';
		}

		unset($_SESSION['change_pwd']);
		$editable = "";
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<section class="container content-wrap">
		<h3 class="center brand-text">Manage Profile</h3>
		<form action="manage_profile.php" method="POST">
			<div class="center">
				<?php if ($editable != "") : ?>
						<button type="submit" name="edit_profile" value="edit" class="btn z-depth-0 brand-dark">Edit Profile<i class="material-icons left">edit</i></button>
				<?php else : ?>
					<?php if (!isset($_SESSION['change_pwd'])) : ?>
						<button type="submit" name="cancel" value="cancel" class="btn z-depth-0 red" formnovalidate>Stop Editing<i class="material-icons left">chevron_left</i></button>
						<button type="submit" name="save_changes" value="save" class="btn z-depth-0 green">Save Changes<i class="material-icons right">check</i></button>
					
						<button type="submit" name="change_pwd" value="change_pwd" class="btn z-depth-0 brand-dark" formnovalidate>Change Password<i class="material-icons left">edit</i></button>
					<?php else : ?>
						<button type="submit" name="cancel_pwd" value="cancel" class="btn z-depth-0 red" formnovalidate>Cancel Changes<i class="material-icons left">chevron_left</i></button>
						<button type="submit" name="save_pwd" value="save" class="btn z-depth-0 green">Save Password<i class="material-icons right">check</i></button>
					<?php endif ?>
				<?php endif ?>
			</div>
			<div class="row">
				<div class="col s12 card white">
	        		<div class="card-content container">
	        			<div class="row">
	        				<?php if (isset($update_msg)) : ?>
				        		<ul class="browser-default update-msg">
				        			<?php foreach ($update_msg as $msg) : ?>
				        				<li><?php echo $msg; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>
	        				<?php if (isset($errors)) : ?>
				        		<ul class="browser-default error-msg">
				        			<?php foreach ($errors as $error) : ?>
				        				<li><?php echo $error; ?></li>
				        			<?php endforeach ?>
				        		</ul>
				        	<?php endif ?>

				        	<?php if (isset($_SESSION['change_pwd'])) : ?>
				        		<!-- CHANGE PASSWORD -->
								<h5 class="center grey-text">Change Password</h5>
				        		<!-- Password input -->
								<div class="input-field col s6">
									<i class="material-icons prefix">lock</i>
									<input id="su_pwd" type="password" name="pwd" class="validate" required onchange="check_pwd_con()">
									<label for="su_pwd">Password</label>
									<span id="pwd_helper" class="helper-text" data-error="Required field."/>
								</div>

								<!-- Confirm Password input -->
								<div class="input-field col s6">
									<input id="su_pwd_con" type="password" name="pwd_con" class="validate" required onchange="check_pwd_con()" onblur="check_pwd_con()">
									<label for="su_pwd_con">Confirm Password</label>
									<span id="pwd_con_helper" class="helper-text" data-error="Required field."/>
								</div>
				        	<?php else : // show profile details ?>
								<!-- START OF ACCOUNT DETAILS -->
								<h5 class="center grey-text">Account Details</h5>

								<!-- Username input -->
								<div class="input-field col s6">
									<i class="material-icons prefix">account_circle</i>
									<input disabled id="su_username" type="text" name="username" 
									value="<?php echo $_SESSION['user_details']['username'] ?>">
									<label for="username">Username</label>
								</div>

								<!-- Contacts input -->
								<div class="input-field col s6">
									<i class="material-icons prefix">phone</i>
									<input <?php echo $editable; ?> id="contact" type="tel" name="contact" value="<?php echo $_SESSION['user_details']['contact_num'] ?>"  class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
									<label for="contact">Contact Number</label>
									<span id="contact_helper" class="helper-text" data-error="Required field."/>
								</div>

								<div class="col s12">
									<div class="divider"></div>
									<br>
								</div>

								<!-- START OF PERSONAL DETAILS -->
								<h5 class="center grey-text">Personal Details</h5>

								<br>

								<!-- IC Number input -->
								<div class="input-field col s6">
									<input disabled id="ic_num" type="number" name="ic_num" value="<?php echo $_SESSION['user_details']['ic_num'] ?>">
									<label for="ic_num">MyKad Number</label>
								</div>

								<!-- IC Name input -->
								<div class="input-field col s6">
									<input disabled id="ic_name" type="text" name="ic_name" value="<?php echo $_SESSION['user_details']['ic_name'] ?>">
									<label for="ic_name">Full Name (According to MyKad)</label>
								</div>

								<!-- CURRENT ADDRESS -->
								<ul class="collection with-header">
									<li class="collection-header grey-text"><h6>Current Address</h6></li>
									<li class="collection-item">
										<!-- Level input -->
										<div class="input-field col s3">
											<input <?php echo $editable; ?> id="level" type="number" name="level" value="<?php echo $current_address['level'] ?>" class="validate" required>
											<label for="level">Level</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- Unit input -->
										<div class="input-field col s3">
											<input <?php echo $editable; ?> id="unit" type="number" name="unit" value="<?php echo $current_address['unit'] ?>" class="validate" required>
											<label for="unit">Unit Number</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- Address input -->
										<div class="input-field col s6">
											<input disabled id="address" type="text" name="address" value="<?php echo $current_address['address'] ?>">
											<label for="address">Address</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- Postcode input -->
										<div class="input-field col s3">
											<input disabled id="postcode" type="number" name="postcode" value="<?php echo $current_address['postcode'] ?>">
											<label for="postcode">Postcode</label>
											<span class="helper-text" data-error="Required field."/>
										</div>

										<!-- City input -->
										<div class="input-field col s3">
											<input disabled id="city" type="text" name="city" value="<?php echo $current_address['city'] ?>">
											<label for="city">City</label>
											<span class="helper-text" data-error="Required field."/>
										</div>
										
										<!-- State input -->
										<div class="input-field col s6">
											<input disabled id="state" type="text" name="state" value="<?php echo $current_address['state'] ?>">
											<label for="state">State</label>
										</div>
									</li>
								</ul>

								<?php if (isset($alt_address) || isset($_SESSION['add_alt'])) : ?>
									<!-- ALTERNATIVE ADDRESS -->
									<ul class="collection with-header">
										<?php if ($editable == "") : ?>
											<!-- https://stackoverflow.com/questions/25617583/how-add-confirmation-box-in-php-before-deleting/45468719 -->
											<button type="submit" name="delete_alt" value="delete_alt" class="btn z-depth-0 red secondary-content" onClick="javascript: return confirm('Are you sure you want to delete this address?');" formnovalidate>Delete<i class="material-icons left">clear</i></button>
										<?php endif ?>
										<li class="collection-header grey-text"><h6>Alternative Address</h6></li>
										
										<li class="collection-item">
											<!-- Level input -->
											<div class="input-field col s3">
												<input <?php echo $editable; ?> id="alt_level" type="number" name="alt_level" min="0" value="<?php echo isset($alt_address['level'])? $alt_address['level'] : "" ?>" class="validate" required>
												<label for="alt_level">Level</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- Unit input -->
											<div class="input-field col s3">
												<input <?php echo $editable; ?> id="alt_unit" type="number" name="alt_unit" min="0" value="<?php echo isset($alt_address['unit'])? $alt_address['unit'] : "" ?>" class="validate" required>
												<label for="alt_unit">Unit Number</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- Address input -->
											<div class="input-field col s6">
												<input <?php echo $editable; ?> id="alt_address" type="text" name="alt_address" value="<?php echo isset($alt_address['address'])? $alt_address['address'] : "" ?>" class="validate" required>
												<label for="alt_address">Address</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- Postcode input -->
											<div class="input-field col s3">
												<input <?php echo $editable; ?> id="alt_postcode" type="number" name="alt_postcode" min="0" max="99999" value="<?php echo isset($alt_address['postcode'])? $alt_address['postcode'] : "" ?>" class="validate" required>
												<label for="alt_postcode">Postcode</label>
												<span class="helper-text" data-error="Required field."/>
											</div>

											<!-- City input -->
											<div class="input-field col s3">
												<input <?php echo $editable; ?> id="alt_city" type="text" name="alt_city" value="<?php echo isset($alt_address['city'])? $alt_address['city'] : "" ?>">
												<label for="alt_city">City</label>
											</div>

											<!-- State input -->
											<?php if ($editable == "") : ?>
												<div class="input-field col s6">
													<select id="alt_state" name="alt_state">
														<?php 
														// set selected option if state has been previously selected
														foreach ($states as $option => $value) {
															$attribute = '';

															// if the state matches the states option value
															if ((isset($alt_address['state'])? $alt_address['state'] : "") == $value) {
																$attribute = ' selected';
															}

															// disable the 'Choose your option' option
															if ($value == '') {
																$attribute .= ' disabled';
															}

															echo '<option value="'.$value.'"'.$attribute.'>'.$option.'</option>';
														} ?>
													</select>
													<label>State</label>
												</div>
											<?php else : ?>
												<!-- State input -->
												<div class="input-field col s6">
													<input disabled id="alt_state" type="text" name="alt_state" value="<?php echo $alt_address['state'] ?>">
													<label for="alt_state">State</label>
												</div>
											<?php endif ?>
										</li>
									</ul>
								<?php elseif ($editable == "") : ?>
									<button type="submit" name="add_alt" value="add_alt" class="btn z-depth-0 green">Add Alternative Address<i class="material-icons left">add</i></button>
								<?php endif ?>
							<?php endif ?>
						</div>
	        		</div>
	        	</div>
			</div>
		</form>
	</section>

	<?php include('../templates/footer.php') ?>
</html>