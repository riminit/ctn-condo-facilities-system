<?php 
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	$user_id = $_SESSION['user_details']['user_id'];

	// query for getting all reports
	$sql = "SELECT `contact_num`, `report_title`, `report_subject`, `report_desc`, `report_img`, `report_date`, `status`  
			FROM `REPORT` 
			WHERE user_id='$user_id'
			ORDER BY `report_date` DESC";

	// get result in array
	$reports = select_multiple($conn, $sql);

	// Toggle Notifications View
	$view = isset($_GET['view']) && strtolower($_GET['view']) == 'list' ? 'list' : 'card';
	$list_or_card = $view == 'card' ? 'list' : 'card';

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	
	<?php include('../templates/navbar.php') ?>

	<section class="content-wrap">
		<h3 class="center brand-text">My Reports</h3>
		<div class="row">
			<form action="my_reports.php?view=<?php echo $view; ?>" method="POST">
				<div class="center">
		          <a href="my_reports.php?view=<?php echo $list_or_card ?>" class="btn z-depth-0 brand-dark"><?php echo ($view=='card') ? 'List View' : 'Card View'; ?></a>
		        </div>
		    </form>
			<?php 
				if ($view == 'card') :
					$counter = 0; 
					foreach ($reports as $report) :
						if ($counter<9) :
			?>
				<div class="col s8 m4">
					<div class="card">
						<div class="card-content" style="height: 200px">
							<span class="card-title"><?php echo $report['report_title']; ?></span>
							<p><?php echo $report['report_subject']; ?></p>
							<div class="row grey-text">
								<div class="col s5">
									<p>Sent on:</p>
								</div>
								<div class="col s7">
									<p class="right"><?php echo date("h:i a, d/m/Y", strtotime($report['report_date'])); ?></p>
								</div>
							</div>
							<div class="col s12">
								<?php if ($report['status']) : ?>
									<span class="new badge green" data-badge-caption="solved"></span>
								<?php else : ?>
									<span class="new badge red" data-badge-caption="unsolved"></span>
								<?php endif ?>
							</div>
					    </div>
						<div class="card-action">
							<a href="#" class="activator">More Info<i class="material-icons right">more_vert</i></a>
						</div>
						<div id="card-reveal" class="card-reveal">
							<span class="card-title"><?php echo $report['report_title']; ?><i class="material-icons right">close</i></span>
							<p class="grey-text">Subject: </p>
							<span><?php echo $report['report_subject']; ?></span>

							<p class="grey-text">Description: </p>
							<span><?php echo nl2br($report['report_desc']); ?></span>
							
							<!-- Display facility image -->
							<p class="grey-text">Submitted Image:</p>
							<div class="card white">
								<div class="card-image">
									<?php if (!is_null($report['report_img'])) : ?>
										<img src="../img/reports/<?php echo $report['report_img'] ?>" alt="Report Image">
									<?php endif ?>
								</div>
							</div>

							<div class="row grey-text">
								<div class="col s5">
									<p>Sent on:</p>
								</div>
								<div class="col s7">
									<p class="right"><?php echo date("h:i a, d/m/Y", strtotime($report['report_date'])); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php 
						endif;
						$counter++;
					endforeach;
				else:
			?>
				<div class="col s12">
					<ul class="collapsible popout">
						<?php foreach ($reports as $report) : ?>
							<li>
								<div class="collapsible-header">
									<div class="col s10">
										<h6><?php echo $report['report_title']; ?></h6>
										<span class="grey-text"><?php echo $report['report_subject']; ?></span>
									</div>
									<div class="col s1">
										<?php if ($report['status']) : ?>
											<span class="new badge green" data-badge-caption="solved"></span>
										<?php else : ?>
											<span class="new badge red" data-badge-caption="unsolved"></span>
										<?php endif ?>
									</div>
									<div class="col s1 grey-text">
										<?php echo date("h:i a, d/m/Y", strtotime($report['report_date'])); ?>
									</div>
								</div>
								<div class="collapsible-body grey lighten-4">
									<span><?php echo $report['report_desc']; ?></span>
									<div class="container">
										<div class="card white">
											<div class="card-image">
												<?php if (!is_null($report['report_img'])) : ?>
													<img src="../img/reports/<?php echo $report['report_img'] ?>" alt="Report Image">
												<?php endif ?>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php endforeach ?>
					</ul>
				</div>

			<?php endif; ?>
		</div>
	</section>

	<?php include('../templates/footer.php') ?>

</html>
