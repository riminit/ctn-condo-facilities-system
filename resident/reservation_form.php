<section class="container content-wrap">
	<?php if (isset($_POST['reserve_facility']) || isset($_POST['review_reservation'])) : ?>
   		<h3 class="center brand-text">Make Reservation</h3>
   	<?php else : // Edit Reservation ?>
   		<h3 class="center brand-text">Edit Reservation</h3>
   	<?php endif ?>
   	<h5 class="center brand-text"><?php echo $facility_name; ?></h5>
      	<div class="card white">
	        <div class="card-content">
	        	<?php if (isset($_POST['reserve_facility'])) : ?>
	        		<h5 class="center grey-text">Fill in the details</h5>
	        		<form action="make_reservation.php" method="POST">
	        	<?php elseif (isset($_POST['review_reservation'])) : ?>
	        		<h5 class="center grey-text">Review the details</h5>
	        		<form action="make_reservation.php" method="POST">
	        	<?php else : // Edit Reservation ?>
	        		<h5 class="center grey-text">Edit the details</h5>
	        		<form action="my_reservations.php" method="POST">
	        	<?php endif ?>
					<div class="row">
						<div class="input-field col s6">
							<i class="material-icons prefix">account_circle</i>
							<?php if (isset($_POST['reserve_facility'])) : ?>
								<input id="name" type="text" name="name" value="<?php echo (isset($_SESSION['reservation_details']) ? $name : $_SESSION['user_details']['ic_name']) ?>" class="validate" required>
							<?php elseif (isset($_POST['review_reservation'])) : ?>
								<input disabled id="name" type="text" name="name" value="<?php echo $name ?>">
							<?php else : // Edit Reservation ?>
								<input id="name" type="text" name="name" value="<?php echo $name ?>" class="validate" required>
							<?php endif ?>
							<label for="name">Name</label>
							<span id="name_helper" class="helper-text" data-error="Required field."/>
						</div>

						<!-- Contacts input -->
						<div class="input-field col s6">
							<i class="material-icons prefix">phone</i>
							<?php if (isset($_POST['reserve_facility'])) : ?>
								<input id="contact" type="tel" name="contact" value="<?php echo (isset($_SESSION['reservation_details']) ? $contact : $_SESSION['user_details']['contact_num']) ?>" class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
							<?php elseif (isset($_POST['review_reservation'])) : ?>
								<input disabled id="contact" type="tel" name="contact" value="<?php echo $contact ?>">
							<?php else : // Edit Reservation ?>
								<input id="contact" type="tel" name="contact" value="<?php echo $contact ?>" class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
							<?php endif ?>
							
							<label for="contact">Contact Number</label>
							<span id="contact_helper" class="helper-text" data-error="Required field."/>
						</div>

						<div class="input-field col s4">
							<?php if (isset($_POST['reserve_facility'])) : ?>
								<input id="date" type="date" name="date" value="<?php echo (isset($_SESSION['reservation_details']) ? $date : $day_after_current) ?>" class="validate" required min="<?php echo $day_after_current ?>" onchange="check_date()">
							<?php elseif (isset($_POST['review_reservation'])) : ?>
								<input disabled id="date" type="date" name="date" value="<?php echo $date ?>">
							<?php else : // Edit Reservation ?>
								<input id="date" type="date" name="date" value="<?php echo $date ?>" class="validate" required min="<?php echo $day_after_current ?>" onchange="check_date()">
							<?php endif ?>
							<label for="date">Date</label>
							<span id="date_helper" class="helper-text" data-error="Required field."/>
						</div>

						<div class="input-field col s4">
							<?php if (isset($_POST['reserve_facility'])) : ?>
								<input id="start_time" type="time" name="start_time" value="<?php echo (isset($_SESSION['reservation_details']) ? $start_time : "") ?>" class="validate" required min="09:00" max="23:00" onchange="check_time('start')">
							<?php elseif (isset($_POST['review_reservation'])) : ?>
								<input disabled id="start_time" type="time" name="start_time" value="<?php echo $start_time ?>">
							<?php else : // Edit Reservation ?>
								<input id="start_time" type="time" name="start_time" value="<?php echo $start_time ?>" class="validate" required min="09:00" max="23:00" onchange="check_time('start')">
							<?php endif ?>
							<label for="start_time">Start Time</label>
							<small class="grey-text">Min: 30 Minutes, Max: 2 hours</small>
							<span id="start_time_helper" class="helper-text" data-error="Required field."/>
						</div>

						<div class="input-field col s4">
							<?php if (isset($_POST['reserve_facility'])) : ?>
								<input id="end_time" type="time" name="end_time" value="<?php echo (isset($_SESSION['reservation_details']) ? $_SESSION['reservation_details']['end_time'] : "") ?>" class="validate" required min="09:00" max="23:00" onchange="check_time('end')">
							<?php elseif (isset($_POST['review_reservation'])) : ?>
								<input disabled id="end_time" type="time" name="end_time" value="<?php echo $end_time ?>">
							<?php else : // Edit Reservation ?>
								<input id="end_time" type="time" name="end_time" value="<?php echo $end_time ?>" class="validate" required min="09:00" max="23:00" onchange="check_time('end')">
							<?php endif ?>
							
							<label for="end_time">End Time</label>
							<small class="grey-text">Min: 30 Minutes, Max: 2 hours</small>
							<span id="end_time_helper" class="helper-text" data-error="Required field."/>
						</div>
						<?php if (isset($_POST['edit_reservation'])) : ?>
							<input type="hidden" name="reservation_id" value="<?php echo $reservation_id; ?>">
							<input type="hidden" name="facility_id" value="<?php echo $facility_id; ?>">
						<?php endif ?>
						<input type="hidden" name="facility_name" value="<?php echo $facility_name; ?>">
					</div>
					<?php if ($errors) : ?>
		        		<ul class="browser-default error-msg">
		        			<?php foreach ($errors as $error) : ?>
		        				<li><?php echo $error; ?></li>
		        			<?php endforeach ?>
		        		</ul>
		        	<?php endif ?>
					<div class="row">
						<div class="col s4 center">
							<a href="<?php echo ((isset($_POST['edit_reservation']) || isset($_POST['confirm_edit'])) ? 'my_reservations.php' : 'view_facilities.php') ?>" class="btn red z-depth-0"><i class="material-icons left">clear</i>Cancel</a>
						</div>
						<?php if (isset($_POST['reserve_facility'])) : ?>
							<div class="col s4 center offset-s4">
								<button type="submit" name="review_reservation" value="Next" class="btn green z-depth-0"><i class="material-icons right">chevron_right</i>Next</button>
							</div>
						<?php elseif (isset($_POST['review_reservation'])) : ?>
							<div class="col s4 center">
								<button type="submit" name="reserve_facility" value="Back" class="btn orange z-depth-0"><i class="material-icons left">chevron_left</i>Back</button>
							</div>
							<div class="col s4 center">
								<button type="submit" name="confirm_reservation" value="Confirm" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
							</div>
						<?php else : // Edit Reservation ?>
							<div class="col s4 offset-s4 center">
								<button type="submit" name="confirm_edit" value="Confirm" class="btn green z-depth-0"><i class="material-icons right">check</i>Confirm</button>
							</div>
						<?php endif ?>
					</div>
				</form>
			</div>
		</div>
</section>