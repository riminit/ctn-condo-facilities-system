<?php
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// unset reservation_details if reservation_details is set
	if (isset($_SESSION['reservation_details'])) { unset($_SESSION['reservation_details']); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	// query for getting all facilities
	$sql = "SELECT * 
			FROM `FACILITY` 
			ORDER BY `facility_name`";

	// get result in array
	$facilities = select_multiple($conn, $sql);

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<section class="content-wrap">
		<h3 class="center brand-text">View Facilities</h3>
		<form action="make_reservation.php" method="POST">
			<div class="row">
				<?php foreach ($facilities as $facility) : ?>
					<div class="col s8 m4">
						<div class="card sticky-action">
							<div class="card-image waves-effect waves-block waves-light" style="height: 200px">
								<img class="activator" src=<?php echo "../img/facilities/".$facility['facility_img']; ?>>
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4"><?php echo $facility['facility_name']; ?><i class="material-icons right">more_vert</i></span>
							</div>
			                    
							<div class="card-reveal">
			                    <span class="card-title grey-text text-darken-4"><?php echo $facility['facility_name']; ?><i class="material-icons right">close</i></span>
			                    <p><?php echo $facility['facility_desc']; ?></p>
			                </div>

			                <div class="card-action valign-wrapper" style="height: 60px">
			                	<!-- display reservation button if facility requires reservation -->
			                	<?php if ($facility['status'] == 1) : ?>
			                		<button type="submit" name="reserve_facility" value="<?php echo $facility['facility_name']; ?>" class="btn z-depth-0 brand-dark">Make Reservation</button>
			                	<?php elseif ($facility['status'] == 0) : ?>
			                		<a class="grey-text">Does not require reservation</a>
			                	<?php else : ?>
			                		<a class="red-text">Not available</a>
			                	<?php endif ?>
			                </div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</form>
	</section>

	<?php include('../templates/footer.php') ?>
	
</html>

