<?php 
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	$user_id = $_SESSION['user_details']['user_id'];

	// Seen All Button Clicked
	if (isset($_POST['seen_all'])) {
		// query to update row in RECIPIENT table
		$sql = "UPDATE `RECIPIENT` 
				SET `status`='1' 
				WHERE `user_id`='$user_id'";
		execute_query($conn, $sql);
	}

	// If clicked for more info
	if (isset($_POST['id'])) {
		$notification_id = $_POST['id'];
		// query to update row in RECIPIENT table
		$sql = "UPDATE `RECIPIENT` 
				SET `status`='1' 
				WHERE `user_id`='$user_id'
				AND `notification_id`='$notification_id'";

		execute_query($conn, $sql);
	}

	// query for getting all notifications
	$sql = "SELECT NOTIFICATION.notification_id, `subject`, `description`, `sent_on`, `ic_name`, NOTIFICATION.user_id, RECIPIENT.status
			FROM `NOTIFICATION` 
			INNER JOIN `RECIPIENT` ON NOTIFICATION.notification_id = RECIPIENT.notification_id 
			INNER JOIN `USER` ON NOTIFICATION.user_id = USER.user_id 
			WHERE RECIPIENT.user_id='$user_id'
			ORDER BY RECIPIENT.status ASC, `sent_on` DESC";

	// get result in array
	$notifications = select_multiple($conn, $sql);

	// Toggle Notifications View
	$view = isset($_GET['view']) && strtolower($_GET['view']) == 'list' ? 'list' : 'card';
	$list_or_card = $view == 'card' ? 'list' : 'card';

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	
	<?php include('../templates/navbar.php') ?>

	<section class="content-wrap">
		<h3 class="center brand-text">Notifications</h3>
		<div class="row">
			<form action="notifications.php?view=<?php echo $view; ?>" method="POST">
				<div class="center">
		          <button type="submit" name="seen_all" value="seen_all" class="btn z-depth-0 brand-dark">Seen All<i class="material-icons left">check</i></button>
		          <a href="notifications.php?view=<?php echo $list_or_card ?>" class="btn z-depth-0 brand-dark"><?php echo ($view=='card') ? 'List View' : 'Card View'; ?></a>
		        </div>
		    </form>
			<?php 
				if ($view == 'card') :
					$counter = 0; 
					foreach ($notifications as $notification) :
						if (($notification['status']=='0') || ($counter<9)) :
			?>
				<div class="col s8 m4">
					<div class="card">
						<div class="card-content" style="height: 200px">
							<span class="card-title"><?php echo $notification['subject']; ?></span>
							<p>by <?php echo $notification['ic_name']; ?></p>
							<div class="row grey-text">
								<div class="col s5">
									<p>Sent on:</p>
								</div>
								<div class="col s7">
									<p class="right"><?php echo date("h:i a, d/m/Y", strtotime($notification['sent_on'])); ?></p>
								</div>
							</div>
							<?php if (!$notification['status']) : ?>
								<div class="col s12">
									<span class="new badge brand brand-light-text">!</span>
								</div>
							<?php endif ?>
					    </div>
						<div class="card-action">
							<a href="#" class="activator" onclick="update_status(<?php echo $notification['notification_id']; ?>)">More Info<i class="material-icons right">more_vert</i></a>
						</div>
						<div id="card-reveal" class="card-reveal">
							<span class="card-title"><?php echo $notification['subject']; ?><i class="material-icons right">close</i></span>
							<p><?php echo nl2br($notification['description']); ?></p>
							<p class="grey-text">by <?php echo $notification['ic_name']; ?></p>
							<div class="row grey-text">
								<div class="col s5">
									<p>Sent on:</p>
								</div>
								<div class="col s7">
									<p class="right"><?php echo date("h:i a, d/m/Y", strtotime($notification['sent_on'])); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php 
						endif;
						$counter++;
					endforeach;
				else:
			?>
				<div class="col s12">
					<ul class="collapsible popout">
						<?php foreach ($notifications as $notification) : ?>
							<li>
								<div class="collapsible-header" onclick="update_status(<?php echo $notification['notification_id']; ?>)">
									<?php if (!$notification['status']) : ?>
										<div class="col s10">
											<h6><?php echo $notification['subject']; ?></h6>
											<span class="grey-text">by <?php echo $notification['ic_name']; ?></span>
										</div>
										<div class="col s1">
											<span class="new badge brand brand-light-text">!</span>
										</div>
										<div class="col s1 grey-text">
											<?php echo date("h:i a, d/m/Y", strtotime($notification['sent_on'])); ?>
										</div>
									<?php else : ?>
										<div class="col s11">
											<h6><?php echo $notification['subject']; ?></h6>
											<span class="grey-text">by <?php echo $notification['ic_name']; ?></span>
										</div>
										<div class="col s1 grey-text">
											<?php echo date("h:i a, d/m/Y", strtotime($notification['sent_on'])); ?>
										</div>
									<?php endif ?>
								</div>
								<div class="collapsible-body grey lighten-4"><span><?php echo $notification['description']; ?></span></div>
							</li>
						<?php endforeach ?>
					</ul>
				</div>

			<?php endif; ?>
		</div>
	</section>

	<?php include('../templates/footer.php') ?>

</html>
