<?php
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	$errors = array();
	
	if (isset($_POST['reserve_facility'])) {
		// if user clicked the make reservation button at view facilities page
		$facility_name = $_POST['reserve_facility'];
		$current_date = date("Y-m-d");
		$day_after_current = date('Y-m-d', strtotime($current_date.'+ 1 day'));

		if (isset($_SESSION['reservation_details'])) {
			$facility_name = $_SESSION['reservation_details']['facility_name'];
			$name = $_SESSION['reservation_details']['name'];
			$contact = $_SESSION['reservation_details']['contact'];
			$date = $_SESSION['reservation_details']['date'];
			$start_time = $_SESSION['reservation_details']['start_time'];
			$end_time = $_SESSION['reservation_details']['end_time'];
		}

	} elseif (isset($_POST['review_reservation'])) {
		// if user confirm reservation
		$facility_name = $_POST['facility_name'];
		$name = htmlspecialchars($_POST['name']);
		$contact = htmlspecialchars($_POST['contact']);
		$date = htmlspecialchars($_POST['date']);
		$start_time = htmlspecialchars($_POST['start_time']);
		$end_time = htmlspecialchars($_POST['end_time']);

		$_SESSION['reservation_details'] = array('facility_name' => $facility_name,
												'name' => $name,
												'contact' => $contact,
												'date' => $date,
												'start_time' => $start_time,
												'end_time' => $end_time);

		// Check if time range is at least 30 minutes
		$temp_start = strtotime($start_time. " +30 minutes");
		$temp_end = strtotime($end_time);

		if (!($temp_start <= $temp_end)) {
			$errors[] = "Must book for at least 30 minutes.";
			$_POST['reserve_facility'] = $facility_name;
		}

		// Check if time range is more than 2 hours
		$temp_start = strtotime($start_time. " +2 hours");
		$temp_end = strtotime($end_time);

		if (($temp_start < $temp_end)) {
			$errors[] = "Cannot book more than 2 hours.";
			$_POST['reserve_facility'] = $facility_name;
		}

	} elseif (isset($_POST['confirm_reservation'])) {
		$facility_name = mysqli_real_escape_string($conn, $_SESSION['reservation_details']['facility_name']);
		$name = mysqli_real_escape_string($conn, $_SESSION['reservation_details']['name']);
		$contact = mysqli_real_escape_string($conn, $_SESSION['reservation_details']['contact']);
		$date = mysqli_real_escape_string($conn, $_SESSION['reservation_details']['date']);
		$start_time = mysqli_real_escape_string($conn, $_SESSION['reservation_details']['start_time']);
		$end_time = mysqli_real_escape_string($conn, $_SESSION['reservation_details']['end_time']);

		// convert to datetime format
		// https://stackoverflow.com/questions/2215354/php-date-format-when-inserting-into-datetime-in-mysql
		$start_datetime = strtotime($start_time." ".$date);
		$start_datetime = date("Y-m-d H:i:s", $start_datetime);

		$end_datetime = strtotime($end_time." ".$date);
		$end_datetime = date("Y-m-d H:i:s", $end_datetime);

		// query for getting the facility_id
		$sql = "SELECT `facility_id` 
				FROM `FACILITY`
				WHERE `facility_name`='$facility_name'";

		// get result in array
		$result = select_single($conn, $sql);
		$facility_id = $result['facility_id'];

		// Check if there are reservation clashes
		// https://stackoverflow.com/a/49089737
		$sql = "SELECT COUNT(*) AS `count` 
				FROM `RESERVATION` 
				WHERE `facility_id` = '$facility_id' 
				AND `status` = '1' 
				AND (
                    (`start_datetime` BETWEEN '$start_datetime' AND '$end_datetime' OR `end_datetime` BETWEEN '$start_datetime' AND '$end_datetime' )
                    OR
                    ('$start_datetime' BETWEEN `start_datetime` AND `end_datetime` OR '$end_datetime' BETWEEN `start_datetime` AND `end_datetime`)
                    );";

        // get result in array
		$clashes = select_single($conn, $sql);

		if ($clashes['count']>0) {
			$errors[] = "There are time clashes with an existing reservation. Please choose another time.";
			$_POST['reserve_facility'] = $facility_name;
		}

		if (!$errors) {
			$user_id = $_SESSION['user_details']['user_id'];

			// query to insert in RESERVATION table
			$sql = "INSERT INTO `RESERVATION` (`user_id`, `facility_id`, `name`, `contact_num`, `start_datetime`, `end_datetime`, `status`, `visibility`)
					VALUES ('$user_id', '$facility_id', '$name', '$contact', '$start_datetime', '$end_datetime', '1', '1')";

			// if reservation successfully added
			if (execute_query($conn, $sql)) {
				// notify and redirect user to facilities page
				echo "<script type='text/javascript'>alert('Reservation successful.'); window.location.href = 'view_facilities.php'</script>";
			} else {
				echo "<script type='text/javascript'>alert('Error adding reservation! Please try again later.'); window.location.href = 'view_facilities.php'</script>";
			}
		}

	} else {
		header("Location: view_facilities.php");
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<?php include('reservation_form.php') ?>

	<?php include('../templates/footer.php') ?>

</html>