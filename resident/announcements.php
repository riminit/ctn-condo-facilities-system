<?php 
	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	// query for getting all announcements order by the latest/newest
	$sql = "SELECT `announcement_title`, `announcement_desc`, `due_date`, `is_pinned`, `announcement_date`, `ic_name` 
			FROM `ANNOUNCEMENT` 
			INNER JOIN `USER` ON ANNOUNCEMENT.user_id = USER.user_id 
			ORDER BY `is_pinned` DESC, `announcement_date` DESC";

	// get result in array
	$announcements = select_multiple($conn, $sql);

	$active_announcements = array();

	foreach ($announcements as $announcement) {
		// if announcement is still active
		// Reference: https://stackoverflow.com/questions/2832467/how-can-i-check-if-the-current-date-time-is-past-a-set-date-time
		if (new DateTime() < new DateTime($announcement['due_date'])) {
		    $active_announcements[] = $announcement;
		}
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	
	<?php include('../templates/navbar.php') ?>

	<section class="content-wrap">
		<h3 class="center brand-text">Announcements</h3>
		<div class="row">
			<?php foreach ($active_announcements as $announcement) : ?>
				<div class="col s8 m4">
					<div class="card">
						<div class="card-content" style="height: 200px">
							<span class="card-title"><?php echo $announcement['announcement_title']; ?></span>
							<p>by <?php echo $announcement['ic_name']; ?></p>
							<div class="row grey-text">
								<div class="col s5">
									<p>Announced on:</p>
								</div>
								<div class="col s7">
									<p class="right "><?php echo date("h:i a, d/m/Y", strtotime($announcement['announcement_date'])); ?></p>
								</div>
							</div>
							<?php if ($announcement['is_pinned']) : ?>
								<div class="col s12">
									<span class="red-text right"><i class="material-icons left">place</i>Pinned</span>
								</div>
							<?php endif ?>
					    </div>
						<div class="card-action">
							<a class="activator">More Info<i class="material-icons right">more_vert</i></a>
						</div>
						<div class="card-reveal">
							<span class="card-title"><?php echo $announcement['announcement_title']; ?><i class="material-icons right">close</i></span>
							<p><?php echo nl2br($announcement['announcement_desc']); ?></p>
							<p class="grey-text">by <?php echo $announcement['ic_name']; ?></p>
							<div class="row grey-text">
								<div class="col s5">
									<p>Announced on:</p>
								</div>
								<div class="col s7">
									<p class="right "><?php echo date("h:i a, d/m/Y", strtotime($announcement['announcement_date'])); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</section>

<?php include('../templates/footer.php') ?>


</html>
