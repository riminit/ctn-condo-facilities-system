<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>	
	<?php include('../templates/navbar.php') ?>
	<!--header design-->
	<header class="header section valign-wrapper">
		<div class="container center">
			<div class="heading_primary white-text row">
				<h2 class="heading_primary_main">Welcome<br>To<br>CTN Condominium</h2>
				<h6 class="heading_primary_sub">Everything You Need. All Right Here.</h6>
			</div>
			<div class="center">
				<a href="#about_us" class="btn white z-depth-0 brand-dark-text About">About Us</a>
			</div>
		</div>
	</header>
	<section id="about_us" class="container brand-text content-wrap">
		<h3 class="center">About Us</h3>
		<p>
			Live at the center of modern convenience and entertainment at CTN Condominium. With our array of facilities and amenities, CTN Condo is the ideal space to balance life, work and play. And with our facilities system, making reservations are just a few taps away, anywhere, anytime. Announcements and notifications regarding the facilities are easily accessible throught this application. Bridging the gap between convenience and luxury, we at CTN Condominium hope to bring to you a comfortable and luxirious stay.
		</p>
	</section>

	<?php include('../templates/footer.php') ?>
</html>