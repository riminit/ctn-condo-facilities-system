<?php
	// start session if session hasnt been started/$_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	$errors = array();

	$user_id = $_SESSION['user_details']['user_id'];

	if (isset($_POST['edit_reservation'])) {
		$reservation_id = $_POST['edit_reservation'];

		// query for getting the reservation details
		$sql = "SELECT `reservation_id`, `name`, `contact_num`, `start_datetime`, `end_datetime`, reservation.facility_id, `facility_name` 
				FROM `RESERVATION` 
				INNER JOIN `FACILITY` ON RESERVATION.facility_id=FACILITY.facility_id 
				WHERE `reservation_id` = '$reservation_id'";

		// get result in array
		$reservation = select_single($conn, $sql);
		$facility_name = $reservation['facility_name'];
		$name = $reservation['name'];
		$contact = "0".$reservation['contact_num'];
		$reservation_id = $reservation['reservation_id'];
		$facility_id = $reservation['facility_id'];

		$start_datetime = $reservation['start_datetime'];
		$end_datetime = $reservation['end_datetime'];

		$date = date("Y-m-d", strtotime($start_datetime));
		$start_time = date("H:i:s", strtotime($start_datetime));
		$end_time = date("H:i:s", strtotime($end_datetime));

	} elseif (isset($_POST['confirm_edit'])) {
		// if user confirm edit
		$reservation_id = $_POST['reservation_id'];
		$facility_id = $_POST['facility_id'];
		$facility_name = $_POST['facility_name'];
		$name = htmlspecialchars($_POST['name']);
		$contact = htmlspecialchars($_POST['contact']);
		$date = htmlspecialchars($_POST['date']);
		$start_time = htmlspecialchars($_POST['start_time']);
		$end_time = htmlspecialchars($_POST['end_time']);

		// Check if time range is at least 30 minutes
		$temp_start = strtotime($start_time . " +30 minutes");
		$temp_end = strtotime($end_time);

		if (!($temp_start <= $temp_end)) {
			$errors[] = "Must book for at least 30 minutes.";
		}

		// Check if time range is more than 2 hours
		$temp_start = strtotime($start_time . " +2 hours");
		$temp_end = strtotime($end_time);

		if (($temp_start < $temp_end)) {
			$errors[] = "Cannot book more than 2 hours.";
		}

		$name = mysqli_real_escape_string($conn, $name);
		$contact = mysqli_real_escape_string($conn, $contact);
		$date = mysqli_real_escape_string($conn, $date);
		$start_time = mysqli_real_escape_string($conn, $start_time);
		$end_time = mysqli_real_escape_string($conn, $end_time);

		// convert to datetime format
		// https://stackoverflow.com/questions/2215354/php-date-format-when-inserting-into-datetime-in-mysql
		$start_datetime = strtotime($start_time." ".$date);
		$start_datetime = date("Y-m-d H:i:s", $start_datetime);

		$end_datetime = strtotime($end_time." ".$date);
		$end_datetime = date("Y-m-d H:i:s", $end_datetime);

		// Check if there are reservation clashes
		// https://stackoverflow.com/a/49089737
		$sql = "SELECT COUNT(*) AS `count` 
				FROM `RESERVATION` 
				WHERE `facility_id` = '$facility_id' 
				AND `reservation_id` != '$reservation_id'
				AND `status` = '1'
				AND (
                    (`start_datetime` BETWEEN '$start_datetime' AND '$end_datetime' OR `end_datetime` BETWEEN '$start_datetime' AND '$end_datetime' )
                    OR
                    ('$start_datetime' BETWEEN `start_datetime` AND `end_datetime` OR '$end_datetime' BETWEEN `start_datetime` AND `end_datetime`)
                    );";

        // get result in array
		$clashes = select_single($conn, $sql);

		if ($clashes['count']>0) {
			$errors[] = "There are time clashes with an existing reservation. Please choose another time.";
		}

		if (!$errors) {
			// query to update row in RESERVATION table
			$sql = "UPDATE `RESERVATION` 
					SET `name`='$name', `contact_num`='$contact', `start_datetime`='$start_datetime', `end_datetime`='$end_datetime' 
					WHERE `reservation_id`='$reservation_id'";

			// if reservation successfully updated
			if (execute_query($conn, $sql)) {
				// notify and redirect user to my_reservations page
				echo "<script type='text/javascript'>alert('Edit successful.'); window.location.href = 'my_reservations.php'</script>";
			} else {
				echo "<script type='text/javascript'>alert('Error editing reservation! Please try again later.'); window.location.href = 'my_reservations.php'</script>";
			}
		} else {
			$_POST['edit_reservation'] = $reservation_id;
		}

	} else {
		// query for getting all reservations
		$sql = "SELECT `reservation_id`, `name`, `contact_num`, `start_datetime`, `end_datetime`, `recorded_timestamp`, RESERVATION.status, `facility_name`, `facility_img` 
				FROM `RESERVATION` 
				INNER JOIN `FACILITY` ON RESERVATION.facility_id=FACILITY.facility_id 
				WHERE `user_id` = '$user_id' 
				AND `visibility` = '1'
				ORDER BY `start_datetime` DESC";

		// get result in array
		$reservations = select_multiple($conn, $sql);

		$active_reservations = array();
		$cancelled_reservations = array();
		$past_reservations = array();

		foreach ($reservations as $reservation) {
			// if reservation is still active
			// https://stackoverflow.com/questions/2832467/how-can-i-check-if-the-current-date-time-is-past-a-set-date-time
			if (new DateTime() < new DateTime($reservation['end_datetime'])) {
				// if not cancelled / rejected
				if ($reservation['status']) {
					$active_reservations[] = $reservation;
				} else {
					$cancelled_reservations[] = $reservation;
				}
			    
			} else {
				$reservation_date = date_create($reservation['end_datetime']);
				$current_date = date_create(date("Y/m/d"));
				$diff = date_diff($reservation_date,$current_date);
				$diff_str = $diff->format("%a");

				// if the reservation is not older than 30 days
				if ($diff_str<=30) {
					$past_reservations[] = $reservation;
				}
			}
		}
	}

	// if user clicked cancel button at active reservations
	if (isset($_POST['cancel_reservation'])) {
		$reservation_id = $_POST['cancel_reservation'];
		// query to update row in RESERVATION table
		$sql = "UPDATE `RESERVATION` 
				SET `status`='0' 
				WHERE `reservation_id`='$reservation_id'";

		// if reservation successfully cancelled
		if (execute_query($conn, $sql)) {
			// notify and redirect user to my_reservations page
			echo "<script type='text/javascript'>alert('Reservation cancelled successfully.'); window.location.href = 'my_reservations.php'</script>";
		} else {
			echo "<script type='text/javascript'>alert('Error cancelling reservation! Please try again later.'); window.location.href = 'my_reservations.php'</script>";
		}
	}

	// if user clicked delete button at cancelled/past reservations
	if (isset($_POST['delete_reservation'])) {
		$reservation_id = $_POST['delete_reservation'];
		// query to update row in RESERVATION table
		$sql = "UPDATE `RESERVATION` 
				SET `visibility`='0' 
				WHERE `reservation_id`='$reservation_id'";

		// if reservation successfully set to invisible
		if (execute_query($conn, $sql)) {
			// notify and redirect user to my_reservations page
			echo "<script type='text/javascript'>alert('Reservation deleted successfully.'); window.location.href = 'my_reservations.php'</script>";
		} else {
			echo "<script type='text/javascript'>alert('Error deleting reservation! Please try again later.'); window.location.href = 'my_reservations.php'</script>";
		}
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<?php if (!isset($_POST['edit_reservation']) && !isset($_POST['confirm_edit'])) : ?>
		<section class="content-wrap">
			<h3 class="center brand-text">My Reservations</h3>
			<form action="my_reservations.php" method="POST">
				<div class="row">
					<h5 class="center brand-text">Active Reservations</h5>
					<?php if (!$active_reservations) : ?>
						<div class="center">
							<div class="container">
								<div class="card">
									<div class="card-content">
										<span class="card-title grey-text text-darken-4">You don't have any active reservations.</span>
									</div>
									<div class="card-action">
										<a href="view_facilities.php" class="btn z-depth-0 brand-dark">Make a reservation now!</a>
									</div>
								</div>
							</div>
						</div>
					<?php endif ?>
					<?php foreach ($active_reservations as $reservation) : ?>
						<div class="col s8 m4">
							<div class="card sticky-action">
								<div class="card-image waves-effect waves-block waves-light" style="height: 200px">
									<img class="activator" src=<?php echo "../img/facilities/".$reservation['facility_img']; ?>>
								</div>
								<div class="card-content">
									<span class="card-title activator grey-text text-darken-4"><?php echo $reservation['facility_name']; ?><i class="material-icons right">more_vert</i></span>
									<p>Date: <?php echo date("d/m/Y", strtotime($reservation['start_datetime'])); ?></p>
									<p>Time: <?php echo date("h:i a", strtotime($reservation['start_datetime'])) . " - " .  date("h:i a", strtotime($reservation['end_datetime'])); ?></p>
								</div>
				                    
								<div class="card-reveal">
				                    <span class="card-title grey-text text-darken-4"><?php echo $reservation['facility_name']; ?><i class="material-icons right">close</i></span>
				                    <p>Date: <?php echo date("d/m/Y", strtotime($reservation['start_datetime'])); ?></p>
									<p>Time: <?php echo date("h:i a", strtotime($reservation['start_datetime'])) . " - " .  date("h:i a", strtotime($reservation['end_datetime'])); ?></p>
				                    <p>Name: <?php echo $reservation['name']; ?></p>
									<p>Contact: 0<?php echo $reservation['contact_num']; ?></p>
									<div class="row grey-text">
										<div class="col s5">
											<p>Reserved on:</p>
										</div>
										<div class="col s7">
											<p class="right "><?php echo date("h:i a, d/m/Y", strtotime($reservation['recorded_timestamp'])); ?></p>
										</div>
									</div>
				                </div>

				                <div class="card-action valign-wrapper" style="height: 60px">
				                	<div class="col s6">
				                		<button type="submit" name="edit_reservation" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 brand-dark">Edit<i class="material-icons left">edit</i></button>
				                	</div>
			                		<div class="col s6">
			                			<!-- https://stackoverflow.com/questions/25617583/how-add-confirmation-box-in-php-before-deleting/45468719 -->
			                			<button type="submit" name="cancel_reservation" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to cancel this reservation?');">Cancel<i class="material-icons left">clear</i></button>
			                		</div>
				                </div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>

				<div class="row">
					<?php if ($cancelled_reservations) : ?>
						<h5 class="center brand-text">Cancelled Reservations</h5>
						<?php foreach ($cancelled_reservations as $reservation) : ?>
							<div class="col s8 m4">
								<div class="card sticky-action">
									<div class="card-image waves-effect waves-block waves-light" style="height: 200px">
										<img class="activator" src=<?php echo "../img/facilities/".$reservation['facility_img']; ?>>
									</div>
									<div class="card-content">
										<span class="card-title activator grey-text text-darken-4"><?php echo $reservation['facility_name']; ?><i class="material-icons right">more_vert</i></span>
										<p>Date: <?php echo date("d/m/Y", strtotime($reservation['start_datetime'])); ?></p>
										<p>Time: <?php echo date("h:i a", strtotime($reservation['start_datetime'])) . " - " .  date("h:i a", strtotime($reservation['end_datetime'])); ?></p>
									</div>
					                    
									<div class="card-reveal">
					                    <span class="card-title grey-text text-darken-4"><?php echo $reservation['facility_name']; ?><i class="material-icons right">close</i></span>
					                    <p>Date: <?php echo date("d/m/Y", strtotime($reservation['start_datetime'])); ?></p>
										<p>Time: <?php echo date("h:i a", strtotime($reservation['start_datetime'])) . " - " .  date("h:i a", strtotime($reservation['end_datetime'])); ?></p>
					                    <p>Name: <?php echo $reservation['name']; ?></p>
										<p>Contact: 0<?php echo $reservation['contact_num']; ?></p>
										<div class="row grey-text">
											<div class="col s5">
												<p>Reserved on:</p>
											</div>
											<div class="col s7">
												<p class="right "><?php echo date("h:i a, d/m/Y", strtotime($reservation['recorded_timestamp'])); ?></p>
											</div>
										</div>
					                </div>

					                <div class="card-action valign-wrapper" style="height: 60px">
				                		<div class="col s6 offset-s6">
				                			<!-- https://stackoverflow.com/questions/25617583/how-add-confirmation-box-in-php-before-deleting/45468719 -->
				                			<button type="submit" name="delete_reservation" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this reservation?');">Delete<i class="material-icons left">delete</i></button>
				                		</div>
					                </div>
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif ?>
				</div>

				<div class="row">
					<h5 class="center brand-text">Past Reservations</h5>
					<h6 class="center brand-text">Last 30 Days</h6>
					<?php if (!$past_reservations) : ?>
						<div class="center">
							<div class="container">
								<div class="card">
									<div class="card-content">
										<span class="card-title grey-text text-darken-4">No reservations during the last 30 days.</span>
									</div>
								</div>
							</div>
						</div>
					<?php endif ?>
					<?php foreach ($past_reservations as $reservation) : ?>
						<div class="col s8 m4">
							<div class="card sticky-action">
								<div class="card-image waves-effect waves-block waves-light" style="height: 200px">
									<img class="activator" src=<?php echo "../img/facilities/".$reservation['facility_img']; ?>>
								</div>
								<div class="card-content">
									<span class="card-title activator grey-text text-darken-4"><?php echo $reservation['facility_name']; ?><i class="material-icons right">more_vert</i></span>
									<p>Date: <?php echo date("d/m/Y", strtotime($reservation['start_datetime'])); ?></p>
									<p>Time: <?php echo date("h:i a", strtotime($reservation['start_datetime'])) . " - " .  date("h:i a", strtotime($reservation['end_datetime'])); ?></p>
								</div>
				                    
								<div class="card-reveal">
				                    <span class="card-title grey-text text-darken-4"><?php echo $reservation['facility_name']; ?><i class="material-icons right">close</i></span>
				                    <p>Date: <?php echo date("d/m/Y", strtotime($reservation['start_datetime'])); ?></p>
									<p>Time: <?php echo date("h:i a", strtotime($reservation['start_datetime'])) . " - " .  date("h:i a", strtotime($reservation['end_datetime'])); ?></p>
				                    <p>Name: <?php echo $reservation['name']; ?></p>
									<p>Contact: 0<?php echo $reservation['contact_num']; ?></p>
									<div class="row grey-text">
										<div class="col s5">
											<p>Reserved on:</p>
										</div>
										<div class="col s7">
											<p class="right "><?php echo date("h:i a, d/m/Y", strtotime($reservation['recorded_timestamp'])); ?></p>
										</div>
									</div>
				                </div>

				                <div class="card-action valign-wrapper" style="height: 60px">
				                	<!-- <div class="col s6">
				                		<button class="btn z-depth-0 green">Reserve Again</button>
				                	</div>
			                		<div class="col s6"> -->
			                		<div class="col s6 offset-s6">
			                			<!-- https://stackoverflow.com/questions/25617583/how-add-confirmation-box-in-php-before-deleting/45468719 -->
			                			<button type="submit" name="delete_reservation" value="<?php echo $reservation['reservation_id']; ?>" class="btn z-depth-0 red" onClick="javascript: return confirm('Are you sure you want to delete this reservation?');">Delete<i class="material-icons left">delete</i></button>
			                		</div>
				                </div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</form>
		</section>
	<?php else :
		include('reservation_form.php');
	endif?>

	<?php include('../templates/footer.php') ?>

</html>