<?php 
	// start session if session hasnt been started / $_SESSION not set
	if (!isset($_SESSION)) { session_start(); }

	// connect to database
	include('../config/db_connect.php');

	// include database functions
	include('../includes/db_functions.php');

	// include config file
	$config = include('../config/config.php');

	$errors = array();

	// if user submitted the report form
	if (isset($_POST['submit_report'])) {
		$title = mysqli_real_escape_string($conn, htmlspecialchars($_POST['title']));
		$contact = mysqli_real_escape_string($conn, htmlspecialchars($_POST['contact']));
		$description = mysqli_real_escape_string($conn, htmlspecialchars($_POST['description']));

		// if a subject was chosen
		if (isset($_POST['subject'])) {
			$subject = $_POST['subject'];
		} else {
			$subject = "Others";
		}

		$user_id = $_SESSION['user_details']['user_id'];

		// if an image has been uploaded
		if ($_FILES['report_img']['name'] != null) {
			$file = $_FILES['report_img'];
			$filename = $file['name'];
			$file_tmp_name = $file['tmp_name'];
			$file_error = $file['error'];
			$file_size = $file['size'];

			$file_ext = explode('.', $filename);
			$file_actual_ext = strtolower(end($file_ext));

			$allowed = array('png', 'jpeg', 'jpg');

			// if it is an allowed file type
			if (in_array($file_actual_ext, $allowed)) {
				// if there are no file errors
				if ($file_error === 0) {
					// if file size less than 500000 KB / 500 MB
					if ($file_size < 500000) {
						// create unique id based on microseconds for the new filename
						// (to prevent other images from being overwritten)
						$filename_new = uniqid('', true) . "." . $file_actual_ext;

						try {
							$file_destination = $config['doc_root'] . "img/reports/" . $filename_new;
							move_uploaded_file($file_tmp_name, $file_destination);

							// query to insert in REPORT table
							$sql = "INSERT INTO `REPORT` (`user_id`, `contact_num`, `report_title`, `report_subject`, `report_desc`, `report_img`, `status`)
									VALUES ('$user_id', '$contact', '$title', '$subject', '$description', '$filename_new', '0')";

							// if report successfully submitted
							if (execute_query($conn, $sql)) {
								// notify and redirect user to the make_report page
								echo "<script type='text/javascript'>alert('Report submitted successfully.'); window.location.href = 'make_report.php'</script>";
							} else {
								$errors[] = "Error submitting report! Please try again later.";
							}

						} catch (Exception $e) {
							$errors[] = "There was an error uploading your file. If you are on a Darwin(Mac) OS, please set the file permission of the images folders for everyone to Read & Write";
						}

					} else {
						$errors[] = "Your file is to big. Max file size is 500 MB.";
					}

				} else {
					$errors[] = "There was an error uploading your file.";
				}
			} else {
				$errors[] = "Incorrect file type. Only .png, .jpg, and .jpeg files are accepted.";
			}
		} elseif (!$errors) {
			// query to insert in REPORT table
			$sql = "INSERT INTO `REPORT` (`user_id`, `contact_num`, `report_title`, `report_subject`, `report_desc`, `status`)
					VALUES ('$user_id', '$contact', '$title', '$subject', '$description', '0')";

			// if report successfully submitted
			if (execute_query($conn, $sql)) {
				// notify and redirect user to the make_report page
				echo "<script type='text/javascript'>alert('Report submitted successfully.'); window.location.href = 'make_report.php'</script>";
			} else {
				$errors[] = "Error submitting report! Please try again later.";
			}
		}
	}

	// close connection
	mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
	<?php include('../templates/header.php') ?>
	<?php include('../templates/navbar.php') ?>

	<div class="content-wrap"	>
		<section class="container">
			<h3 class="center brand-text">Make A Report</h3>
			<div class="card white">
		        <div class="card-content">
		        	<h5 class="center grey-text">Fill in the details</h5>
		        	<form action="make_report.php" method="POST" enctype="multipart/form-data">
		        		<div class="row">
							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<input id="title" type="text" name="title" class="validate" required>
								<label for="title">Title</label>
								<span id="title_helper" class="helper-text" data-error="Required field."/>
							</div>
							<div class="input-field col s6">
								<i class="material-icons prefix">phone</i>
								<input id="contact" type="tel" name="contact" value="<?php echo $_SESSION['user_details']['contact_num'] ?>" class="validate" required pattern="^(01)[0-46-9]*[0-9]{7,8}$" maxlength="11" onchange="check_contact()">
								<label for="contact">Contact Number</label>
								<span id="contact_helper" class="helper-text" data-error="Required field."/>
							</div>
							<div class="input-field col s6">
								<select id="subject" name="subject">
									<option value="" disabled selected>Choose a Subject</option>
									<option value="Broken Facility">Broken Facility</option>
									<option value="Reservation Issue">Reservation Issue</option>
									<option value="Feedback">Feedback/Suggestion</option>
									<option value="Others">Others</option>
								</select>
								<label>Subject</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<textarea id="description" name="description" class="materialize-textarea validate" required></textarea>
								<label for="description">Description</label>
								<span id="description" class="helper-text" data-error="Required field."/>
							</div>

							<div class="col s12">
								<div class="file-field input-field center">
									<div class="btn-small brand-dark z-depth-0">
										<i class="material-icons left">file_upload</i>
										<span>Upload</span>
										<input type="file" name="report_img">
									</div>
									<div class="file-path-wrapper">
										<input id="image" disabled class="file-path" type="text">
										<p class="left grey-text">*Upload an image to be more specific (Max file size is 500 MB)</p>
									</div>
								</div>
							</div>

							<div class="col s12">
								<?php if ($errors) : ?>
					        		<ul class="browser-default error-msg">
					        			<?php foreach ($errors as $error) : ?>
					        				<li><?php echo $error; ?></li>
					        			<?php endforeach ?>
					        		</ul>
					        	<?php endif ?>
							</div>

							<div class="col s4 center offset-s8">
								<button type="submit" name="submit_report" value="Confirm" class="btn green z-depth-0"><i class="material-icons right">send</i>Submit</button>
							</div>
						</div> <!-- end of div.row -->
		        	</form>
		        </div>	<!-- end of div.card-content -->
		    </div>
		</section>
	</div>

	<?php include('../templates/footer.php') ?>

</html>
