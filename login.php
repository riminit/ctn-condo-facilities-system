<?php
	$username = $password = '';
	$error = '';

	session_start();

	// clear sign up form values
	if (isset($_SESSION['signup_details'])) {
		// unset the signup session variables
		unset($_SESSION['signup_details']);

		if (isset($_SESSION['alt_address'])) {
			// unset the alt_address session variables
			unset($_SESSION['alt_address']);
		}
	}

	// if form was submitted
	if(isset($_POST['login'])) {
		// connect to database
		include('config/db_connect.php');

		// include database functions
		include('includes/db_functions.php');

		// escape any kind of malicious SQL characters & protect from SQL injection
		$username = mysqli_real_escape_string($conn, $_POST['username']);
		$password = mysqli_real_escape_string($conn, $_POST['password']);

		// query for getting data of the user with the same name
		// BINARY to ensure search is case sensitive
		// Reference: https://stackoverflow.com/questions/5629111/how-can-i-make-sql-case-sensitive-string-comparison-on-mysql
		$sql = "SELECT * 
				FROM `USER`
				WHERE BINARY `username`='$username'";

		// get result in array
		$user_account = select_single($conn, $sql);

		// if there is user with the specified username
		if(sizeof($user_account)>0) {
			// if the password is correct
			if (password_verify($password, $user_account['password'])) {
				// if user account is verified
				if ($user_account['status']) {
					session_start();
					session_regenerate_id();

					$_SESSION['user_details'] = $user_account;
					$_SESSION['user_details']['contact_num'] = '0' . $_SESSION['user_details']['contact_num'];
					unset($_SESSION['user_details']['password']);

				} else {
					$error = "User account has not been verified. Please contact the condominium manager to verify your account.";
				}
			} else {
				$error = "Wrong password.";
			}
		} else {
			$error = "There is no user with the specified username.";
		}

		// close connection
		mysqli_close($conn);
	}

	if (isset($_SESSION['user_details'])) {
		// Check user type and redirect accordingly
		if($_SESSION['user_details']['user_type'] == 'resident') {
			header("Location: resident/index.php");
		} else {
			header("Location: admin/index.php");
		}
	}
?>

<!DOCTYPE html>
<html>
	<?php include('templates/header.php') ?>

   <div class="content-wrap">
	<section class="container">
		<div class="row">
			<div class="col s2 offset-s5" style="padding-top: 1rem; margin-bottom: -2rem">
				<img src="img/icons/ctncondo_brandlogo2.ico" class="responsive-img">
			</div>
		</div>
		<h3 id="title" class="center brand-text" style="margin-bottom: -10px">CTN Condominium</h3>
		<h5 class="center brand-text">Facilities System</h5>

		<div class="row">
	      <div class="col m6 offset-m3 card white">
	        <div class="card-content">
	        	<h5 class="center grey-text">Log In to Your Account</h5>
						<form action="login.php" method="POST">
							<div class="row">
								<div class="input-field">
									<i class="material-icons prefix">account_circle</i>
									<input id="username" type="text" name="username" value="<?php echo htmlspecialchars($username) ?>" class="validate" 
									required pattern="^[a-zA-Z]+((_|-)?[a-zA-Z0-9])*[a-zA-Z0-9]*$" 
									onchange="check_username()">
									<label for="username">Username</label>
									<span id="usr_helper" class="helper-text" data-error="Required field."/>
								</div>

								<div class="input-field">
									<i class="material-icons prefix">lock</i>
									<input id="password" type="password" name="password" value="<?php echo htmlspecialchars($password) ?>" class="validate" required>
									<label for="password">Password</label>
									<span id="pswd_helper" class="helper-text" data-error="Required field."/>

									<!-- <a href="#" class="right">Forgot Password?</a> -->
								</div>
							</div>

							<?php if ($error) : ?>
				        		<ul class="error-msg browser-default">
				        			<li><?php echo $error; ?></li>
				        		</ul>
				        	<?php endif ?>
							
							<div class="center">
								<input type="submit" name="login" value="Log In" class="btn brand-dark z-depth-0">
							</div>
						</form>

	        </div>	<!-- End of card-content div -->
	        <div class="divider"></div>

	        <div class="center">
	        	<p class="grey-text">Need an account?
	        		<a href="signup/signup.php">Sign Up</a>
	        	</p>
	        </div>

	      </div>
	    </div>	<!-- End of row div -->
	</section>
  </div>

	
	<?php include('templates/footer.php') ?>
	

	<script type="text/javascript">
		// Check if username was entered correctly
		function check_username(){
			var input = document.getElementById('username');
			var validityState_object = input.validity;

			var spanTxt = document.getElementById('usr_helper');

			if (validityState_object) {
				var val = document.getElementById("username").value;
				var errorMsg = '';
				if (val == '') {
					errorMsg = "Required field.";

				}else if (val.startsWith('-') || val.startsWith('_') || val.endsWith('-') || val.endsWith('_')) {
					errorMsg = "Cannot have an underscore or hyphen at the start or end."

				} else if (/[a-zA-Z]+((_|-){2,})[a-zA-Z0-9]*/.test(val)) {
					errorMsg = "Cannot have two or more underscores or hyphens in a row."

				} else {
					errorMsg = "Only alphanumeric characters, underscores and hyphens."
				}

				spanTxt.setAttribute("data-error", errorMsg)
			} else {
				spanTxt.setAttribute("data-error", null)
			}
		}
	</script>

</html>