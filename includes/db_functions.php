<?php 

	// Check and redirect user if they try to go to db_functions.php
	$current_dir = $_SERVER['PHP_SELF'];
	if (strpos($current_dir, 'db_functions.php')) {
		// start session if session hasnt been started/$_SESSION not set
		if (!isset($_SESSION)) { session_start(); }

		// Check if there is no user logged in
		if (!isset($_SESSION['user_details'])) {
			header("Location: ../login.php");
		} else { 
			// Check if user is logged in as a resident or admin
			$user_type = $_SESSION['user_details']['user_type'];

			// Separate the dir by '/'
			$explode_dir = explode("/",$current_dir);
			$account_dir = $explode_dir[2];

			// Send user back to respective index page
			header("Location: ../" . $user_type . "/index.php");

		}
	}

	function select_single($conn, $sql) {
		// make query & get result
		$result = mysqli_query($conn, $sql);

		$array_res = array();

		// Check if number of rows affected is only 1
		if(mysqli_num_rows($result) == 1) {
			// fetch the resulting row as an array
			$array_res = mysqli_fetch_assoc($result);
		}

		// free result from memory
		mysqli_free_result($result);

		return $array_res;

	}

	function select_multiple($conn, $sql) {
		// make query & get result
		$result = mysqli_query($conn, $sql);

		// fetch the resulting rows as an array
		$array_res = mysqli_fetch_all($result, MYSQLI_ASSOC);

		// free result from memory
		mysqli_free_result($result);

		return $array_res;
	}

	function execute_query($conn, $sql) {
		// execute query and check
		if (mysqli_query($conn, $sql)) {
			// success
			return true;
		} else {
			// failure
			return false;
		}
	}

?>