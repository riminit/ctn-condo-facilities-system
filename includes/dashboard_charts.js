window.onload = function() {
  // RESERVATION CHARTS ======================================
  // Today
  var ctx = document.getElementById('reservationsTodayChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($fac_res_today); ?>,
          datasets: [{
              label: '# of Reservations',
              data: <?php echo json_encode($res_fre_today); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.BlueRed6'
            }
          }
      }
  });

  // This Month
  var ctx = document.getElementById('reservationsMonthChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($res_fac_month); ?>,
          datasets: [{
              label: '# of Reservations',
              data: <?php echo json_encode($res_fre_month); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.BlueRed6'
            }
          }
      }
  });

  // Active
  var ctx = document.getElementById('reservationsActiveChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($res_fac_active); ?>,
          datasets: [{
              label: '# of Reservations',
              data: <?php echo json_encode($res_fre_active); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.BlueRed6'
            }
          }
      }
  });

  // REPORT CHARTS ===========================================
  // Today
  var ctx = document.getElementById('reportsTodayChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($rep_sub_today); ?>,
          datasets: [{
              label: '# of Report',
              data: <?php echo json_encode($rep_fre_today); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.Violet6'
            }
          }
      }
  });

  // This Month
  var ctx = document.getElementById('reportsMonthChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($rep_sub_month); ?>,
          datasets: [{
              label: '# of Report',
              data: <?php echo json_encode($rep_fre_month); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.Violet6'
            }
          }
      }
  });

  // Unsolved
  var ctx = document.getElementById('reportsUnsolvedChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($rep_sub_active); ?>,
          datasets: [{
              label: '# of Report',
              data: <?php echo json_encode($rep_fre_active); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.Violet6'
            }
          }
      }
  });

  // ACCOUNTS CHARTS ======================================
  // Resident Accounts
  var ctx = document.getElementById('residentAccChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($res_status); ?>,
          datasets: [{
              label: '# of Accounts',
              data: <?php echo json_encode($res_status_fre); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.Forte6'
            }
          }
      }
  });

  // All Enabled Accounts
  var ctx = document.getElementById('allAccChart');
  var chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: <?php echo json_encode($user_types); ?>,
          datasets: [{
              label: '# of Accounts',
              data: <?php echo json_encode($user_types_fre); ?>,
              borderWidth: 1
          }]
      },
      options: {
          responsive:true,
          maintainAspectRatio:false,
          legend: {
            position: 'top',
          },
          plugins: {
            colorschemes: {
              scheme: 'office.Forte6'
            }
          }
      }
  });
};