<?php 
  // Check and redirect user if they try to go to dashboard_data.php
  $current_dir = $_SERVER['PHP_SELF'];
  if (strpos($current_dir, 'dashboard_data.php')) {
    // start session if session hasnt been started/$_SESSION not set
    if (!isset($_SESSION)) { session_start(); }

    // Check if there is no user logged in
    if (!isset($_SESSION['user_details'])) {
      header("Location: ../login.php");
    } else { 
      // Check if user is logged in as a resident or admin
      $user_type = $_SESSION['user_details']['user_type'];

      // Separate the dir by '/'
      $explode_dir = explode("/",$current_dir);
      $account_dir = $explode_dir[2];

      // Send user back to respective index page
      header("Location: ../" . $user_type . "/index.php");
        
    }
  }

  //Connect the database
  include('../config/db_connect.php');
  // include database functions
  include('../includes/db_functions.php');

  // FACILITIES ==================================================================
  // query for getting all facilities
  $sql = "SELECT `status` 
          FROM `FACILITY` 
          ORDER BY `status`";

  // get result in array
  $facilities = select_multiple($conn, $sql);

  $facility_status = array();
  foreach ($facilities as $result) {
    $facility_status[] = $result['status'];
  }
  // get the frequency of each facility status
  $facility_status_fre = array_values(array_count_values($facility_status));

  // ANNOUNCEMENTS ==================================================================
  // query for getting all active announcements
  $sql = "SELECT `is_pinned` 
          FROM `ANNOUNCEMENT` 
          WHERE `due_date` >= NOW()
          ORDER BY `is_pinned`";

  // get result in array
  $announcements = select_multiple($conn, $sql);

  $announcement_status = array();
  foreach ($announcements as $result) {
    $announcement_status[] = $result['is_pinned'];
  }
  // get the frequency of each announcement status
  $announcement_status_fre = array_values(array_count_values($announcement_status));

  // query for getting new announcements in the current month
  $sql = "SELECT COUNT(*) AS `count` 
          FROM `ANNOUNCEMENT` 
          WHERE MONTH(`announcement_date`) = MONTH(CURRENT_DATE())";

  // get result in array
  $month_announcements = select_multiple($conn, $sql);
  
  // RESERVATIONS ==================================================================
  // ACTIVE
  // query for getting all active reservations
  $sql = "SELECT `facility_name` 
          FROM `RESERVATION` 
          INNER JOIN `FACILITY` ON RESERVATION.facility_id=FACILITY.facility_id 
          WHERE RESERVATION.status = '1'
          AND `end_datetime` > NOW()";

  // get result in array
  $active_res_results = select_multiple($conn, $sql);

  $reservations = array();
  foreach ($active_res_results as $result) {
    $reservations[] = $result['facility_name'];
  }
  // get facilities
  $res_fac_active = array_keys(array_count_values($reservations));
  // get the frequency of each facility
  $res_fre_active = array_values(array_count_values($reservations));

  // TODAY
  // query for getting all active reservations for current day
  $sql = "SELECT `facility_name`  
          FROM `RESERVATION` 
          INNER JOIN `FACILITY` ON RESERVATION.facility_id=FACILITY.facility_id 
          WHERE RESERVATION.status = '1'
          AND DATE(`start_datetime`) = CURDATE()";

  // get result in array
  $today_res_results = select_multiple($conn, $sql);

  $reservations = array();
  foreach ($today_res_results as $result) {
    $reservations[] = $result['facility_name'];
  }
  // get facilities
  $fac_res_today = array_keys(array_count_values($reservations));
  // get the frequency of each facility
  $res_fre_today = array_values(array_count_values($reservations));

  // THIS MONTH
  // query for getting all active reservations for current month
  $sql = "SELECT `facility_name`  
          FROM `RESERVATION` 
          INNER JOIN `FACILITY` ON RESERVATION.facility_id=FACILITY.facility_id 
          WHERE RESERVATION.status = '1'
          AND MONTH(`start_datetime`) = MONTH(CURRENT_DATE())";

  // get result in array
  $month_res_results = select_multiple($conn, $sql);

  $reservations = array();
  foreach ($month_res_results as $result) {
    $reservations[] = $result['facility_name'];
  }
  // get facilities
  $res_fac_month = array_keys(array_count_values($reservations));
  // get the frequency of each facility
  $res_fre_month = array_values(array_count_values($reservations));

  // REPORTS ==================================================================
  // UNSOLVED
  // query for getting all unsolved reports
  $sql = "SELECT `report_subject` 
          FROM `REPORT` 
          WHERE `status` = '0'";

  // get result in array
  $unsolved_rep_results = select_multiple($conn, $sql);

  $reports = array();
  foreach ($unsolved_rep_results as $result) {
    $reports[] = $result['report_subject'];
  }
  // get report subjects
  $rep_sub_active = array_keys(array_count_values($reports));
  // get the frequency of each subject
  $rep_fre_active = array_values(array_count_values($reports));

  // TODAY
  // query for getting all new reports for current day
  $sql = "SELECT `report_subject`  
          FROM `REPORT` 
          WHERE DATE(`report_date`) = CURDATE()";

  // get result in array
  $today_rep_results = select_multiple($conn, $sql);

  $reports = array();
  foreach ($today_rep_results as $result) {
    $reports[] = $result['report_subject'];
  }
  // get report subjects
  $rep_sub_today = array_keys(array_count_values($reports));
  // get the frequency of each subject
  $rep_fre_today = array_values(array_count_values($reports));

  // THIS MONTH
  // query for getting all new reports for current month
  $sql = "SELECT `report_subject`  
          FROM `REPORT` 
          WHERE MONTH(`report_date`) = MONTH(CURRENT_DATE())";

  // get result in array
  $month_rep_results = select_multiple($conn, $sql);

  $reports = array();
  foreach ($month_rep_results as $result) {
    $reports[] = $result['report_subject'];
  }
  // get report subjects
  $rep_sub_month = array_keys(array_count_values($reports));
  // get the frequency of each subject
  $rep_fre_month = array_values(array_count_values($reports));

  // ACCOUNTS =============================================================
  // RESIDENT
  // query for getting all resident accounts
  $sql = "SELECT `status` 
          FROM `USER` 
          WHERE `user_type` = 'resident'
          ORDER BY `status`";

  // get result in array
  $resident_results = select_multiple($conn, $sql);

  $resident_accounts = array();
  foreach ($resident_results as $result) {
    $resident_accounts[] = $result['status'];
  }
  // account status
  $res_status = array('Disabled', 'Enabled');
  // get the frequency of each status type
  $res_status_fre = array_values(array_count_values($resident_accounts));

  // ALL ENABLED ACCOUNTS
  // query for getting all enabled accounts
  $sql = "SELECT `user_type`  
          FROM `USER` 
          WHERE `status` = '1'
          ORDER BY `user_type` DESC";

  // get result in array
  $enabled_acc_results = select_multiple($conn, $sql);

  $enabled_acc = array();
  foreach ($enabled_acc_results as $result) {
    $enabled_acc[] = $result['user_type'];
  }
  // user types
  $user_types = array('Admin', 'Resident');
  // get the frequency of each user type
  $user_types_fre = array_values(array_count_values($enabled_acc));

  mysqli_close($conn);

?>