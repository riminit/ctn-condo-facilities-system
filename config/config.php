<?php

	return [
		'database' => [
			'host' => 'localhost',
			'username' => 'root',
			'password' => '',
			'db_name' => 'ctn_condo'
		],
		'doc_root' => $_SERVER['DOCUMENT_ROOT'].'/'.explode("/", $_SERVER['PHP_SELF'])[1].'/',
		'states' => [
			'Choose your option'=>'', 
			'Johor'=>'Johor', 
			'Kedah'=>'Kedah', 
			'Kelantan'=>'Kelantan', 
			'Kuala Lumpur'=>'Kuala Lumpur', 
			'Labuan'=>'Labuan', 
			'Melaka'=>'Melaka', 
			'Negeri Sembilan'=>'Negeri Sembilan', 
			'Pahang'=>'Pahang', 
			'Penang'=>'Penang', 
			'Perak'=>'Perak', 
			'Perlis'=>'Perlis', 
			'Putrajaya'=>'Putrajaya', 
			'Sabah'=>'Sabah', 
			'Sarawak'=>'Sarawak', 
			'Selangor'=>'Selangor', 
			'Terengganu'=>'Terengganu'
		],
		'default_address' => [
			'address' => 'CTN Condominium',
			'postcode' => '10400',
			'city' => array('Georgetown', 'George Town'),
			'state' => 'Penang'
		]
	]

?>