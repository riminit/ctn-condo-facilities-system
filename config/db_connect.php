<?php
	
	$config = include('config.php');
	$db_config = $config['database'];

	// connect to database
	$conn = mysqli_connect($db_config['host'], $db_config['username'], $db_config['password'], $db_config['db_name']);

	// check the connection
	if (!$conn) {
		echo "Connection error: " . mysqli_connect_error();
	}

?>